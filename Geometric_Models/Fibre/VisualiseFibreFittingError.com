# Create transparent muscle material
gfx create material muscle_trans normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 0.67 shininess 0.2;

# Read in fitted model. 
gfx read node DS_FibreFitted
gfx read elem DS_FibreFitted

# Read in error data
gfx read data 0912/FibreFittingError

# Create 3D window
gfx cre window

# Make background colour white
gfx modify window 1 background colour 1 1 1 texture none

# Draw graphics
gfx modify g_element "/" general clear;
gfx modify g_element "/" points domain_datapoints coordinate coordinates tessellation default_points LOCAL glyph sphere size "1*1*1" offset 0,0,0 font default select_on material default data error spectrum default selected_material default_selected render_shaded;
gfx modify g_element "/" lines domain_mesh1d coordinate coordinates tessellation default LOCAL circle_extrusion line_base_size 0.2 select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element "/" surfaces domain_mesh2d coordinate coordinates face xi3_1 tessellation default LOCAL select_on invisible material muscle_trans selected_material default_selected render_shaded;
gfx modify g_element "/" surfaces domain_mesh2d coordinate coordinates face xi3_0 tessellation default LOCAL select_on material muscle_trans selected_material default_selected render_shaded;

# Adjust spectrum scale
gfx modify spectrum default clear overwrite_colour;
gfx modify spectrum default linear reverse range -0.1 0.1 extend_above extend_below rainbow colour_range 0 1 component 1;

