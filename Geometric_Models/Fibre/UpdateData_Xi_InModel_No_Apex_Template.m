function UpdateData_Xi_InModel_No_Apex
clc;

%% This function updates the ipxi and ipdata files for fibre fitting so 
% that only those data points contained within the model and also not
% contained in the apex are used for fibre fitting. 

%% Rewrite ipxi file to only include xi points within non apical and non 
% zero elements. 
fidr = fopen('DS_0912_FibreVector_InModel.ipxi','r');
fidw = fopen('DS_0912_FibreVector_InModel_No_Apex.ipxi', 'w');
temp = fscanf(fidr, '%d', 1);
included_Data = [];
i = 0;
while ~isempty(temp)
    Data_Number = temp;
    Element_Number=fscanf(fidr,'%d', 1);
    Xi = fscanf(fidr, '%f %f %f', 3);
    if ((Element_Number~=0)&(Element_Number ~=1)&(Element_Number~=2)&(Element_Number~=3)&(Element_Number~=4))
        i = i + 1;
        included_Data(i)=Data_Number;
        fprintf(fidw, '\t%d\t%d\t%f\t%f\t%f\n', i, Element_Number, Xi(:)); 
    end
    temp = fscanf(fidr, '%d', 1);
end
fclose(fidw);
fclose(fidr);

%% Rewrite ipdata file to only include data points which are contained by 
% the model and is not in the apical elements. 
fidr = fopen('0912/DS_0912_FibreVector.ipdata', 'r');
fidw = fopen('0912/DS_0912_FibreVector_InModel_No_Apex.ipdata', 'w');

temp = fgetl(fidr);
fprintf(fidw, temp);
fprintf(fidw, '\n');

temp = fscanf(fidr, '%d', 1);
i = 0;
while ~isempty(temp);
    Data_Number = temp;
    location = fscanf(fidr, '%f %f %f', 3);
    vector = fscanf(fidr, '%f %f %f', 3);
    if ~isempty(find(included_Data==Data_Number,1))
        i = i + 1;
        fprintf(fidw, '%d   %f   %f   %f   %f   %f   %f\n', i,location(:), vector(:)); 
    end
    temp = fscanf(fidr, '%d', 1);
end

fclose(fidw);
fclose(fidr);
quit
return
