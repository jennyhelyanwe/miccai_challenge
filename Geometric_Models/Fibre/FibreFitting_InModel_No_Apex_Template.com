fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tri-cubic Hermite Case

fem define node;r;fitted_endo_CIMToDTI
fem define elem;r;fitted_endo_CIMToDTI

#fem export node;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI
#fem export elem;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI

fem define fibre;r;LVModel_Noversions
fem define elem;r;LVModel fibre


################## Data Contained by the model only ####################
fem define data;r;Data/DS_0912_FibreAngles_Proj_ModelBasedCor fibre;
fem list data statistics;

## Convert back to radians
fem change data fibre radians
## Correct the fibre angles
fem change data fibre wrt_xi1
## Write out the correct fibre angles
fem define data;w;Data/DS_0912_FibreAngles_Proj_ModelBasedCor_CorXi1 as DS_0912_FibreAngles_Proj_ModelBasedCor_CorXi1 fibre radians

## Calculate the local coordinates ####
fem define xi;r;DS_0912_FibreVector_InModel_No_Apex

############################################ Fitting ##################################

#system "echo ' =====================================================' " 
#system "echo '               Start To Fit Fibre Orientation ' "
#system "echo ' =====================================================' " 


# Define the fitting parameters

fem define fit;r;LVModelFibre_FixedApex fibre;

# Fit the fibre
fem fit fibre
fem list data;FibreFittingError error full;
fem list data;FibreGeo;

################################ Output the fitted field #########################
fem define fibre;w;DS_FibreFitted

fem export node;DS_FibreFitted as LVModel
fem export elem;DS_FibreFitted as LVModel


