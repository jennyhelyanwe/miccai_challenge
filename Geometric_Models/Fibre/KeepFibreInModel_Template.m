function KeepFibreInModel

%% This function is designed to sort out the ipxi and only keep the points
%% contained by the model

clc;
% Define the filename to be read
filenamer=('DS_0912_FibreVector_ContainCalc.ipxi');
fidr=fopen(filenamer,'r');

%No_data=11184; %input('Enter the number of data .......\n');
% Read in the data points and local xi coordinates
%Data_Number=zeros(No_data,1);
%Element_Number=zeros(No_data,1);
%Xi=zeros(No_data,3);
%load('0912/fibreInfo'); 
temp = fscanf(fidr, '%d', 1);
i = 0;
while ~isempty(temp)
    i = i + 1;
    Data_Number(i)=temp;
    Element_Number(i)=fscanf(fidr,'%d',1);
    Xi(i,:)=fscanf(fidr,'%f %f %f',3);
    temp = fscanf(fidr, '%d', 1);
    %fprintf('The %dth Data is read......\n',i);
end

index=find((Element_Number~=0)&(Element_Number~=1)&(Element_Number~=2)&(Element_Number~=3)&(Element_Number~=4));
fprintf('****** The total number of points contained by the model excluding apical elements is %d ******\n',size(index,1));
Data_Number_red=Data_Number(index);
Element_Number_red=Element_Number(index);
Xi_red=Xi(index,:);
Xi_InModel=[Data_Number_red,Element_Number_red,Xi_red];

save Data/Xi_InModel.mat Xi_InModel;

quit;
return;
