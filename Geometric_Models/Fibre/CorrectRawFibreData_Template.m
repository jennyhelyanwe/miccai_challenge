function CorrectRawFibreData

%% This function is designed to correct the raw fibre angles with the
% corrected epicardial septum data.
clc;

%% Read in the original fibre data first
filenamer1=('0912/DS_0912_FibreAngles_Proj.ipdata');
fidr1=fopen(filenamer1,'r');
% Read in the header
header=fgets(fidr1);
%% Read in data with unknown number of data points. 
%Fibre_Raw=zeros(10000,5);
temp = fscanf(fidr1, '%d', 1);
i = 0;
while ~isempty(temp)
    i = i + 1;
    Fibre_Raw(i,1)=temp;
    Fibre_Raw(i,2:4)=fscanf(fidr1,'%f %f %f',3);
    Fibre_Raw(i,5)=fscanf(fidr1,'%f',1);
    weight=fscanf(fidr1,'%d %d %d %d',4);
    temp = fscanf(fidr1, '%d', 1);
end
No_FibreData = i;
%% Load the local xi coordinates and evaluate theoritcal fibre angles. 
load 0912/Xi_InModel.mat;
TheoFibreAngle=zeros(No_FibreData,1);
for i=1:No_FibreData
    TheoFibreAngle(i)=80-140*Xi_InModel(i,5); 
end

%% Correct fibre angles with respect to theoritical angles. 
% [index,x,y,z,fibre angle,index,element number,xi1,xi2,xi3]
Fibre_Raw_WithXi=[Fibre_Raw,Xi_InModel];
No_data_corrected_epi=0;
No_data_corrected_endo=0;
Fibre_Angle_Corrected=Fibre_Raw_WithXi;
for i=1:No_FibreData
    if (Fibre_Raw_WithXi(i,5)-TheoFibreAngle(i))>90 %%% Epi Case
        Fibre_Angle_Corrected(i,5)=Fibre_Raw_WithXi(i,5)-180;
        No_data_corrected_epi=No_data_corrected_epi+1;
    elseif (Fibre_Raw_WithXi(i,5)-TheoFibreAngle(i))<-90  %%% Endo Case
        Fibre_Angle_Corrected(i,5)=Fibre_Raw_WithXi(i,5)+180;
        No_data_corrected_endo=No_data_corrected_endo+1;
    else
		Fibre_Angle_Corrected(i,5)=Fibre_Raw_WithXi(i,5); % No need for correction. 
    end
end

fprintf('\n ***** The number of data corrected for the epicardial region is %d *****\n',No_data_corrected_epi);
fprintf('\n ***** The number of data corrected for the endocardial region is %d *****\n',No_data_corrected_endo);

save Fibre_Angle_CorrectedWithXi.mat Fibre_Angle_Corrected;

%% Save corrected fibre angle data to data files. 
load Fibre_Angle_CorrectedWithXi.mat
No_FibreData=size(Fibre_Angle_Corrected,1);
filenamew=('0912/DS_0912_FibreAngles_Proj_ModelBasedCor.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'FibreAngles_Proj_ModelBasedCor\n');
for i=1:No_FibreData
    fprintf(fidw,'%d  ',Fibre_Angle_Corrected(i,1));
    fprintf(fidw,'%f  %f  %f  %f  ',Fibre_Angle_Corrected(i,(2:5)));
    fprintf(fidw,'%d  %d  %d  %d\n',1,1,1,1);
end
% Save as exdata for CMGUi visualisation. 
filenamew2=('0912/DS_0912_FibreAngles_Proj_ModelBasedCor.exdata');
fidw2=fopen(filenamew2,'w');
fprintf(fidw2,' Group name: FibreAngles_Proj_ModelBasedCor\n');
fprintf(fidw2,' #Fields=2\n');
fprintf(fidw2,' 1) coordinates, coordinate, rectangular cartesian, #Components=3\n');
fprintf(fidw2,'   x.  Value index= 1, #Derivatives=0\n');
fprintf(fidw2,'   y.  Value index= 2, #Derivatives=0\n');
fprintf(fidw2,'   z.  Value index= 3, #Derivatives=0\n');
fprintf(fidw2,' 2) fibre, field, rectangular cartesian, #Components=1\n');
fprintf(fidw2,'   fibre.  Value index= 4, #Derivatives=0\n');
for i=1:No_FibreData
    fprintf(fidw2,' Node:   %d\n',Fibre_Angle_Corrected(i,1));
    fprintf(fidw2,'  %f\n',Fibre_Angle_Corrected(i,2));
    fprintf(fidw2,'  %f\n',Fibre_Angle_Corrected(i,3));
    fprintf(fidw2,'  %f\n',Fibre_Angle_Corrected(i,4));
    fprintf(fidw2,'  %f\n',Fibre_Angle_Corrected(i,5));
end
fclose(fidw2);
quit;
return
