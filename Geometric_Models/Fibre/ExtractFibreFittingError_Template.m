function ExtractFibreFittingError

clc;
%% This function is designed to extract the fibre fitting error information 
% at each fibre data point using the .opdata error file.

%% Read in the .opdata file
filename = ('0912/FibreFittingError.opdata');
fid = fopen(filename, 'r');

% Get total number of data points
junk = fscanf(fid, '%s', 7);
num_data = fscanf(fid, '%d', 1);
junk = fgetl(fid); % read junk end of line character
junk = fgetl(fid); % read junk second line
junk = fgetl(fid); % read junk third line
%i = 1;

for i = 1:num_data
    junk = fscanf(fid, '%s', 1); % read junk 'nd='
    data_number(i) = fscanf(fid, '%d', 1); 
    junk = fscanf(fid, '%s', 1); % read junk 'EDD(nd)='
    error(i) = fscanf(fid, '%lf', 1);
end
fclose(fid);

%% Sort error data using data number. 
[data_number, I] = sort(data_number); % I is the rearrangement vector
error = error(I); % Use I to reorder error values. 

%% Export as .exdata file
load 0912/FibreLocations;
filenamew = '0912/FibreFittingError.exdata';
fidw = fopen(filenamew, 'w');

fprintf(fidw, ' Group name: FibreFittingError\n');
fprintf(fidw, ' #Fields=2\n');
fprintf(fidw, ' 1) coordinates, coordinate, rectangular cartesian, #Components=3\n');
fprintf(fidw, '   x.  Value index= 1, #Derivatives=0\n');
fprintf(fidw, '   y.  Value index= 2, #Derivatives=0\n');
fprintf(fidw, '   z.  Value index= 3, #Derivatives=0\n');
fprintf(fidw, ' 2) error, field, rectangular cartesian, #Components=1\n');
fprintf(fidw, '   error.  Value index= 4, #Derivatives=0\n');

for i = 1:num_data
    fprintf(fidw,' Node:   %d\n', data_number(i));
    fprintf(fidw,'  %f\n', FibreLocations(i,1));
    fprintf(fidw,'  %f\n', FibreLocations(i,2));
    fprintf(fidw,'  %f\n', FibreLocations(i,3));
    fprintf(fidw,'  %f\n', error(i));
end

fclose(fidw);
disp('Finished writing out fibre fitting error to .exdata file\n')

%% Extract RMSE for errors not at apex.
apex_elem = [1,2,3,4];
RMSE = 0;
mean_error = 0;
n = 1;
tot_data = 0;
fid_ipxi = fopen('DS_0912_FibreVector_InModel_No_Apex.ipxi', 'r');
for i = 1:num_data
    data_number = fscanf(fid_ipxi, '%d', 1);
    elem_number = fscanf(fid_ipxi, '%d', 1);
    if isempty(find(apex_elem == elem_number,1))
        nonapex_error(tot_data+1) = error(i);
        %RMSE = RMSE + error(i)*error(i);
        %mean_error = mean_error + abs(error(i));
        tot_data = tot_data + 1;
    else
        excluded_data(n) = data_number;
        n = n + 1;
    end
    xi = fscanf(fid_ipxi, '%f %f %f', 3);
end
mean_error = mean(abs(nonapex_error));
RMSE = sqrt(sum(nonapex_error.*nonapex_error)/tot_data);
disp(['The RMSE excluding the apex elements is: ', num2str(RMSE)]);
disp(['This is equivalent to ', num2str(RMSE*180/pi), ' degrees.']);
disp(['The mean absolute error excluding the apex elements is: ', num2str(mean_error)]);
disp(['This is equivalent to ', num2str(mean_error*180/pi), ' degrees.']);
quit
return


