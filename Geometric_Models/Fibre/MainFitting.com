############### Main file designed to implement fibre fitting #################


########### Step (1): Rename the extension of the fibre file ###########
system('mv 0912/DS_0912_FibreVector.txt 0912/DS_0912_FibreVector.ipdata');

###### Step (2): Calculate local finite element coordinates for all fibre vectors #######
###### This step is neccesary to project fibre vectors to the ventricular wall ###### 

fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tricubic

fem define node;r;fitted_endo_CIMToDTI
fem define elem;r;fitted_endo_CIMToDTI

## Enter name of the fibre vector file
$filename_vector="0912/DS_0912_FibreVector";
fem define data;r;$filename_vector;
fem list data statistics;
fem define xi;c closest;
fem define xi;w;DS_0912_FibreVector;

fem reallocate;

####### Step (3): Evaluate fibre angles by projecting fibre vectors to the wall ######
fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tricubic

fem define node;r;fitted_endo_CIMToDTI
fem define elem;r;fitted_endo_CIMToDTI

#fem export node;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI
#fem export elem;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI

fem define fibre;r;LVModel_Noversions
fem define elem;r;LVModel fibre

$filename_vector="0912/DS_0912_FibreVector";
fem define data;r;$filename_vector;
fem list data statistics;
fem define xi;r;DS_0912_FibreVector;
fem eval field;DTIFibreField wall_vectors dotprod at_data

########### Step (4): Extract fibre angles from the opfiel file 
system('matlab -nodisplay -r ExtractFibreAngleFromOPFIEL');


########### Step (5): Rewrite finite element coordinates to .mat formt
system('matlab -nodisplay -r KeepFibreInModel');


########### Step (6): Correct the fibre angles based on model predicted theoretical angles
system('matlab -nodisplay -r CorrectRawFibreData');

########### Step (7): Implement fibre fit
read commands;FibreFitting;

