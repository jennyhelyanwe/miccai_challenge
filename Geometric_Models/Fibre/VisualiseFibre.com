gfx create material muscle_trans normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 0.57 shininess 0.2;

gfx read node DS_FibreFitted
gfx read elem DS_FibreFitted

gfx cre window

gfx modify window 1 background colour 1 1 1 texture none

gfx modify g_element "/" general clear;
gfx modify g_element "/" surfaces coordinate coordinates exterior face xi3_0 tessellation default LOCAL select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element "/" cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element "/" lines coordinate coordinates tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element "/" element_points coordinate coordinates discretization "4*4*10" tessellation NONE LOCAL glyph cylinder general size "4*0.5*0.5" centre 0,0,0 font default orientation fibres scale_factors "0*0*0" use_elements cell_centres select_on material blue data fibres spectrum default selected_material default_selected;
gfx modify g_element "/" surfaces coordinate coordinates face xi3_1 tessellation default LOCAL select_on material muscle_trans selected_material default_selected render_shaded;
gfx modify g_element "/" point LOCAL glyph axes_solid_xyz general size "10*10*10" centre -40,-20,0 font default select_on material default selected_material default_selected;
gfx modify spectrum default clear overwrite_colour;
gfx modify spectrum default linear reverse range -1 1 extend_above extend_below rainbow colour_range 0 1 component 1;
