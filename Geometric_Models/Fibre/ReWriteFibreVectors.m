function ReWriteFibreVectors

clc;

%% This function is designed to rewrite the fibre vectors into
%% appropriate format for evaluating field

%% Define the file to be read
filenamer=('Data/FibreVector_CardiacCoor_NegWrong.ipdata');
fidr=fopen(filenamer,'r');

%% Read in the header
junk=fgets(fidr);

%% Define the vector array
no_data=11184;
Fibre_Vectors=zeros(no_data,6);

for i=1:no_data
    index=fscanf(fidr,'%d',1);
    Fibre_Vectors(i,:)=fscanf(fidr,'%f %f %f %f %f %f',6);
end

%% ReWrite the file
filenamew=('Data/FibreVector_CardiacCoor_NegWrong_ForField.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'FibreVector_CardiacCoor_NegWrong_ForField\n');
for i=1:no_data
    fprintf(fidw,'%d  ',i);
    fprintf(fidw,'%f  %f  %f  %f  %f  %f\n',Fibre_Vectors(i,:));
end

fprintf('Finished writing the data file ........\n');

quit;
return
