fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tri-cubic Hermite Case

fem define node;r;../DS_FE_Models/0912/DS_fitted
fem define base;r;LVBasisV2	# Bicubic-linear
fem define elem;r;../DS_FE_Models/0912/DS_fitted

#fem export node;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI
#fem export elem;fitted_endo_CIMToDTI as fitted_endo_CIMToDTI

fem define fibre;r;LVModel_Noversions
fem define elem;r;LVModel fibre


################## Data Contained by the model only ####################
fem define data;r;0912/DS_0912_FibreAngles_Proj_ModelBasedCor fibre;
fem list data statistics;
fem define xi;c contain;
fem define xi;w;DS_0912_FibreVector_InModel

fem reallocate;
