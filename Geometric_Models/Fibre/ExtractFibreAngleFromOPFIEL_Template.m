function ExtractFibreAngleFromOPFIEL

clc;

%% This function is designed to extract the fibre angles from the OPFIEL
% and compare with the angles evaluated from fibre field directly to
% varify the correctness of the cmiss routinue

%
%% Define the file to be read
filenamer=('DTIFibreField_InModel_No_Apex.opfiel');
fidr=fopen(filenamer,'r');

%% Option 1: Initialise arrays with pre-determined number of data points. 
%no_data=11184;

%fibre_angles=zeros(no_data,6); % Xi1,Xi2,Xi3,dot product Xi1,fibre angle,imbrication
%fibre_vectors=zeros(no_data,6); % Xi1, Xi2, Xi3, u,v,w
%F_vector=zeros(no_data,3);
%G_vector=zeros(no_data,3);
%H_vector=zeros(no_data,3);

%% Option 2: Read in data with unknown number of data points. 
data_number_str=fscanf(fidr,'%s',1);
i=1;
j=1;
n = 1;
fprintf('\n The following fibre data points had a 0 fibre vector:\n');
while ~isempty(data_number_str)
    data_number=fscanf(fidr,'%d',1);  % Data point index number
    elem_name=fscanf(fidr,'%s',1);    
    elem_number(i)=fscanf(fidr,'%d',1);  % Element number
    Xi_name=fscanf(fidr,'%s',1);         
    Xi(i,:)=fscanf(fidr,'%f %f %f',3);        % Elemental coordinates for data
    fibre_vectors(i,1:3)=Xi(i,:);
    fibre_angles(i,1:3)=Xi(i,:);
    F_vector_name=fscanf(fidr,'%s',1);    
    F_vector(i,:)=fscanf(fidr, '%f %f %f',3);  % Circumferential Axis
    G_vector_name=fscanf(fidr,'%s',1);        
    G_vector(i,:)=fscanf(fidr,'%f %f %f',3);   % Vertical Axis
    H_vector_name=fscanf(fidr,'%s',1);
    H_vector(i,:)=fscanf(fidr,'%f %f %f',3);   % Normal Axis
    fibre_name=fscanf(fidr,'%s',1);
    fibre_vectors(i,4:6)=fscanf(fidr,'%f %f %f',3);
    if (fibre_vectors(i,4)==0)&(fibre_vectors(i,5)==0)&(fibre_vectors(i,6)==0)
        zero_fibre(n) = i; % Check for zero fibre vectors which will give NaN angles. 
        disp(['Data point number ', num2str(i), ' at element ', num2str(elem_number(i))])
        n = n+1;
    end
    dot_pro_name=fscanf(fidr,'%s',1);
    fibre_angles(i,4)=fscanf(fidr,'%f',1);     
    angle_name=fscanf(fidr,'%s',1);
    fibre_angles(i,5)=fscanf(fidr,'%f',1);     % Fibre angle
    angle_name=fscanf(fidr,'%s',1);
    fibre_angles(i,6)=fscanf(fidr,'%f',1);     % Imbrication angle
	data_number_str=fscanf(fidr,'%s',1);
	if i==100000*j
		fprintf('\n The %dth data have been collected ........\n',i);
        j=j+1;
	end
	i=i+1;
end
fclose(fidr);
no_data = i-1;

%% Read the geometric information
filenamer=('0912/DS_0912_FibreVector.ipdata');
fidr=fopen(filenamer,'r');
junk=fgets(fidr);
fibre_loc=zeros(no_data,3);
for i=1:no_data
    index=fscanf(fidr,'%d',1);
    fibre_loc(i,:)=fscanf(fidr,'%f %f %f',3);  % Get global location of data point. 
    vectors(i,:)=fscanf(fidr,'%f %f %f',3);
end
% Save data info in one structure. 
FibreDataAll.locations=fibre_loc; 
FibreDataAll.angles=fibre_angles;
FibreDataAll.vectors=fibre_vectors;
FibreDataAll.elements=elem_number;
FibreDataAll.F=F_vector;
FibreDataAll.G=G_vector;
FibreDataAll.H=H_vector;

save FibreDataAll FibreDataAll;

load FibreDataAll;
no_data=size(FibreDataAll.angles,1);
FibreDataAll.angles(:,7)=FibreDataAll.angles(:,5)/pi*180; %% Fibre Angle converted to radians
FibreDataAll.angles(:,8)=FibreDataAll.angles(:,6)/pi*180; %% Imbrication Angle converted to radians

%% Write angle and vector data to data files. 
filenamew=('0912/DS_0912_FibreAngles_Proj.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'FibreAngles_Proj\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            fprintf(fidw,'%d  %f  %f  %f  %f  %d  %d  %d  %d\n',n,FibreDataAll.locations(i,:),FibreDataAll.angles(i,7),1,1,1,1);
            n = n + 1;
        end
    end
end
fclose(fidw);
fprintf('Finished writing the angles ipdata file.......\n'); % write fibre angle to file. 

filenamew=('0912/DS_0912_FibreAngle_Proj.exdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,' Group name: FibreAngles_Proj\n');
fprintf(fidw,' #Fields=2\n');
fprintf(fidw,' 1) coordinates, coordinate, rectangular cartesian, #Components=3\n');
fprintf(fidw,'   x.  Value index= 1, #Derivatives=0\n');
fprintf(fidw,'   y.  Value index= 2, #Derivatives=0\n');
fprintf(fidw,'   z.  Value index= 3, #Derivatives=0\n');
fprintf(fidw,' 2) fibre, field, rectangular cartesian, #Components=1\n');
fprintf(fidw,'   fibre.  Value index= 4, #Derivatives=0\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            fprintf(fidw,' Node:   %d\n',n);
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,1));
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,2));
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,3));
            fprintf(fidw,'  %f\n',FibreDataAll.angles(i,7));
            n = n + 1;
        end
    end 
end
fclose(fidw);
fprintf('Finished writing the angles exdata file.......\n'); % for CMGUI visualisation

filenamew=('0912/DS_0912_FibreImbrication_Proj.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'FibreImbrication_Proj\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7))
        fprintf(fidw,'%d  %f  %f  %f  %f  %d  %d  %d  %d\n',n,FibreDataAll.locations(i,:),FibreDataAll.angles(i,8),1,1,1,1);
        n = n + 1;
    end
end
fclose(fidw);
fprintf('Finished writing the imbrication ipdata file.......\n'); % write imbrication angles to file. 

filenamew=('0912/DS_0912_FibreImbrication_Proj.exdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,' Group name: FibreImbrication_Proj\n');
fprintf(fidw,' #Fields=2\n');
fprintf(fidw,' 1) coordinates, coordinate, rectangular cartesian, #Components=3\n');
fprintf(fidw,'   x.  Value index= 1, #Derivatives=0\n');
fprintf(fidw,'   y.  Value index= 2, #Derivatives=0\n');
fprintf(fidw,'   z.  Value index= 3, #Derivatives=0\n');
fprintf(fidw,' 2) fibre, field, rectangular cartesian, #Components=1\n');
fprintf(fidw,'   fibre.  Value index= 4, #Derivatives=0\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            fprintf(fidw,' Node:   %d\n',n);
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,1));
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,2));
            fprintf(fidw,'  %f\n',FibreDataAll.locations(i,3));
            fprintf(fidw,'  %f\n',FibreDataAll.angles(i,8));
            n = n + 1;
        end
    end
end
fclose(fidw);
fprintf('Finished writing the imbrication exdata file.......\n'); % for CMGUI visualisation

filenamew=('0912/DS_0912_FibreVectors_Proj.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'FibreVectors_Proj\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            fprintf(fidw,'%d  %f  %f  %f  %f  %f  %f\n',n,FibreDataAll.locations(i,:),FibreDataAll.vectors(i,(4:6)));
            n = n + 1;
        end
    end
end
fclose(fidw);
fprintf('Finished writing the vectors ipdata file.......\n'); % Store fibre vector data. 

filenamew=('0912/DS_0912_Fibre_WallVector.ipdata');
fidw=fopen(filenamew,'w');
fprintf(fidw,'Fibre_WallVector\n');
n = 1;
for i=1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            fprintf(fidw,'%d  %f  %f  %f  %f  %f  %f\n',n,FibreDataAll.locations(i,:),FibreDataAll.F(i,:));
            n = n + 1;
        end
    end
end
fclose(fidw);
fprintf('Finished writing the wall vectors ipdata file.......\n'); % Store circumferential axis vectors. 

%% Save xi information for all non-zero fibre vectors. 
% Rewrite ipxi file also with updated information (eliminating the zero
% vector fibre data points).
k = 1;
for i = 1:no_data
    if ~isnan(FibreDataAll.angles(i,7)) % Check for zero fibre vectors
        if (FibreDataAll.elements(i) ~=0) % Check that data is within model. 
            Data_number(k) = i;
            Element_number(k) = FibreDataAll.elements(i);
            FibreLocations(k,:) = FibreDataAll.locations(i,:);
            xi(k,:) = Xi(i,:);
            k = k + 1;
        end
    end
end

% Eliminate also the 
% Save data information for use later. 
Xi_InModel = [Data_number', Element_number', xi]; 
save 0912/Xi_InModel.mat Xi_InModel; 
save 0912/FibreLocations.mat FibreLocations;
quit;
return
    
    
    
