function KeepFibreInModel

%% This function is designed to sort out the ipxi and only keep the points
%% contained by the model

clc;
% Define the filename to be read
filenamer=('DS_0912_FibreVector.ipxi');
fidr=fopen(filenamer,'r');

No_data=11184; %input('Enter the number of data .......\n');
% Read in the data points and local xi coordinates
Data_Number=zeros(No_data,1);
Element_Number=zeros(No_data,1);
Xi=zeros(No_data,3);

for i=1:No_data
    Data_Number(i)=fscanf(fidr,'%d',1);
    Element_Number(i)=fscanf(fidr,'%d',1);
    Xi(i,:)=fscanf(fidr,'%f %f %f',3);
    %fprintf('The %dth Data is read......\n',i);
end

index=find(Element_Number~=0);
fprintf('****** The total number of points contained by the model is %d ******\n',size(index,1));
Data_Number_red=Data_Number(index);
Element_Number_red=Element_Number(index);
Xi_red=Xi(index,:);
Xi_InModel=[Data_Number_red,Element_Number_red,Xi_red];
save Data/Xi_InModel.mat Xi_InModel;

quit;
return;
