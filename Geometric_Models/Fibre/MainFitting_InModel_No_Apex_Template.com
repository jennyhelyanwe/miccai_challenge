############### Main file designed to implement fibre fitting #################


########### Step (1): Rename the extension of the fibre file                              ###########
system('cp 0912/DS_0912_FibreVector.txt 0912/DS_0912_FibreVector.ipdata');

##### Step (2): Calculate local finite element coordinates for all fibre vectors ####
#####This step is neccesary to project fibre vectors to the ventricular wall ########  

fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tricubic

fem define node;r;../DS_FE_Models/0912/DS_fitted   # Read in geometrically fitted model. 
fem define elem;r;../DS_FE_Models/0912/DS_fitted

## Enter name of the fibre vector file
$filename_vector="0912/DS_0912_FibreVector";
fem define data;r;$filename_vector;
fem list data statistics;
fem define xi;c contain;        # Calculate xi values of data points inside the model. 
fem define xi;w;DS_0912_FibreVector_InModel; # Write out .ipxi file. 
## Use matlab script to eliminate all data points not in the model (element number of zero).
system('matlab -nodisplay -r UpdateData_Xi_InModel_No_Apex');
fem reallocate;

########### Step (3): Evaluate fibre angles by projecting fibre vectors to the wall       ###########

fem define para;r;LVCubic
fem define coor;r;mapping
fem define base;r;LVBasisV2	# Tricubic

fem define node;r;../DS_FE_Models/0912/DS_fitted # Read in geometrically fitted model. 
fem define elem;r;../DS_FE_Models/0912/DS_fitted

fem define fibre;r;LVModel_Noversions
fem define elem;r;LVModel fibre

$filename_vector="0912/DS_0912_FibreVector_InModel_No_Apex";
fem define data;r;$filename_vector;
fem list data statistics;
fem define xi;r;DS_0912_FibreVector_InModel_No_Apex; # Read in corrected xi file. 
## Do dot product of fibre vector project onto wall to find fibre angle. 
fem eval field;DTIFibreField_InModel_No_Apex wall_vectors dotprod at_data

########### Step (4): Extract fibre angles from the opfiel file 
system('matlab -nodisplay -r ExtractFibreAngleFromOPFIEL');

######## Step (5): Correct the fibre angles based on model predicted theoretical angles
system('matlab -nodisplay -r CorrectRawFibreData');

########### Step (6): Implement fibre fit
read commands;FibreFitting_InModel_No_Apex;

########### Step (7): Convert fitting errors into .exdata format
system('matlab -nodisplay -r ExtractFibreFittingError');

fem quit;
