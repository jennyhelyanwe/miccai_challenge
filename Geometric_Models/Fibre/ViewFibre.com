gfx create material heart ambient 0.3 0 0.3 diffuse 1 0 0 specular 0.5 0.5 0.5 shininess 0.5;
gfx cre mat trans_purple ambient 0.4 0 0.9 diffuse 0.4 0 0.9 alpha 0.3
gfx cre mat bluey ambient 0 0.2 0.4 diffuse 0 0.5 1 specular 0.5 0.5 0.5 shininess 0.8
gfx cre mat gold ambient 1 0.7 0 diffuse 1 0.7 0 specular 1 1 0.8 shininess 0.8
gfx cre mat axes ambient 0.5 0.5 0.5 diffuse 0.5 0.5 0.5

gfx read nodes;DTMRI_CIMModel_FreeApex
gfx read elements;DTMRI_CIMModel_FreeApex

gfx cre win 

gfx  Mod g_element DTMRI_CIMModel_FreeApex general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx  win 1 background colour 1 1 1

gfx  Mod g_element DTMRI_CIMModel_FreeApex lines select_on material default_selected selected_material default_selected
gfx  Mod g_element DTMRI_CIMModel_FreeApex cylinders constant_radius 0.3 material default
gfx  Mod g_element DTMRI_CIMModel_FreeApex element_points glyph cylinder_solid general size "4*0.5*0.5" centre 0,0,0 orientation fibres scale_factor "0*0*0" use_element cell_centres discretization "4*4*10" native_discretization NONE select_on material green Data fibres selected_material default_selected

gfx  Mod g_e DTMRI_CIMModel_FreeApex surfaces exterior face xi3_0 mat heart
gfx  Mod g_e DTMRI_CIMModel_FreeApex surfaces exterior face xi3_1 mat trans_purple


gfx Modify spectrum default clear overwrite_colour;
gfx Modify spectrum default linear range -1.57 1.57 colour_range 0 1 component 1 reverse extend_below extend_above;

#gfx read data;Data/FibreAnglesFromOriginalDTI_aligned_LocalCircInModel_XiBasedCor_CorrectedXi1
#gfx modify g_element FibreAnglesFromOriginalDTI_aligned_LocalCircInModel_XiBasedCor_CorrectedXi1 data_points glyph sphere general size "0.8*0.8*0.8" centre 0,0,0 font default select_on material default data fibre spectrum default selected_material default_selected;




