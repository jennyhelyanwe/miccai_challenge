import os
import scipy
from scipy import array
from fitting_SurfaceData import readIpdata, writeIpdata
from writeComFile import writeComFile

studies = ['0912', '0917', '1017', '1024']
### Step 1: Convert text files to .ipdata files
for study in studies:
    print 'Current study is '+study
    all_files = os.listdir(study)
    os.chdir(study)
 
    for filename in all_files:
        print filename
        name,ext = os.path.splitext(filename)
        if ext == '.txt':
            coords = readIpdata(filename) # This function can take either txt or ipdata file. 
            print coords
            writeIpdata(coords,name+'.ipdata', header='Data File') 
            ### Step 2: Export ipdata file in cm.
            print 'about to write com file'
            os.chdir('../')
            writeComFile(study,name)
            os.system('cm ExportData.com')
            os.chdir(study)
    os.chdir('../')
