def writeComFile(study,name):
    comFile = 'ExportData.com'

    try:
        file = open(comFile, 'w')
    except IOError:
        print 'ERROR: unable to open ', comFile
        return

    file.write('fem define para;r;small;\n')
    file.write('fem define coor;r;mapping;\n')
    file.write('fem define base;r;LVBasis;\n')
    file.write('fem define data;r;'+study+'/'+name+';\n')
    file.write('fem export data '+study+'/'+name+' as '+name+';\n')
    file.write('fem quit;\n')
    file.close()
