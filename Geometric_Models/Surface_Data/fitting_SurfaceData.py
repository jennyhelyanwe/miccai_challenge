import os
import scipy
from scipy import array


def readIpdata( fileName ):
	""" reads ipdata file and returns the x y z coords on data points
	and the header if there is one
	"""
	
	try:
		file = open( fileName, 'r' )
	except IOError:
		print 'ERROR: readIpdata: unable to open', fileName
		return
	
	header = None
	lines = file.readlines()
	if lines[0][0] != ' ':
		header = lines[0]
    	del lines[0]
	
	coords = []
	for l in lines:
		coords.append([float(string) for string in l.split()[1:4]])
	coords = array( coords )
	
	
	return coords



def writeIpdata( d, filename, header=None):
	""" write the coordinates of points in 3D space to ipdata file. Each
	row in d is [x,y,z] of a datapoint. filename is a string, header is
	a string. if ex!=False, uses cmConvert to conver to ex formates. ex 
	can be 'data', 'node' or 'both'
	"""
	
	outputFile = open( filename, 'w')
	if header:
		outputFile.write( header+'\n' )
		
	n = 1
	for i in d:
		outputFile.write(" "+str(n)+"\t   %(x)3.10f\t%(y)3.10f\t%(z)3.10f\t1.0 1.0 1.0\n" %{'x':i[0], 'y':i[1], 'z':i[2]})
		n += 1
	outputFile.close()


