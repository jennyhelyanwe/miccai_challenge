gfx modify spectrum default clear overwrite_colour;
gfx modify spectrum default linear reverse range 0 1 extend_above extend_below rainbow colour_range 0 1 component 1;
gfx create material black normal_mode ambient 0 0 0 diffuse 0 0 0 emission 0 0 0 specular 0.3 0.3 0.3 alpha 1 shininess 0.2;
gfx create material blue normal_mode ambient 0 0 0.5 diffuse 0 0 1 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.2;
gfx create material bone normal_mode ambient 0.7 0.7 0.6 diffuse 0.9 0.9 0.7 emission 0 0 0 specular 0.1 0.1 0.1 alpha 1 shininess 0.2;
gfx create material default normal_mode ambient 1 1 1 diffuse 1 1 1 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;
gfx create material default_selected normal_mode ambient 1 0.2 0 diffuse 1 0.2 0 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;
gfx create material gold normal_mode ambient 1 0.4 0 diffuse 1 0.7 0 emission 0 0 0 specular 0.5 0.5 0.5 alpha 1 shininess 0.3;
gfx create material gray50 normal_mode ambient 0.5 0.5 0.5 diffuse 0.5 0.5 0.5 emission 0.5 0.5 0.5 specular 0.5 0.5 0.5 alpha 1 shininess 0.2;
gfx create material green normal_mode ambient 0 0.5 0 diffuse 0 1 0 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.1;
gfx create material muscle normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 1 shininess 0.2;
gfx create material red normal_mode ambient 0.5 0 0 diffuse 1 0 0 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.2;
gfx create material silver normal_mode ambient 0.4 0.4 0.4 diffuse 0.7 0.7 0.7 emission 0 0 0 specular 0.5 0.5 0.5 alpha 1 shininess 0.3;
gfx create material tissue normal_mode ambient 0.9 0.7 0.5 diffuse 0.9 0.7 0.5 emission 0 0 0 specular 0.2 0.2 0.3 alpha 1 shininess 0.2;
gfx create material transparent_gray50 normal_mode ambient 0.5 0.5 0.5 diffuse 0.5 0.5 0.5 emission 0.5 0.5 0.5 specular 0.5 0.5 0.5 alpha 0 shininess 0.2;
gfx create material white normal_mode ambient 1 1 1 diffuse 1 1 1 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;

gfx read data pa069_ds_lvepi
gfx modify g_element pa069_ds_lvepi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element pa069_ds_lvepi lines select_on material default selected_material default_selected;
gfx modify g_element pa069_ds_lvepi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data pa069_ds_lvendo_trunc
gfx modify g_element pa069_ds_lvendo_trunc general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element pa069_ds_lvendo_trunc lines select_on material default selected_material default_selected;
gfx modify g_element pa069_ds_lvendo_trunc data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;


gfx change data_offset 10000
gfx read data ../../../../LVModel/VickyCanineModel/LVModelEpi
gfx modify g_element LVModelEpi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element LVModelEpi lines select_on material default selected_material default_selected;
gfx modify g_element LVModelEpi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data ../../../../LVModel/VickyCanineModel/LVModelEndo
gfx modify g_element LVModelEndo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element LVModelEndo lines select_on material default selected_material default_selected;
gfx modify g_element LVModelEndo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data TransformedCanine_Epi
gfx modify g_element TransformedCanine_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element TransformedCanine_Epi lines select_on material default selected_material default_selected;
gfx modify g_element TransformedCanine_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data TransformedCanine_Endo
gfx modify g_element TransformedCanine_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element TransformedCanine_Endo lines select_on material default selected_material default_selected;
gfx modify g_element TransformedCanine_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPF_Epi
gfx modify g_element BackTransformedUPF_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPF_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPF_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPF_Endo
gfx modify g_element BackTransformedUPF_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPF_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPF_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedCanine_Epi
gfx modify g_element BackTransformedCanine_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanine_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanine_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedCanine_Endo
gfx modify g_element BackTransformedCanine_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanine_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanine_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedCanineRotated_Epi
gfx modify g_element BackTransformedCanineRotated_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanineRotated_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanineRotated_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedCanineRotated_Endo
gfx modify g_element BackTransformedCanineRotated_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanineRotated_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanineRotated_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFRotated_Epi
gfx modify g_element BackTransformedUPFRotated_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFRotated_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFRotated_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFRotated_Endo
gfx modify g_element BackTransformedUPFRotated_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFRotated_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFRotated_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Epi
gfx modify g_element BackTransformedUPFFinalRotated_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Endo
gfx modify g_element BackTransformedUPFFinalRotated_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Epi_ED
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ED general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ED lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ED data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material tissue selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Endo_ED
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ED general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ED lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ED data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material tissue selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Epi_ES
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ES general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ES lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Epi_ES data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material blue selected_material default_selected;

gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Endo_ES
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ES general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ES lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFFinalRotated_Endo_ES data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material blue selected_material default_selected;


gfx cre axes length 100 mat gold
gfx draw axes

gfx create window 1 double_buffer;
gfx modify window 1 image scene default light_model default;
gfx modify window 1 image add_light default;
gfx modify window 1 layout simple ortho_axes z -y eye_spacing 0.25 width 951 height 957;
gfx modify window 1 set current_pane 1;
gfx modify window 1 background colour 0 0 0 texture none;
gfx modify window 1 view parallel eye_point -156.231 129.865 160.987 interest_point 26.5459 -16.8488 12.6939 up_vector -0.745537 -0.553364 -0.371434 view_angle 34.813 near_clipping_plane 2.7735 far_clipping_plane 991.155 relative_viewport ndc_placement -1 1 2 2 viewport_coordinates 0 0 1 1;
gfx modify window 1 overlay scene none;
