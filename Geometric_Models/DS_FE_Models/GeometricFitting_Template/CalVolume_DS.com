	########## Read in the reference model ############
	

	fem def para;r;small;			# Define the parameter file
	fem def coor;r;mapping;			# Define the coordinate file which involves mapping
	fem define region;r;LV_Cubic		# Define the region file

	# Define the regions
	$WALL=1;
	$LV_CAVITY=2;
	
	# Define the basis functions
	fem def base;r;LVBasisV2;
	
	# Define the geometry of the model
	
	fem def node;r;fitted_epi_humanLV region $WALL
	fem def elem;r;fitted_epi_humanLV region $WALL
	
	fem export node;fitted_epi_humanLV_DS as fitted_epi_humanLV_DS region $WALL;
	fem export elem;fitted_epi_humanLV_DS as fitted_epi_humanLV_DS region $WALL;
	
	# Define the basis functions which allow the scale factors to be read in 
	fem define base;r;LVBasisV2ReadSF
	fem define line;w;fitted_epi_humanLV_DS
	fem def base;r;LVBasisV2;
	
	fem list element;LVWall_DS total region $WALL;
	
	########## Define the Cavity Mesh ###########
	fem define region;r;LV_Cubic  
	fem define coordinates;r;LV_Cavity region $LV_CAVITY			
                      
	fem define nodes;r;LVCavity region $LV_CAVITY		# Define the Cavity mesh
	fem define elements;r;LVCavity region $LV_CAVITY		# Define the Cavity mesh

	# Read in the scale factors of the LV cavity
	fem define base;r;LVBasisV2ReadSF
	fem define line;r;fitted_epi_humanLV_DS
	fem define base;r;LVBasisV2
	
	fem export node;LVCavity_DS  as LVCavity_DS region $LV_CAVITY
	fem export element;LVCavity_DS as LVCavity_DS region  $LV_CAVITY

	fem list element;LVCavity_DS total region $LV_CAVITY;

	fem quit;      
