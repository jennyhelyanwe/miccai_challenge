gfx create material muscle_trans normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 0.66 shininess 0.2;

gfx read node LVCanineModel region LVCanineModel
gfx read elem LVCanineModel region LVCanineModel

gfx read node LVCanineModel_Transformed region LVCanineModel_Transformed
gfx read elem LVCanineModel_Transformed region LVCanineModel_Transformed

gfx read node fitted_endo_humanLV region fitted_endo_humanLV
gfx read elem fitted_endo_humanLV region fitted_endo_humanLV

gfx read node fitted_epi_humanLV region fitted_epi_humanLV
gfx read elem fitted_epi_humanLV region fitted_epi_humanLV

gfx read data BackTransformedUPFFinalRotated_Endo
gfx change data_offset 10000
gfx read data BackTransformedUPFFinalRotated_Epi

gfx change data_offset 10000
gfx read data Epifittingerror region Epifittingerror
gfx change data_offset 10000
gfx read data Endofittingerror region Endofittingerror

gfx cre window 1
gfx modify window 1 background colour 1 1 1 texture none
gfx modify g_element "/" general clear;
gfx modify g_element "/" data_points subgroup BackTransformedUPFFinalRotated_Endo coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material gold selected_material default_selected;
gfx modify g_element "/" data_points subgroup BackTransformedUPFFinalRotated_Epi coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material green selected_material default_selected;
gfx modify g_element /LVCanineModel/ general clear;
gfx modify g_element /LVCanineModel/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.3 select_on material default selected_material default_selected render_shaded;
gfx modify g_element /LVCanineModel_Transformed/ general clear;
gfx modify g_element /LVCanineModel_Transformed/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.3 select_on material gold selected_material default_selected render_shaded;
gfx modify g_element /LVCanineModel_Transformed/ surfaces coordinate coordinates tessellation default LOCAL select_on invisible material default selected_material default_selected render_shaded;
gfx modify g_element /fitted_epi_humanLV/ general clear;
gfx modify g_element /fitted_epi_humanLV/ cylinders coordinate coordinates exterior tessellation default LOCAL circle_discretization 6 constant_radius 0.3 select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element /fitted_epi_humanLV/ surfaces coordinate coordinates tessellation default LOCAL select_on material muscle_trans selected_material default_selected render_shaded;
gfx modify g_element /fitted_endo_humanLV/ general clear;
gfx modify g_element /fitted_endo_humanLV/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.3 select_on material green selected_material default_selected render_shaded;
gfx modify g_element /Epifittingerror/ general clear;
gfx modify g_element /Epifittingerror/ data_points coordinate coordinates LOCAL glyph arrow_solid general size "1*1*1" centre 0,0,0 font default orientation error scale_factors "1*1*1" select_on material red selected_material default_selected;
gfx modify g_element /Endofittingerror/ general clear;
gfx modify g_element /Endofittingerror/ data_points coordinate coordinates LOCAL glyph arrow_solid general size "1*1*1" centre 0,0,0 font default orientation error scale_factors "1*1*1" select_on material red selected_material default_selected;


