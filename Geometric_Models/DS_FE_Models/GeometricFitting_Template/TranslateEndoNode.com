
set echo on;
fem def para;r;small;			# Define parameters
fem def coor;r;mapping;			# Define the coordinate file with mapping
fem def base;r;LVBasis;		# Read in the basis functions

## Read in the transformed model after scaling
fem define node;r;LVCanineModel_Transformed
fem define elem;r;LVCanineModel_Transformed

## Extract position of the data 

$Node_14_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$2)}'`


$Node_15_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=595 'NR==line{printf("%.15f",\$2)}'`



$Node_16_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=20 'NR==line{printf("%.15f",\$2)}'`



$Node_17_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$2)}'`


## Extract position of the endo basal nodes

$Node_14_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=476 'NR==line{printf("%.15f",\$NF)}'`


$Node_15_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=505 'NR==line{printf("%.15f",\$NF)}'`


$Node_16_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=534 'NR==line{printf("%.15f",\$NF)}'`


$Node_17_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=563 'NR==line{printf("%.15f",\$NF)}'`


## Calculate the displacement of endo basal nodes

$Node14_x_disp=$Node_14_x-$Node_14_x_ref


$Node15_x_disp=$Node_15_x-$Node_15_x_ref


$Node16_x_disp=$Node_16_x-$Node_16_x_ref


$Node17_x_disp=$Node_17_x-$Node_17_x_ref



## Translate the endo basal nodes

fem change node translate by $Node14_x_disp,0,0 node 14
fem change node translate by $Node15_x_disp,0,0 node 15
fem change node translate by $Node16_x_disp,0,0 node 16
fem change node translate by $Node17_x_disp,0,0 node 17

## Export transformed model
fem define node;w;LVCanineModel_Transformed_EndoTrans
fem define elem;w;LVCanineModel_Transformed_EndoTrans
