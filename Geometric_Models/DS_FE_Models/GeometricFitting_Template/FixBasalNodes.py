"""

This function is designed to fix the epicardial basal nodes of the LV model so that it 
matches with specific data points at the base of the canine data. 

Author: Zhinuo Wang
Date: 03/0602014

"""

import os
import scipy
import numpy
import math
import re
import array
import shutil
from shutil import copy
## Step 1: Find coordinates of selected data points. 
filer = open('DS_Epi.ipdata', 'r')
lines = filer.readlines()
data_points = [1045, 1126, 1134, 1215];
node = numpy.zeros((4,3));
for i in range(0, len(data_points)):
    node[i,0] = float(lines[data_points[i]].split()[1]);
    node[i,1] = float(lines[data_points[i]].split()[2]);
    node[i,2] = float(lines[data_points[i]].split()[3]);
filer.close()

## Step 2: Modify model node locations to match these data points before
# any further geometric fitting. 
copy('LVCanineModel_Rotated.ipelem', 'LVCanineModel_Transformed.ipelem')
filer = open('LVCanineModel_Rotated.ipnode', 'r')
filew = open('LVCanineModel_Transformed.ipnode', 'w')

num_line =1
data = filer.readline()
while (num_line<=1165):
    if (num_line==1053):
        string = ' The Xj(1) coordinate is [ 0.45289E+01]:   '+str(node[0,0])+'\n'
    elif(num_line==1062):
        string = ' The Xj(2) coordinate is [-0.14359E+01]:    '+str(node[0,1])+'\n'
    elif(num_line==1071):
        string = ' The Xj(3) coordinate is [-0.14359E+01]:    '+str(node[0,2])+'\n'
    elif(num_line==1082):
        string = ' The Xj(1) coordinate is [-0.14359E+01]:    '+str(node[1,0])+'\n'
    elif(num_line==1091):
        string = ' The Xj(2) coordinate is [-0.14359E+01]:    '+str(node[1,1])+'\n'
    elif(num_line==1100):
        string = ' The Xj(3) coordinate is [-0.14359E+01]:    '+str(node[1,2])+'\n'
    elif(num_line==1111):
        string = ' The Xj(1) coordinate is [-0.14359E+01]:    '+str(node[2,0])+'\n'
    elif(num_line==1120):
        string = ' The Xj(2) coordinate is [-0.14359E+01]:    '+str(node[2,1])+'\n'
    elif(num_line==1129):
        string = ' The Xj(3) coordinate is [-0.14359E+01]:    '+str(node[2,2])+'\n'
    elif(num_line==1140):
        string = ' The Xj(1) coordinate is [-0.14359E+01]:    '+str(node[3,0])+'\n'
    elif(num_line==1149):
        string = ' The Xj(2) coordinate is [-0.14359E+01]:    '+str(node[3,1])+'\n'
    elif(num_line==1158):
        string = ' The Xj(3) coordinate is [-0.14359E+01]:    '+str(node[3,2])+'\n'
    else:
        string = data
    filew.write(str(string))
    data = filer.readline()
    num_line = num_line + 1

print '== Finished adjusting epi nodes ==\n'

filer.close()
filew.close()

