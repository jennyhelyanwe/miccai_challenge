"""

This function is designed to calculate the angles of rigid body rotation required to 
align the apex of the model with the apex of the canine data. 

Author: Zhinuo Wang
Date: 02/06/2014

"""
import os
import scipy
import numpy
import math
import re

from scipy import array

## Step 1: Find coordinates of model apex (epi). 
filer = open('LVCanineModel_Scaled.ipnode','r')
lines = filer.readlines()
regularapex_x = float(lines[592].split()[5]);
regularapex_y = float(lines[629].split()[5]);
regularapex_z = float(lines[666].split()[6]);
filer.close()
print regularapex_x, regularapex_y, regularapex_z

#The data numbers used here apply only 
# to the MICCAI challenge canine data. 
filer = open('DS_Epi.ipdata', 'r')
lines = filer.readlines()
targetapex_x = float(lines[1].split()[1]);
targetapex_y = (float(lines[1].split()[2])+float(lines[90].split()[2]))/2.0;
targetapex_z = (float(lines[82].split()[3])+float(lines[171].split()[3]))/2.0;
filer.close()
print targetapex_x, targetapex_y, targetapex_z

xy_angle = (math.atan(targetapex_y/regularapex_x)-math.atan(regularapex_y/regularapex_x))*180/3.1415
xz_angle = (math.atan(targetapex_z/regularapex_x)-math.atan(regularapex_z/regularapex_x))*180/3.1415
print xy_angle, xz_angle

filew = open('AlignApex.TRN', 'w')
filew.write("%(x)3.10f," %{'x':xy_angle})
filew.write("%(x)3.10f" %{'x':xz_angle})
filew.close()
