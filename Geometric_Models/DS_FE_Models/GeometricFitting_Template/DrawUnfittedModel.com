gfx modify spectrum default clear overwrite_colour;
gfx modify spectrum default linear reverse range 0 1 extend_above extend_below rainbow colour_range 0 1 component 1;
gfx create material black normal_mode ambient 0 0 0 diffuse 0 0 0 emission 0 0 0 specular 0.3 0.3 0.3 alpha 1 shininess 0.2;
gfx create material blue normal_mode ambient 0 0 0.5 diffuse 0 0 1 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.2;
gfx create material bone normal_mode ambient 0.7 0.7 0.6 diffuse 0.9 0.9 0.7 emission 0 0 0 specular 0.1 0.1 0.1 alpha 1 shininess 0.2;
gfx create material default normal_mode ambient 1 1 1 diffuse 1 1 1 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;
gfx create material default_selected normal_mode ambient 1 0.2 0 diffuse 1 0.2 0 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;
gfx create material gold normal_mode ambient 1 0.4 0 diffuse 1 0.7 0 emission 0 0 0 specular 0.5 0.5 0.5 alpha 1 shininess 0.3;
gfx create material gray50 normal_mode ambient 0.5 0.5 0.5 diffuse 0.5 0.5 0.5 emission 0.5 0.5 0.5 specular 0.5 0.5 0.5 alpha 1 shininess 0.2;
gfx create material green normal_mode ambient 0 0.5 0 diffuse 0 1 0 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.1;
gfx create material muscle normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 1 shininess 0.2;
gfx create material red normal_mode ambient 0.5 0 0 diffuse 1 0 0 emission 0 0 0 specular 0.2 0.2 0.2 alpha 1 shininess 0.2;
gfx create material silver normal_mode ambient 0.4 0.4 0.4 diffuse 0.7 0.7 0.7 emission 0 0 0 specular 0.5 0.5 0.5 alpha 1 shininess 0.3;
gfx create material tissue normal_mode ambient 0.9 0.7 0.5 diffuse 0.9 0.7 0.5 emission 0 0 0 specular 0.2 0.2 0.3 alpha 1 shininess 0.2;
gfx create material transparent_gray50 normal_mode ambient 0.5 0.5 0.5 diffuse 0.5 0.5 0.5 emission 0.5 0.5 0.5 specular 0.5 0.5 0.5 alpha 0 shininess 0.2;
gfx create material white normal_mode ambient 1 1 1 diffuse 1 1 1 emission 0 0 0 specular 0 0 0 alpha 1 shininess 0;


gfx read node;LVCanineModel
gfx read elem;LVCanineModel

gfx modify g_element LVCanineModel general clear circle_discretization 6 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;
gfx modify g_element LVCanineModel lines select_on material default selected_material default_selected;
gfx modify g_element LVCanineModel cylinders constant_radius 0.3 select_on material default selected_material default_selected render_shaded;
gfx modify g_element LVCanineModel surfaces face xi3_0 select_on material default selected_material default_selected render_shaded;


gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000

gfx read node;LVCanineModel_Transformed
gfx read elem;LVCanineModel_Transformed
gfx modify g_element LVCanineModel_Transformed general clear circle_discretization 6 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;
gfx modify g_element LVCanineModel_Transformed lines select_on material default selected_material default_selected;
gfx modify g_element LVCanineModel_Transformed cylinders constant_radius 0.3 select_on material default selected_material default_selected render_shaded;
gfx modify g_element LVCanineModel_Transformed surfaces face xi3_0 select_on material green selected_material default_selected render_shaded;


gfx read data /hpc/ywan215/RicordoProject/RegistrationBetweenDataSets/ModelTree_pa129/BackTransformedCanineSurfaceRotated_ModelTree_Epi
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data /hpc/ywan215/RicordoProject/RegistrationBetweenDataSets/ModelTree_pa129/BackTransformedCanineSurfaceRotated_ModelTree_Endo
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedCanineSurfaceRotated_ModelTree_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material blue selected_material default_selected;

gfx change data_offset 10000
gfx read data /hpc/ywan215/RicordoProject/RegistrationBetweenDataSets/ModelTree_pa129/BackTransformedUPFSurfaceFinalRotated_ModelTree_Epi
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Epi general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Epi lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Epi data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material gold selected_material default_selected;

gfx change data_offset 10000
gfx read data /hpc/ywan215/RicordoProject/RegistrationBetweenDataSets/ModelTree_pa129/BackTransformedUPFSurfaceFinalRotated_ModelTree_Endo
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Endo general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Endo lines select_on material default selected_material default_selected;
gfx modify g_element BackTransformedUPFSurfaceFinalRotated_ModelTree_Endo data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material blue selected_material default_selected;

gfx cre axes length 50 mat gold
gfx draw axes

gfx create window 1 double_buffer;
gfx modify window 1 image scene default light_model default;
gfx modify window 1 image add_light default;
gfx modify window 1 layout simple ortho_axes z -y eye_spacing 0.25 width 1475 height 973;
gfx modify window 1 set current_pane 1;
gfx modify window 1 background colour 0 0 0 texture none;
gfx modify window 1 view parallel eye_point 31.8415 81.4003 -237.577 interest_point 14.8275 -2.4114 13.4424 up_vector -0.997328 -0.0128927 -0.0719032 view_angle 48.3176 near_clipping_plane 2.65188 far_clipping_plane 947.691 relative_viewport ndc_placement -1 1 2 2 viewport_coordinates 0 0 1 1;
gfx modify window 1 overlay scene none;
gfx modify window 1 set data_tool current_pane 1 std_view_angle 40 normal_lines no_antialias depth_of_field 0.0 fast_transparency blend_normal;
