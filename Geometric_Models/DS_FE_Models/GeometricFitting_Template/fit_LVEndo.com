###########################   FEM starts  ###############################

fem def data;r;DS_Endo		# Read in data file
fem exp data;DS_Endo as DS_Endo;

fem group faces 4,8,12,15,20,24,28,31,36,40,44,47,52,56,60,63 as EXTERNAL;

fem def xi;c closest_face faces EXTERNAL;		# Calculate the value for xi
fem export data;EndoInitialError as EndoInitialError error;

fem list data error;
set output;Endofittingerror on

for ($fit_itt=1; $fit_itt<=$tot_itt; $fit_itt++)
     {
	fem def fit;r;TestSANoSMEndoCorrect geometry faces EXTERNAL;		# Read in the ipfit file
	fem define mapping;r;Endomapping;					# Read in the mapping file
	fem fit;
	fem update node fit					# Update the node value after fitting
	fem update scale_factor normalise
	fem def xi;c closest_face faces EXTERNAL; 		# Calculate xi
	fem list data error					# List the error
	system "echo ' =========================' " 
   	system "echo ' ITERATION ${fit_itt} DONE' "
   	system "echo ' =========================' " 
     }

set out off

################# Update the S3 derivatives again
fem update nodes derivative 3 linear node 2..13,19..30;

fem exp node;fitted_endo_humanLV as fitted_endo_humanLV;		# Export the node after fitting
fem exp elem;fitted_endo_humanLV as fitted_endo_humanLV;		# Export the element after fitting
fem define node;w;fitted_endo_humanLV;			# Write out the nodes 
fem define elem;w;fitted_endo_humanLV;			# Write out the elements

fem export data;Endofittingerror as Endofittingerror error 
