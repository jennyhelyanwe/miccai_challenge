set echo on;
fem def para;r;small;			# Define parameters
fem def coor;r;mapping;			# Define the coordinate file with mapping
fem def base;r;LVBasis;		# Read in the basis functions

fem def node;r;fitted_endo_humanLV;	# Read in the previous fitted node file
fem def elem;r;fitted_endo_humanLV;	# Readin the previous fitted element file


$Node_14_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=476 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=485 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=494 'NR==line{printf("%.15f",\$NF)}'`

$Node_15_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=505 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=514 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=523 'NR==line{printf("%.15f",\$NF)}'`

$Node_17_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=563 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=572 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=581 'NR==line{printf("%.15f",\$NF)}'`

$Node_14_x_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=476 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_y_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=485 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_z_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=494 'NR==line{printf("%.15f",\$NF)}'`


$Node_15_x_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=505 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_y_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=514 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_z_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=523 'NR==line{printf("%.15f",\$NF)}'`

$Node_17_x_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=563 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_y_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=572 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_z_def=`sed -e s%D%E% fitted_endo_humanLV.ipnode | awk -v line=581 'NR==line{printf("%.15f",\$NF)}'`

$Node14_x_disp=$Node_14_x_def-$Node_14_x_ref
$Node14_y_disp=$Node_14_y_def-$Node_14_y_ref
$Node14_z_disp=$Node_14_z_def-$Node_14_z_ref

$Node15_x_disp=$Node_15_x_def-$Node_15_x_ref
$Node15_y_disp=$Node_15_y_def-$Node_15_y_ref
$Node15_z_disp=$Node_15_z_def-$Node_15_z_ref

$Node17_x_disp=$Node_17_x_def-$Node_17_x_ref
$Node17_y_disp=$Node_17_y_def-$Node_17_y_ref
$Node17_z_disp=$Node_17_z_def-$Node_17_z_ref

fem change node translate by $Node14_x_disp,$Node14_y_disp,$Node14_z_disp node 31
fem change node translate by $Node15_x_disp,$Node15_y_disp,$Node15_z_disp node 32
fem change node translate by $Node17_x_disp,$Node17_y_disp,$Node17_z_disp node 34

#fem update nodes derivative 3 linear;

fem define node;w;fitted_endo_humanLV_Translated
fem define elem;w;fitted_endo_humanLV_Translated
fem export node;fitted_endo_humanLV_Translated as fitted_endo_humanLV_Translated
fem export elem;fitted_endo_humanLV_Translated as fitted_endo_humanLV_Translated
