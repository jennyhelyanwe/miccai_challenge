
###########################   FEM starts  ###############################

fem def data;r;DS_Epi				
# Read in data file
fem exp data;DS_Epi as DS_Epi;

fem group faces 5,9,13,16,21,25,29,32,37,41,45,48,53,57,61,64 as EXTERNAL;

fem def xi;c closest_face faces EXTERNAL search_start 20;		# Calculate the value for xi
fem export data;EpiInitialError as EpiInitialError error;

fem list data error;
set output;Epifittingerror on

for ($fit_itt=1; $fit_itt<=$tot_itt; $fit_itt++)
     {
	fem def fit;r;TestSANoSMCorrect geometry faces EXTERNAL;		# Read in the ipfit file
	fem define mapping;r;Epimapping			# Read in the mapping file
	fem fit;
	fem update node fit					# Update the node value after fitting
	fem update scale_factor normalise			# Update scale factor	
	fem def xi;c closest_face faces EXTERNAL search_start 20; 		# Calculate xi
	fem def xi;w;EpiProjection closest_face faces EXTERNAL search_start 20;
	fem li data error					# List the error
	system "echo ' =========================' " 
   	system "echo ' ITERATION ${fit_itt} DONE' "
   	system "echo ' =========================' " 
     }
     
set out off

################# Update the S3 derivatives again
fem update nodes derivative 3 linear node 2..13,19..30;

fem exp node;fitted_epi_humanLV as fitted_epi_humanLV;		# Export the node after fitting
fem exp elem;fitted_epi_humanLV as fitted_epi_humanLV;		# Export the element after fitting
fem define node;w;fitted_epi_humanLV		# Write out the nodes 
fem define elem;w;fitted_epi_humanLV		# Write out the elements

fem export data;Epifittingerror as Epifittingerror error 
