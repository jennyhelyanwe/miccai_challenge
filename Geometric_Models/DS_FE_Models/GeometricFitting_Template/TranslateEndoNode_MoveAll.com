
set echo on;
fem def para;r;small;			# Define parameters
fem def coor;r;mapping;			# Define the coordinate file with mapping
fem def base;r;LVBasisV2;		# Read in the basis functions

## Read in the transformed model after scaling
fem define node;r;LVCanineModel_Transformed
fem define elem;r;LVCanineModel_Transformed

## Extract position of the data 

$Node_14_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$2)}'`
$Node_14_y=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$3)}'`
$Node_14_z=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$4)}'`

$Node_15_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=595 'NR==line{printf("%.15f",\$2)}'`
$Node_15_y=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=595 'NR==line{printf("%.15f",\$3)}'`
$Node_15_z=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=595 'NR==line{printf("%.15f",\$4)}'`


$Node_16_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=20 'NR==line{printf("%.15f",\$2)}'`
$Node_16_y=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=20 'NR==line{printf("%.15f",\$3)}'`
$Node_16_z=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=20 'NR==line{printf("%.15f",\$4)}'`


$Node_17_x=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$2)}'`
$Node_17_y=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$3)}'`
$Node_17_z=`sed -e s%D%E% BackTransformedUPFFinalRotated_Endo.ipdata | awk -v line=53 'NR==line{printf("%.15f",\$4)}'`

## Extract position of the endo basal nodes

$Node_14_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=476 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=485 'NR==line{printf("%.15f",\$NF)}'`
$Node_14_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=494 'NR==line{printf("%.15f",\$NF)}'`

$Node_15_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=505 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=514 'NR==line{printf("%.15f",\$NF)}'`
$Node_15_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=523 'NR==line{printf("%.15f",\$NF)}'`

$Node_16_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=534 'NR==line{printf("%.15f",\$NF)}'`
$Node_16_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=543 'NR==line{printf("%.15f",\$NF)}'`
$Node_16_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=552 'NR==line{printf("%.15f",\$NF)}'`

$Node_17_x_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=563 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_y_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=572 'NR==line{printf("%.15f",\$NF)}'`
$Node_17_z_ref=`sed -e s%D%E% LVCanineModel_Transformed.ipnode | awk -v line=581 'NR==line{printf("%.15f",\$NF)}'`

## Calculate the displacement of endo basal nodes

$Node14_x_disp=$Node_14_x-$Node_14_x_ref
$Node14_y_disp=$Node_14_y-$Node_14_y_ref
$Node14_z_disp=$Node_14_z-$Node_14_z_ref

$Node15_x_disp=$Node_15_x-$Node_15_x_ref
$Node15_y_disp=$Node_15_y-$Node_15_y_ref
$Node15_z_disp=$Node_15_z-$Node_15_z_ref

$Node16_x_disp=$Node_16_x-$Node_16_x_ref
$Node16_y_disp=$Node_16_y-$Node_16_y_ref
$Node16_z_disp=$Node_16_z-$Node_16_z_ref

$Node17_x_disp=$Node_17_x-$Node_17_x_ref
$Node17_y_disp=$Node_17_y-$Node_17_y_ref
$Node17_z_disp=$Node_17_z-$Node_17_z_ref


## Translate the endo basal nodes

fem change node translate by $Node14_x_disp,$Node14_y_disp,$Node14_z_disp node 14
fem change node translate by $Node15_x_disp,$Node15_y_disp,$Node15_z_disp node 15
fem change node translate by $Node16_x_disp,$Node16_y_disp,$Node16_z_disp node 16
fem change node translate by $Node17_x_disp,$Node17_y_disp,$Node17_z_disp node 17

## Export transformed model
fem define node;w;LVCanineModel_Transformed_EndoTrans
fem define elem;w;LVCanineModel_Transformed_EndoTrans
