# Create one window to display the fitted mesh to -axis data
gfx cre window 1;

# Add the scene editor
gfx edit scene;

# Read in the node file
gfx read nodes LVCanineModel_Transformed

# Read in the element file
gfx read elements LVCanineModel_Transformed

gfx  modify g_element LVCanineModel_Transformed general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element LVCanineModel_Transformed node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element LVCanineModel_Transformed element_points; 
gfx modify g_element LVCanineModel_Transformed cylinders constant_radius 0.2 material default;
gfx modify g_element LVCanineModel_Transformed surfaces select_on material muscle;


gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000

# Read in the node file
gfx read nodes LVCanineModel_Transformed_EndoTrans

# Read in the element file
gfx read elements LVCanineModel_Transformed_EndoTrans

gfx  modify g_element LVCanineModel_Transformed_EndoTrans general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element LVCanineModel_Transformed_EndoTrans node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element LVCanineModel_Transformed_EndoTrans element_points; 
gfx modify g_element LVCanineModel_Transformed_EndoTrans cylinders constant_radius 0.2 material default;
gfx modify g_element LVCanineModel_Transformed_EndoTrans surfaces select_on material muscle;

gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000

# Read in the node file
gfx read nodes fitted_endo_humanLV

# Read in the element file
gfx read elements fitted_endo_humanLV

gfx  modify g_element fitted_endo_humanLV general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element fitted_endo_humanLV node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element fitted_endo_humanLV element_points; 
gfx modify g_element fitted_endo_humanLV cylinders constant_radius 0.2 material gold;
gfx modify g_element fitted_endo_humanLV surfaces select_on material gold;

gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000


# Read in the node file
gfx read nodes fitted_endo_humanLV_Translated

# Read in the element file
gfx read elements fitted_endo_humanLV_Translated

gfx  modify g_element fitted_endo_humanLV_Translated general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element fitted_endo_humanLV_Translated node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element fitted_endo_humanLV_Translated element_points; 
gfx modify g_element fitted_endo_humanLV_Translated cylinders constant_radius 0.2 material gold;
gfx modify g_element fitted_endo_humanLV_Translated surfaces select_on material gold;

gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000


# Read in the node file
gfx read nodes fitted_epi_humanLV

# Read in the element file
gfx read elements fitted_epi_humanLV

gfx  modify g_element fitted_epi_humanLV general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element fitted_epi_humanLV node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element fitted_epi_humanLV element_points; 
gfx modify g_element fitted_epi_humanLV cylinders constant_radius 0.2 material green;
gfx modify g_element fitted_epi_humanLV surfaces select_on material green;



gfx change node_offset 1000
gfx change face_offset 1000
gfx change element_offset 1000


## Read in the node file
gfx read nodes LVCavity

## Read in the element file
gfx read elements LVCavity

gfx  modify g_element LVCavity general clear circle_discretization 48 default_coordinate coordinates element_discretization "12*12*12" native_discretization none;

gfx modify g_element LVCavity node_points glyph sphere general size "2*2*2" centre 0,0,0 select_on material default;
gfx modify g_element LVCavity element_points; 
gfx modify g_element LVCavity cylinders constant_radius 0.2 material muscle;
gfx modify g_element LVCavity surfaces select_on material muscle;


# Change some visualization paramaters
gfx modify window 1 image view_all;

gfx mod win 1 background colour 1 1 1

# Read in the epicardial -axis data file
gfx read data BackTransformedUPFFinalRotated_Epi
gfx modify g_element BackTransformedUPFFinalRotated_Epi data_points glyph sphere general size "1*1*1" centre 0,0,0 select_on material green;

gfx change data_offset 10000000

# Read in the epicardial -axis data file
gfx read data BackTransformedUPFFinalRotated_Endo
gfx modify g_element BackTransformedUPFFinalRotated_Endo data_points glyph sphere general size "1*1*1" centre 0,0,0 select_on material gold;




