#set echo on;
system('rm *.*~');

###################################### Define auxiliary parameters ################################
# Define the total number of iterations required
$tot_itt=5;

# Set up manual mapping 
$manual_mappings = 1;
###################################################################################################

####################################### FEM starts ################################################
fem def para;r;small;			# Define the parameter file
fem def coor;r;mapping;			# Define the coordinate file which involves mapping

##################### Read in the LV canine model ################################
fem define node;r;LVCanineModel
fem def base;r;LVBasis;			# Define the basis functions
fem define elem;r;LVCanineModel;

fem update nodes derivative 1 versions individual;	# Update the derivative 1 with versions
fem update nodes derivative 2 versions individual;	# Update the derivative 2 with versions
fem update nodes derivative 3 versions individual;	# Update the derivative 3 with versions
fem update scale_factor normalise;			# Update the scale factor

## Output the untransformed model
fem export node;LVCanineModel as LVCanineModel;
fem export elem;LVCanineModel as LVCanineModel;

## Read in the scaling vector from python
CORE::open( MYFILE,'TransformationMatrix_Scaling.TRN' ) or die( "ERROR: Unable to open output file\n" );
while (<MYFILE>) {
 	chomp;
 	$transformation= "$_\n";
 }
close (MYFILE); 
$scale= substr $transformation, 41,-29;
$translate=substr $transformation, 0,40;
#$zrotation=substr $transformation, -28, -16;
#$yrotation=substr $transformation, -15, -2;

print $scale
print $translate

fem change node translate by $translate
fem export node;LVCanineModel_Translated
fem export elem;LVCanineModel_Translated

fem change node scale by $scale
fem export node;LVCanineModel_Scaled
fem export elem;LVCanineModel_Scaled
fem define node;w;LVCanineModel_Scaled
fem define elem;w;LVCanineModel_Scaled

## Calculate angle to align apex. 
system('python AlignApex.py')

CORE::open(MYFILE, 'AlignApex.TRN') or die("ERROR: Unable to open output file\n");
while (<MYFILE>)  {
    chomp;
    $rotation= "$_\n";
}
close (MYFILE);

$zrotation=substr $rotation, 0, 12;
$yrotation=substr $rotation, 13, -1;

print $zrotation
print $yrotation

fem change node rotate by $zrotation about 0,0,0 axis 0,0,1
fem change node rotate by -$yrotation about 0,0,0 axis 0,1,0

fem define node;w;LVCanineModel_Rotated
fem define elem;w;LVCanineModel_Rotated


## Update derivatives after transformation
#fem update nodes derivative 1 versions individual;	# Update the derivative 1 with versions
#fem update nodes derivative 2 versions individual;	# Update the derivative 2 with versions
#fem update nodes derivative 3 versions individual;	# Update the derivative 3 with versions
#fem update scale_factor normalise;			# Update the scale factor
## Fix 4 epicardial basal nodes before fitting. 
system('python FixBasalNodes.py')

fem define node;r;LVCanineModel_Transformed
fem define elem;r;LVCanineModel_Transformed
fem exp node;LVCanineModel_Transformed as LVCanineModel_Transformed;	# Export the nodes for unfitted  
fem exp elem;LVCanineModel_Transformed as LVCanineModel_Transformed;	# Export the elements for unfitted 


fem reallocate				# Reallocate memories

fem def para;r;small;			# Define the parameter file
fem def coor;r;mapping;			# Define the coordinate file which involves mapping
fem def base;r;LVBasis;			# Define the basis functions

##################### Read in the LV canine model ################################

## Read in the model
fem define node;r;LVCanineModel_Transformed
fem define elem;r;LVCanineModel_Transformed

fem update field from geometry;		# Updat the filed according to the current geometry
fem def elem;d field; 			# Create an element field matching the geometry 



###################### Fit the Endosurface now (-axis data) #######################

system "echo ' ==================================================' " 
system "echo '           Start To Fit Endocardial Surface         ' "
system "echo ' ==================================================' " 

read commands;fit_LVEndo;
fem list element total

fem reallocate				# Reallocate memories

###########################   FEM starts  ###############################

fem def para;r;small;			# Define parameters
fem def coor;r;mapping;			# Define the coordinate file with mapping
fem def base;r;LVBasis;		# Read in the basis functions

fem define node;r;fitted_endo_humanLV
fem define elem;r;fitted_endo_humanLV

fem update field from geometry;		# Update the filed according to current geometry
fem def elem;d field;			#/Create an elemet field matching geometry

###################### Fit the Episurface now	 #######################

system "echo ' ==================================================' " 
system "echo '          Start To Fit Epicardial Surface         ' "
system "echo ' ==================================================' " 


read commands;fit_LVEpi;

fem list element total


fem quit
