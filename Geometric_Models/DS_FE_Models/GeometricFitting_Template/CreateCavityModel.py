"""
This function is designed to create the ipnode file representing the LV cavity
Author: Vicky Wang 
"""

import string
import scipy
from scipy import append,array

##############################################################
def CreateCavityModel(fileName_wall,fileName_cavity):

	try:
			file_wall = open( fileName_wall, 'r' )
			file_cavity=open( fileName_cavity,'w')
	except IOError:
			print 'ERROR: CreateCavityModel: unable to open', fileName_wall
			return
			
	## Read a single line at a time
	no_line=0
	data = file_wall.readline()
	no_line=no_line+1
	
	## Initialise a variable to store the x coordinates of the basal nodes
	x_coor_base=[]
	x_coor_apex=[]
	
	while (no_line<=588): ## Line 588 is the end of node 17
			if (no_line==16): ## x-coordinate of Node 1
				data_coor_tmp=data.split()
				data_coor=data_coor_tmp[len(data_coor_tmp)-1]
				x_coor_apex.append(float(data_coor))			
			elif (no_line==476): ## x-coordinate of Node 14
				data_coor_tmp=data.split()
				data_coor=data_coor_tmp[len(data_coor_tmp)-1]
				x_coor_base.append(float(data_coor))
			elif (no_line==505): ## x-coordinate of Node 15
				data_coor_tmp=data.split()
				data_coor=data_coor_tmp[len(data_coor_tmp)-1]
				x_coor_base.append(float(data_coor))
			elif (no_line==534): ## x-coordinates of Node 16
				data_coor_tmp=data.split()
				data_coor=data_coor_tmp[len(data_coor_tmp)-1]
				x_coor_base.append(float(data_coor))
			elif (no_line==563): ## x-coordinate of Node 17
				data_coor_tmp=data.split()
				data_coor=data_coor_tmp[len(data_coor_tmp)-1]
				x_coor_base.append(float(data_coor))			
								
			if (no_line==4):
				string=' The number of nodes is [    21]:     21\n'
			else :
				string=data
				
			file_cavity.write(str(string))
			data = file_wall.readline()
			no_line=no_line+1
			
	file_wall.close()
	x_coor_apex=array(x_coor_apex)
	x_coor_base=array(x_coor_base)
	print 'The x-coordinate of the apex is ',float(x_coor_apex)
	print 'The x-coordinates of the base is ',x_coor_base
	## Calculate the mean coordinate of the basal node
	x_coor_mean=scipy.mean(x_coor_base)
	#x_coor_mean=(float(x_coor_base[0])+float(x_coor_base[1])+float(x_coor_base[2])+float(x_coor_base[3]))/4
	print 'The mean x-coordinate of the base is ',float(x_coor_mean)
	## Calculate the base-to-apex dimension
	base_apex_dimen=x_coor_apex-x_coor_mean
	print 'The Base-To-Apex dimension is ',float(base_apex_dimen)
	### Calculate the coordinates of other nodes in the cavity
	x_coor_118=float(x_coor_apex-4*base_apex_dimen/4)
	x_coor_119=float(x_coor_apex-3*base_apex_dimen/4)
	x_coor_120=float(x_coor_apex-2*base_apex_dimen/4)
	x_coor_121=float(x_coor_apex-1*base_apex_dimen/4)
	print 'The x-coordinate of node 118 is ',x_coor_118
	print 'The x-coordinate of node 119 is ',x_coor_119
	print 'The x-coordinate of node 120 is ',x_coor_120
	print 'The x-coordinate of node 121 is ',x_coor_121
	
	## Add these information to the cavity ipnode file
	string=' \n'
	file_cavity.write(str(string))
	string=' Node number [  118]:   118\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=1 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_118) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_118) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_118) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_118) + '\n'
	file_cavity.write(str(string))		
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=2 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=3 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string='  \n'
	file_cavity.write(str(string))
	string=' Node number [  119]:   119\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=1 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_119) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_119) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_119) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_119) + '\n'
	file_cavity.write(str(string))		
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=2 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=3 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string='  \n'
	file_cavity.write(str(string))
	string=' Node number [  120]:   120\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=1 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_120) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_120) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_120) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_120) + '\n'
	file_cavity.write(str(string))		
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=2 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=3 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string='  \n'
	file_cavity.write(str(string))
	string=' Node number [  121]:   121\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=1 is [1]:  8\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))		
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 5:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 6:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 7:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 8:\n'
	file_cavity.write(str(string))
	string=' The Xj(1) coordinate is [ 0.00000E+00]:                 '+ str(x_coor_121) + '\n'
	file_cavity.write(str(string))		
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=2 is [1]:  8\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 5:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 6:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 7:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 8:\n'
	file_cavity.write(str(string))
	string=' The Xj(2) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The number of versions for nj=3 is [1]:  4\n'
	file_cavity.write(str(string))
	string=' For version number 1:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 2:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 3:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 4:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 5:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))        
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 6:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 7:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' For version number 8:\n'
	file_cavity.write(str(string))
	string=' The Xj(3) coordinate is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 1 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 2 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt direction 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	string=' The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]:                         0\n'
	file_cavity.write(str(string))
	
	file_cavity.close()
	
	return
