set dir /hpc/zwan145/MICCAI_Challenge/Geometric_Models/DS_FE_Models
gfx read node CreateRegularModel/rc.exnode
gfx read elem CreateRegularModel/rc.exelem

# Read regular model data
gfx read data CreateRegularModel/LVRegular_Endo.exdata
gfx change data_offset 10000
gfx read data CreateRegularModel/LVRegular_Epi.exdata
gfx change data_offset 10000

# Read untransformES challenge data.
gfx read data ../Surface_Data/0912/ES_Endo.exdata
gfx change data_offset 10000
gfx read data ../Surface_Data/0912/ES_Epi.exdata
gfx change data_offset 10000

# Read registerES challenge data
gfx read data ../Surface_Data/0912/ES_Endo_registered.exdata
gfx change data_offset 10000
gfx read data ../Surface_Data/0912/ES_Epi_registered.exdata
gfx change data_offset 10000

gfx cre window 1


gfx modify g_element "/" general clear;
gfx modify g_element "/" data_points subgroup LVRegular_Endo coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on invisible material gold selected_material default_selected;
gfx modify g_element "/" data_points subgroup LVRegular_Epi coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material gold selected_material default_selected;
gfx modify g_element "/" data_points subgroup ES_Endo coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on invisible material green selected_material default_selected;
gfx modify g_element "/" data_points subgroup ES_Epi coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material green selected_material default_selected;
gfx modify g_element "/" data_points subgroup RegisteredEndo coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on invisible material blue selected_material default_selected;
gfx modify g_element "/" data_points subgroup RegisteredEpi coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material blue selected_material default_selected;

