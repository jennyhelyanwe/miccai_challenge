package coordConversion;

use PDL;
use Math::Trig;
use PDL::Math "floor";
use PDL::Core "sclr";
use PDL::Core "topdl";


# Function prolate2rc converts prolate coords to rc
# NB: prolate coords mu and theta must be in radians
sub prolate2rc
{
  my ($d, $lambda, $mu, $theta) = @_;

  my $x;
  my $y;
  my $z;
  
  $x = $d*cosh($lambda)*cos($mu);
  $y = $d*sinh($lambda)*sin($mu)*cos($theta);
  $z = $d*sinh($lambda)*sin($mu)*sin($theta);  
  #print "$x, $y, $z\n";
  return ($x, $y, $z);  
}


# Function rc2prolate converts rc coords to prolate
# NB: prolate coords mu and theta are returned as radians
sub rc2prolate
{
  my ($d, $x, $y, $z) = @_;

  my $lambda;
  my $mu;
  my $theta;

  my $a1;
  my $a2;
  my $a3;
  my $a4;
  my $a5;
  my $a6;
  my $a7;
  my $a8;
  my $a9;

  
  $a1 = $x**2+$y**2+$z**2-$d**2;
  $a2 = sqrt($a1**2+4*$d**2*($y**2+$z**2));
  $a3 = 2*$d**2;
  $a4 = max(pdl [[($a2+$a1)/$a3], [0]]);
  $a5 = max(pdl [[($a2-$a1)/$a3], [0]]);
  $a6 = sqrt($a4);
  $a7 = min(pdl [[sqrt($a5)], [1]]);
  if(abs($a7) <= 1)
  {
    $a8 = asin($a7);
  }
  else
  {
    $a8 = 0;
    print "\nWarning: rc2prolate: Put $a8=0 since abs($a7)>1\n";
  }
  if( ($z==0) || ($a6==0) || ($a7==0) )
  {
    $a9 = 0;
  }
  else
  {
    if( abs($a6*$a7) > 0)
    {
      $a9 = $z/($d*$a6*$a7);
    }
    else
    {
      $a9 = 0;
      print "\nWarning: rc2prolate: Put $a9=0 since $a6*$a7=0\n";
    }
    if( $a9 >= 1 )
    {
      $a9 = pi/2;
    }
    elsif( $a9 <= -1 )
    {
      $a9 = -(pi/2);
    }
    else
    {
      $a9 = asin($a9);
    }
  }
  $lambda = log($a6+sqrt($a4+1));
  if( $x > 0 )
  {
    $mu = $a8;
  }
  else
  {
    $mu = pi-$a8;
  }
  if( $y >= 0 )
  {
    #$theta = ($a9+2*pi) % (2*pi); # % is the remainder (modulus) operator (mod in matlab).
    #The above line don't work. Try matlab-expression instead:
    $theta = ($a9+2*pi) - (2*pi)*floor(($a9+2*pi)/(2*pi));
  }
  else
  {
    $theta = pi - $a9;
  }
  #print (ref $theta), "\n";
  # For some unknown reason $theta or $a9 is SOMETIMES a pdl...
  # Convert to pdl (if not pdl: topdl) and take scalar value:
  my $tmp = topdl $theta;
  $theta = $tmp->sclr;

  #print $lambda, "\n";
  #print $mu, "\n";
  #print $theta, "\n";
  return ($lambda, $mu, $theta);  
}

1;
 
__END__
