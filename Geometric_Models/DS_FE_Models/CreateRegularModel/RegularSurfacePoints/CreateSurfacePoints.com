#### This com file creates surface data points for the generic geometric model based on the number of element points specified by the ipxi file. 

#### Read in the set-up files
fem define para;r;small;		# Define the parameter file
fem define coor;r;mapping;		# Define the coordinate file which involves mapping
fem define base;r;LVBasis

#### Read in the geometric model
fem define node;r;../rc
fem define elem;r;../rc

#### Read in the local FE coordinates for endocardial surface #######
fem define data;r;Temp			# This is just a temporary data file
fem define xi;r;Surface_Points_Endo
fem define data;c from_xi
fem define data;w;../LVRegular_Endo
fem list data statistics
fem export data;../LVRegular_Endo as LVRegular_Endo

#### Read in the local FE coordinates for epicardial surface #######
fem define data;r;Temp			# This is just a temporary data file
fem define xi;r;Surface_Points_Epi
fem define data;c from_xi
fem define data;w;../LVRegular_Epi
fem list data statistics
fem export data;../LVRegular_Epi as LVRegular_Epi

fem reallocate
fem quit

