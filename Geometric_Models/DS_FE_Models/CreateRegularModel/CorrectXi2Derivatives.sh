#! /bin/tcsh

# This file is used to correct the Xi2 derivatives for the apex to remain circular 


foreach NODE (1) 
  awk -f CorrectXi2DerivativesSub.awk -v node1=$NODE rc_arithmetic.ipnode > rc_tmp.ipnode
  echo "Updated node $NODE"
end

foreach NODE (18) 
  awk -f CorrectXi2DerivativesSub.awk -v node1=$NODE rc_tmp.ipnode > rcV2.ipnode
  echo "Updated node $NODE"
end

mv rcV2.ipnode rc_arithmetic.ipnode
rm rc_tmp.ipnode
