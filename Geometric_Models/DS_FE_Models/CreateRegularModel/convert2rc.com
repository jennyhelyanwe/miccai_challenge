fem define para;r;small;
fem define coor;r;prolate
fem define node;r;prolate
fem define base;r;LVBasis
fem define elem;r;prolate
fem export node;prolate as prolate
fem export elem;prolate as prolate

fem change coordinates to 1

fem define nodes;w;rc
fem define elements;w;rc

fem reallocate

# Change basis to arc length based (arithmetic mean)
fem define para;r;small
fem define coor;r;rc
fem define base;r;LVBasis_RC
fem define node;r;rc 
fem define elem;r;rc

fem update nodes derivative 1 versions individual;	# Update the derivative 1 with versions
fem update nodes derivative 2 versions individual;	# Update the derivative 2 with versions
fem update nodes derivative 3 versions individual;	# Update the derivative 3 with versions
fem update scale_factor normalise;			# Update the scale factor

fem define node;w;rc_arithmetic
fem define elem;w;rc_arithmetic

system('perl CorrectXi2Derivatives.sh')                 # Correct for short axis derivatives. 
fem define node;r;rc_arithmetic
fem define elem;r;rc_arithmetic

fem define window;c
fem draw lines
fem export node;rc as rc_mesh
fem export elem;rc as rc_mesh

#fem quit
