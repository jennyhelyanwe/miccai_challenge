package makeMesh;

use Math::Trig;

# Number of elements
my $nElemCirc = 4;
my $nElemTrans = 1;
my $nElemLong = 4;
my $nElements = $nElemCirc*$nElemTrans*$nElemLong;
my $nNodes = $nElemCirc*($nElemTrans+1)*$nElemLong + ($nElemTrans+1);
my $muApex = 0;
my $version=1;
my $iApex=0;

#set from parent function:
#our($focus) = 37.5;
#our($lambdaEndocard) = 0.5;
#our($lambdaEpicard) = 0.77;
#our($lambdaApex) = 0.77;
#our($muBase) = 100;


sub createNodes
{
  # Resolution
  my $nLambdaValues = $nElemTrans+1;
  my @lambdaValues;
  $#lambdaValues = $nLambdaValues-1;
  $lambdaValues[0] = $lambdaEndocard;
  $lambdaValues[$#lambdaValues] = $lambdaEpicard;
  my $dLambda = ($lambdaEpicard-$lambdaEndocard)/$nElemTrans;
  print "lambda endocardial: ", $lambdaValues[0], "\n";
  print "lambda epiocardial: ", $lambdaValues[$#lambdaValues], "\n";
  if($nElemTrans>1)
  {
    for(my $n=1;$n<$#lambdaValues;$n++)
    {
      $lambdaValues[$n] = $lambdaValues[$n-1]+$dLambda;
    }
  }
  my @lambdaApexValues;
  $#lambdaApexValues = $nLambdaValues-1;
  $lambdaApexValues[0] = $lambdaEndocard;
  $lambdaApexValues[$#lambdaApexValues] = $lambdaEpicard;
  #$lambdaApexValues[0] = $lambdaApex-0.1;
  #$lambdaApexValues[$#lambdaApexValues] = $lambdaApex;
  my $dLambda = 0.1/$nElemTrans;
  if($nElemTrans>1)
  {
    for(my $n=1;$n<$#lambdaApexValues;$n++)
    {
      $lambdaApexValues[$n] = $lambdaApexValues[$n-1]+$dLambda;
    }
  }
  print "\n\n";  
  for(my $n=0;$n<=$#lambdaApexValues;$n++)
  {
    print $lambdaApexValues[$n], "\n";
  }
  
  my $nMuValues = $nElemLong+1;
  my @muValues;
  $#muValues = $nMuValues-1;
  $muValues[0] = $muApex;
  $muValues[$#muValues] = $muBase;
  my $dMu = ($muBase-$muApex)/$nElemLong;
  if($nElemLong>1)
  {
    for(my $n=1;$n<$#muValues;$n++)
    {
      $muValues[$n] = $muValues[$n-1]+$dMu;
    }
  }

  my $nThetaValues = $nElemCirc;
  my @thetaValues;
  $#thetaValues = $nThetaValues-1;
  my @versionAngles;
  $#versionAngles = $nThetaValues-1;
  if($nElemCirc>1)
  {
    #$thetaValues[0] = 355;
    #$versionAngles[0] = 355;
    $thetaValues[0] = 0;
    $versionAngles[0] = 0;
    my $dTheta = -360/$nElemCirc;
    for(my $n=1;$n<=$#thetaValues;$n++)
    {
      $thetaValues[$n] = $thetaValues[$n-1]+$dTheta;
      $versionAngles[$n] = $thetaValues[$n];
    }
  } else
  {
    $thetaValues[0] = 0;
  }

  my ($lambda, $mu, $theta);
  my $file = "prolate.ipnode";
  
  CORE::open(MYFID, "> $file") || die "Couldn't open $file file";
  print MYFID " CMISS Version 1.21 ipnode File Version 2\n";
  print MYFID " Heading: Nodes created in Perl\n";
  print MYFID "\n";
  print MYFID " Specify the focus position [1.0]:   $focus\n";
  print MYFID " The number of nodes is [    1]:     $nNodes\n";
  print MYFID " Number of coordinates [3]: 3\n";
  print MYFID " Do you want prompting for different versions of nj=1 [N]? N\n";
  print MYFID " Do you want prompting for different versions of nj=2 [N]? N\n";
  if ($nElemCirc>1)
  {
    print MYFID " Do you want prompting for different versions of nj=3 [N]? Y\n";
  } else
  {
    print MYFID " Do you want prompting for different versions of nj=3 [N]? N\n";
  }
  print MYFID " The number of derivatives for coordinate 1 is [0]: 7\n";
  print MYFID " The number of derivatives for coordinate 2 is [0]: 7\n";
  print MYFID " The number of derivatives for coordinate 3 is [0]: 7\n";
  print MYFID "\n";




  my $node = 1;
  my $dLambdaDxi1 = 0;
  my $dLambdaDxi2 = 0;
  my $dMuDxi2 = $dMu;
  #my $dThetaDxi1 = $dTheta;
  my $dThetaDxi1 = -360/$nElemCirc;
  my ($iLambda,$boolApexSet,$iMu,$iTheta,$nVersions);
  for($iLambda=0;$iLambda<$nLambdaValues;$iLambda++)
  {
    $boolApexSet = 0; # Is Apex node set for this value of Lambda
    for($iMu=0;$iMu<$nMuValues;$iMu++)
    {
      $bContinue=1;
      $iTheta=0;
      #for($iTheta=0;$iTheta<$nElemCirc;$iTheta++)
      while($bContinue)
      {
        # $dLambdaDxi1 = $lambdaValues[$iLambda+1]-$lambdaValues[$iLambda];
        print MYFID sprintf(" Node number [    %d]:     %d\n", $node, $node);
	      if($muValues[$iMu]==0) #is epi apex
	      {
	        writeApexNode($dLambdaDxi2, \@lambdaApexValues); 
		print "Printing dLambdaDxi2\n";
		print $dLambdaDxi2;
	        #readNodeCoords(\@pt26,3,28);         
	        $iApex++;
	      } else
      	{
          print MYFID sprintf(" The Xj(1) coordinate is [ 0.15000E+01]:    %f\n",$lambdaValues[$iLambda]);
	        print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: %f\n",$dLambdaDxi1);
          print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: %f\n",$dLambdaDxi2);
 	        print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: %f\n", dLambdaDxi3($focus, $lambdaValues[$iLambda], $muValues[$iMu], $thetaValues[$iTheta]));
          print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
       	  print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
       	  print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
        }
	
        print MYFID sprintf(" The Xj(2) coordinate is [ 0.10000E+02]:    %f\n",$muValues[$iMu]);
        print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
	      #if($node==1) #is endo apex
	      if($muValues[$iMu]==0) # apex node
	      {
       	  print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: %f\n",0);
       	  #print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: %f\n",$dMuDxi2);
	      } else
      	{
   	      print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: %f\n",$dMuDxi2);
      	}
       	print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
        print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: 0.00000000000000000E+00\n");
        print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
       	print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
       	print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");

        if (($nElemCirc>1) && ($muValues[$iMu]==0))
        {
          print MYFID sprintf(" The number of versions for nj=3 is [1]: %d\n",$nElemCirc);
          for($nVersions=1;$nVersions<=$nElemCirc;$nVersions++)
	        {
            print MYFID sprintf(" For version number%2d:\n",$nVersions);
            print MYFID sprintf(" The Xj(3) coordinate is [ 0.00000E+00]:    %f\n",$versionAngles[$nVersions-1]);
            print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
            print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
     	      print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
            print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: 0.00000000000000000E+00\n");
            print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
            print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
     	      print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          }
        } elsif (($nElemCirc>1) && ($muValues[$iMu] != 0))
        {
          print MYFID sprintf(" The number of versions for nj=3 is [1]: 1\n");
          print MYFID sprintf(" The Xj(3) coordinate is [ 0.00000E+00]:    %f\n",$thetaValues[$iTheta]);
          print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: %f\n",$dThetaDxi1);
          print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
 	        print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
        } else
        {
          print MYFID sprintf(" The Xj(3) coordinate is [ 0.00000E+00]:    %f\n",$thetaValues[$iTheta]);
          print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
          print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
        }
        print MYFID sprintf(" \n");
        $node++;
	      $iTheta++;
        if (($muValues[$iMu]==0) || ($iTheta==$nElemCirc))
        {
          $boolApexSet = 1;
	        $bContinue = 0;
          #iTheta = nElemCirc;
          #last;
        }
      }
    }
  }
  CORE::close(MYFID);
}



sub createElements
{
  my $file = "prolate.ipelem";
  
  CORE::open(MYFID, "> $file") || die "Couldn't open $file file";
  print MYFID " CMISS Version 1.21 ipelem File Version 2\n";
  print MYFID " Heading: Elements created in Perl\n";
  print MYFID " \n"; 
  print MYFID sprintf(" The number of elements is [1]:   %d\n", $nElements);
  print MYFID " \n";

  my $elemCounter = 1;
  my $nNodesPerSurface = $nElemCirc*$nElemLong+1;
  my $dCirc = 1; 
  my $dLong = $nElemCirc; 
  my $dTrans = $nNodesPerSurface;
  
  for($trans=1;$trans<=$nElemTrans;$trans++)
  {
    $boolIsElemAtApex = 1;
    for($long=1;$long<=$nElemLong;$long++)
    {
      for($circ=1;$circ<=$nElemCirc;$circ++)
      {
        if($boolIsElemAtApex)
        {
	        $node1 = $long + ($trans-1)*$nNodesPerSurface;
          $node2 = $node1;
          $node3 = $node1+$circ;
          $node4 = $node3+$dCirc;
          $node5 = $node1+$dTrans;            
          $node6 = $node5;
          $node7 = $node5+$circ;
          $node8 = $node7+$dCirc;
          if($circ==$nElemCirc)
	        {
            $node4 = $node1+$dCirc;
            $node8 = $node5+$dCirc;
          } 
	      } else
	      {
	        $node1 = $circ + ($long-2)*$dLong + 1 + ($trans-1)*$nNodesPerSurface;
          $node2 = $node1+$dCirc;
          $node3 = $node1+$dLong;
          $node4 = $node2+$dLong;
          $node5 = $node1+$dTrans;
          $node6 = $node5+$dCirc;
          $node7 = $node5+$dLong;
          $node8 = $node6+$dLong;
          if($circ==$nElemCirc)
	        {
            $node2 = ($long-2)*$dLong + 2 + ($trans-1)*$nNodesPerSurface;
            $node4 = $node2+$dLong;
            $node6 = $node2+$dTrans;
            $node8 = $node6+$dLong;
          }
        }  
        writeToElementFile($elemCounter, $node1, $node2, $node3, $node4, $node5, $node6, $node7, $node8);
        $elemCounter = $elemCounter+1;
      }
      $boolIsElemAtApex = 0;
    }
  }
  CORE::close(MYFID);
}


sub writeToElementFile
{
  my ($elNum, $node1, $node2, $node3, $node4, $node5, $node6, $node7, $node8) = @_;

  print MYFID sprintf(" Element number [    %d]:     %d\n", $elNum, $elNum);
  print MYFID sprintf(" The number of geometric Xj-coordinates is [3]: 3\n");
  print MYFID sprintf(" The basis function type for geometric variable 1 is [1]: 1\n");
  print MYFID sprintf(" The basis function type for geometric variable 2 is [1]: 1\n");
  print MYFID sprintf(" The basis function type for geometric variable 3 is [1]: 1\n");
  print MYFID sprintf(" Enter the 8 global numbers for basis 1: %d %d %d %d %d %d %d %d\n",$node1,$node2,$node3,$node4,$node5,$node6,$node7,$node8);

  # Versions?
  if (($nElemCirc>1) && ($node1==$node2))
  {
    print MYFID sprintf(" The version number for occurrence  1 of node   %3d, njj=3 is [ 1]:  %d\n",$node1,$version);
    if (($version+1)>$nElemCirc)
    {
      print MYFID sprintf(" The version number for occurrence  2 of node   %3d, njj=3 is [ 1]:  %d\n",$node1,1);
    } else
    {
      print MYFID sprintf(" The version number for occurrence  2 of node   %3d, njj=3 is [ 1]:  %d\n",$node1,$version+1);
    }
  }
  if (($nElemCirc>1) && ($node5==$node6))
  {
    print MYFID sprintf(" The version number for occurrence  1 of node   %3d, njj=3 is [ 1]:  %d\n",$node5,$version);
    if (($version+1)>$nElemCirc)
    {
      print MYFID sprintf(" The version number for occurrence  2 of node   %3d, njj=3 is [ 1]:  %d\n",$node5,1);
      $version = 0;
    } else
    {
      print MYFID sprintf(" The version number for occurrence  2 of node   %3d, njj=3 is [ 1]:  %d\n",$node5,$version+1);
    }
  }
  
  # Due to cavity elements where nodeVec(1)==nodeVec(2), but unless at apex: nodeVec(5)~=nodeVec(6)
  if (($nElemCirc>1) && ($node1==$node2))
  {
    if ($version+1>$nElemCirc)
    {
      $version = 0;
    }
  }
  if (($nElemCirc>1) && ($node1==$node2))
  {
    $version = $version+1;
  }
  #print MYFID sprintf(" Enter the 8 numbers for basis 2 [prev]:\n",$node1,$node2,$node3,$node4,$node5,$node6,$node7,$node8);
  print MYFID sprintf(" Enter the 8 numbers for basis 3 [prev]: %d %d %d %d %d %d %d %d\n",$node1,$node2,$node3,$node4,$node5,$node6,$node7,$node8);

}



sub dLambdaDxi3
{
  my ($f, $lambda, $mu, $theta) = @_;

  my $dx = $f*sinh($lambda)*cos($mu);
  my $dy = $f*cosh($lambda)*sin($mu)*cos($theta);
  my $dz = $f*cosh($lambda)*sin($mu)*sin($theta);
  my $ds = sqrt($dx**2 + $dy**2 + $dz**2);
  my $dLambda = 1/$ds;
  
  return ($dLambda);
}


sub writeApexNode
{
  my ($dLambdaDxi2, $lambdaApexValues) = @_;
  
  print MYFID sprintf(" The Xj(1) coordinate is [ 0.15000E+01]:    %f\n",$lambdaApexValues->[$iApex]);
  print MYFID sprintf(" The derivative wrt direction 1 is [ 0.00000E+00]: %f\n",0);
  print MYFID sprintf(" The derivative wrt direction 2 is [ 0.00000E+00]: %f\n",$dLambdaDxi2);
  print MYFID sprintf(" The derivative wrt directions 1 & 2 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
  print MYFID sprintf(" The derivative wrt direction 3 is [ 0.50000E+00]: %f\n", ($lambdaApexValues->[1]-$lambdaApexValues->[0]));
  print MYFID sprintf(" The derivative wrt directions 1 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
  print MYFID sprintf(" The derivative wrt directions 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
  print MYFID sprintf(" The derivative wrt directions 1, 2 & 3 is [ 0.00000E+00]: 0.00000000000000000E+00\n");
}


1;
 
__END__
