## Read in model and data files
gfx read node rc.exnode
gfx read elem rc.exelem

gfx read data LVRegular_Endo.exdata
gfx change data_offset 10000
gfx read data LVRegular_Epi.exdata

# Create extra transparent muscle material. 
gfx create material muscle_transparent normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 0.64 shininess 0.2;

# Draw
gfx modify g_element "/" general clear;
gfx modify g_element "/" data_points subgroup LVRegular_Endo coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material gold selected_material default_selected;
gfx modify g_element "/" data_points subgroup LVRegular_Epi coordinate coordinates LOCAL glyph sphere general size "1*1*1" centre 0,0,0 font default select_on material green selected_material default_selected;
gfx modify g_element "/" node_points coordinate coordinates LOCAL glyph point general size "1*1*1" centre 0,0,0 font default label cmiss_number select_on material default selected_material default_selected;
gfx modify g_element "/" cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element "/" surfaces coordinate coordinates face xi3_0 tessellation default LOCAL select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element "/" surfaces coordinate coordinates face xi3_1 tessellation default LOCAL select_on material muscle_transparent selected_material default_selected render_shaded;


# Create window and fix view. 
gfx create window 1 double_buffer;
gfx modify window 1 image scene default light_model default;
gfx modify window 1 image add_light default;
gfx modify window 1 layout simple ortho_axes z x eye_spacing 0.25 width 635 height 974;
gfx modify window 1 set current_pane 1;
gfx modify window 1 background colour 1 1 1 texture none;
gfx modify window 1 view parallel eye_point -46.0474 21.7539 133.428 interest_point 11.615 0 0 up_vector -0.91681 0.0169162 -0.398966 view_angle 40 near_clipping_plane 1.46974 far_clipping_plane 525.235 relative_viewport ndc_placement -1 1 2 2 viewport_coordinates 0 0 1 1;
gfx modify window 1 set transform_tool current_pane 1 std_view_angle 40 normal_lines no_antialias depth_of_field 0.0 fast_transparency blend_normal;


