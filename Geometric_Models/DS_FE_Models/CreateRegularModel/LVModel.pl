# This function makes a prolate hostmesh scaled to data points of the apex and base.
# It converts the mesh to RC and rotates and translates the hostmesh over the
# original positions of the data points.

my $cm = "cm";

use makeMesh;
use coordConversion;
use PDL;
use Math::Trig;
use File::Copy "cp";

# Find focus from x-value at apex(epi) position where
# it is assumed lambda is $testLambda, and mu = 0.
# x = f*cosh(lambda)*cos(mu)
#my $testLambda = 0.9;
my $focus=27.3323;#22.6588;
#my $focus=25.0;
print "focus : $focus\n";

$muBase = 120;
print "Mu at base : $muBase\n";

$makeMesh::focus = $focus;
$makeMesh::lambdaEndocard =0.56911077539440;
$makeMesh::lambdaEpicard =0.84405159731812;
$makeMesh::lambdaApex = 1;
$makeMesh::muBase = $muBase;
createFiles();
#die("Test finished\n");

cp("prolate/rcphase48.ipnode", "rcphase48.ipnode");
cp("prolate/rcphase48.ipelem", "rcphase48.ipelem");


if (glob "prolate/tmp.Faa*")
{
  system("rm prolate/tmp.Faa*");
}

#####################################################################

sub createFiles
{

  makeMesh::createNodes();
  makeMesh::createElements();

  #chdir("prolate/");
  print "Starting cm\n";
  system("$cm convert2rc.com");
  print "Finishing cm\n";
  #chdir("../");
}





