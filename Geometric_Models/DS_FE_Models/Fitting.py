def writeFittingComFile(DS_Endo, DS_Epi):
    comFile = 'GeometricFitting.com'

    try:
        file = open(comFile, 'w')
    except IOError:
        print 'ERROR: unable to open ', comFile
        return

    file.write('set echo on;\n')
    file.write('# Define total number of iterations required \n')
    file.write('$tot_itt = 5;\n')
    file.write('fem def para;r;small;\n')
    file.write('fem def coor;r;mapping;\n')
    file.write('fem define node;r;../CreateRegularModel/rc;\n')
    file.write('fem define elem;r;../CreateRegularModel/rc;\n')
    file.write('fem def base;r;LVBasisV2;\n')
    file.write('## Read in the scaling vector from python\n')
    file.write('CORE::open( MYFILE,'../transMatrix_Scaling.TRN' ) or die( "ERROR: Unable to open output file\n" );\n')
    file.write('while (<MYFILE>) {\n')
    file.write(' 	chomp;\n')
    file.write(' 	$transformation= "$_\n";\n')
    file.write(' }\n')
    file.write('close (MYFILE); \n')
    file.write('$scale= substr $transformation, 0,-15;\n')
    file.write('$rotation=substr $transformation, 39;\n')
    file.write('$rotation=-$rotation;\n')
    file.write('print $scale\n')
    file.write('print $rotation\n')
    file.write('fem change node scale by $scale\n')
    file.write('fem update nodes derivative 1 versions individual;\n')
    file.write('fem update nodes derivative 2 versions individual;\n')
    file.write('fem update nodes derivative 3 versions individual;\n')
    file.write('fem define node;w;LVRegularModel_Transformed\n')
    file.write('fem define elem;w;LVRegularModel_Transformed\n')
    file.write('fem exp node;LVRegularModel_Transformed as LVRegularModel_Transformed;\n')
    file.write('fem exp elem;LVRegularModel_Transformed as LVRegularModel_Transformed;\n')
    file.write('\n')
    file.write('read commands;TranslateEndoNode\n')
    file.write('\n')
    file.write('\n')
    file.write('\n')
    file.write('\n')
    file.write('\n')

    file.write('\n')
    file.write('\n')
    file.write('\n')

    file.close()
