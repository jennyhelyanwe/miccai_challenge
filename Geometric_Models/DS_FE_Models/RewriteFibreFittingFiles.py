import dircache
import os,sys
import shutil
from shutil import copy, move

########################################################################################
def Rewrite_Fibre_Fitting_Comfiles(current_study_name):
    # Write custom MainFitting.com file for fitting 
    try:
        file = open('Fibre/MainFitting_Template.com', 'r')
        filew = open('Fibre/MainFitting.com', 'w')
    except IOError:
        print 'ERROR: Unable to open MainFitting_Template.com'
        return
  
    num_line = 1
    data = file.readline()
    while (num_line <= 60):
        if(num_line == 5):
            string = 'system(\'cp '+current_study_name+'/DS_'+current_study_name+ '_FibreVector.txt '+current_study_name+'/DS_'+current_study_name+'_FibreVector.ipdata\');\n'
        elif(num_line == 14)|(num_line == 32):
            string = 'fem define node;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 15)|(num_line == 33):
            string = 'fem define elem;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 18)|(num_line == 41):
            string = '$filename_vector="'+current_study_name+'/DS_'+current_study_name+'_FibreVector";\n'
        elif(num_line == 22):
            string = 'fem define xi;w;DS_'+current_study_name+'_FibreVector;\n'
        elif(num_line == 44):
            string = 'fem define xi;r;DS_'+current_study_name+'_FibreVector;\n'
        else:
            string = data

        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()
    
    # Write custom MainFitting.com file for fitting 
    try:
        file = open('Fibre/MainFitting_InModel_No_Apex_Template.com', 'r')
        filew = open('Fibre/MainFitting_InModel_No_Apex.com', 'w')
    except IOError:
        print 'ERROR: Unable to open MainFitting_Template.com'
        return
  
    num_line = 1
    data = file.readline()
    while (num_line <= 58):
        if(num_line == 5):
            string = 'system(\'cp '+current_study_name+'/DS_'+current_study_name+ '_FibreVector.txt '+current_study_name+'/DS_'+current_study_name+'_FibreVector.ipdata\');\n'
        elif(num_line == 14)|(num_line == 33):
            string = 'fem define node;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 15)|(num_line == 34):
            string = 'fem define elem;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 18):
            string = '$filename_vector="'+current_study_name+'/DS_'+current_study_name+'_FibreVector";\n'
        elif(num_line == 22):
            string = 'fem define xi;w;DS_'+current_study_name+'_FibreVector_InModel;\n'
        elif(num_line == 42):
            string = 'fem define xi;r;DS_'+current_study_name+'_FibreVector_InModel_No_Apex;\n'
        else:
            string = data

        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()


    # Write custom FibreFitting.com file for fitting
    try:
        file = open('Fibre/FibreFitting_Template.com', 'r')
        filew = open('Fibre/FibreFitting.com','w')
    except IOError:
        print 'ERROR: Unable to open FibreFitting_Template.com'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 51):
        if(num_line == 5):
            string = 'fem define node;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 6):
            string = 'fem define elem;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 16):
            string = 'fem define data;r;'+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor fibre;\n'
        elif(num_line == 24):
            string = 'fem define data;w;'+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor_CorXi1 as DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor_CorXi1 fibre radians\n'
        elif(num_line == 27):
            string = 'fem define xi;r;DS_'+current_study_name+'_EliminatedZeroVector\n'
        elif(num_line == 42):
            string = 'fem list data;'+current_study_name+'/FibreFittingError error full;\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()

    # Write custom FibreFitting.com file for fitting
    try:
        file = open('Fibre/FibreFitting_InModel_No_Apex_Template.com', 'r')
        filew = open('Fibre/FibreFitting_InModel_No_Apex.com','w')
    except IOError:
        print 'ERROR: Unable to open FibreFitting_Template.com'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 51):
        if(num_line == 5):
            string = 'fem define node;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 6):
            string = 'fem define elem;r;../DS_FE_Models/'+current_study_name+'/DS_fitted\n'
        elif(num_line == 16):
            string = 'fem define data;r;'+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor fibre;\n'
        elif(num_line == 24):
            string = 'fem define data;w;'+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor_CorXi1 as DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor_CorXi1 fibre radians\n'
        elif(num_line == 27):
            string = 'fem define xi;r;DS_'+current_study_name+'_FibreVector_InModel_No_Apex\n'
        elif(num_line == 42):
            string = 'fem list data;'+current_study_name+'/FibreFittingError error full;\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()
    # Write custom matlab files for fitting. 
    try:
        file = open('Fibre/UpdateXi_Template.com', 'r')
        filew = open('Fibre/UpdateXi.com','w')
    except IOError:
        print 'ERROR: Unable to open KeepFibreInModel_Template.m'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 22):
        if(num_line == 5):
            string = 'fem define node;r;../DS_FE_Models/'+current_study_name+'/DS_fitted;\n'
        elif(num_line == 7):
            string = 'fem define elem;r;../DS_FE_Models/'+current_study_name+'/DS_fitted;\n'
        elif(num_line == 17):
            string = 'fem define data;r;'+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor fibre;\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()

    # Write custom matlab files for fitting. 
    try:
        file = open('Fibre/ExtractFibreAngleFromOPFIEL_Template.m', 'r')
        filew = open('Fibre/ExtractFibreAngleFromOPFIEL.m','w')
    except IOError:
        print 'ERROR: Unable to open ExtractFibreAngleFromOPFIEL_Template.m'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 225):
        if(num_line == 67):
            string = 'filenamer=(\''+current_study_name+'/DS_'+current_study_name+'_FibreVector_InModel_No_Apex.ipdata\');\n'
        elif(num_line == 93):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj.ipdata\');\n'
        elif(num_line == 108):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj.exdata\');\n'
        elif(num_line == 134):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreImbrication_Proj.ipdata\');\n'
        elif(num_line == 147):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreImbrication_Proj.exdata\');\n'
        elif(num_line == 173):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreVectors_Proj.ipdata\');\n'
        elif(num_line == 188):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_Fibre_WallVector.ipdata\');\n'
        elif(num_line == 222):
            string = 'save '+current_study_name+'/Xi_InModel.mat Xi_InModel;\n'
        elif(num_line == 223):
            string = 'save '+current_study_name+'/FibreLocations.mat FibreLocations;\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()


    # Write custom matlab files for fitting. 
    try:
        file = open('Fibre/CorrectRawFibreData_Template.m', 'r')
        filew = open('Fibre/CorrectRawFibreData.m','w')
    except IOError:
        print 'ERROR: Unable to open CorrectRawFibreData_Template.m'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 86):
        if(num_line == 8):
            string = 'filenamer1=(\''+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj.ipdata\');\n'
        elif(num_line == 26):
            string = 'load '+current_study_name+'/Xi_InModel.mat;\n'
        elif(num_line == 58):
            string = 'filenamew=(\''+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor.ipdata\');\n'
        elif(num_line == 67):
            string = 'filenamew2=(\''+current_study_name+'/DS_'+current_study_name+'_FibreAngles_Proj_ModelBasedCor.exdata\');\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close()    

    # Write custom matlab files for fitting. 
    try:
        file = open('Fibre/ExtractFibreFittingError_Template.m', 'r')
        filew = open('Fibre/ExtractFibreFittingError.m','w')
    except IOError:
        print 'ERROR: Unable to open CorrectRawFibreData_Template.m'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 84):
        if(num_line == 8):
            string = 'filename = (\''+current_study_name+'/FibreFittingError.opdata\');\n'
        elif(num_line == 32):
            string = 'load '+current_study_name+'/FibreLocations;\n'
        elif(num_line == 33):
            string = 'filenamew = \''+current_study_name+'/FibreFittingError.exdata\';\n'
        elif(num_line == 62):
            string = 'fid_ipxi = fopen(\'DS_'+current_study_name+'_FibreVector_InModel_No_Apex.ipxi\', \'r\');\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close() 
    # Write custom matlab files for fitting. 
    try:
        file = open('Fibre/UpdateData_Xi_InModel_No_Apex_Template.m', 'r')
        filew = open('Fibre/UpdateData_Xi_InModel_No_Apex.m','w')
    except IOError:
        print 'ERROR: Unable to open CorrectRawFibreData_Template.m'
        return 

    num_line = 1
    data = file.readline()
    while (num_line <= 54):
        if(num_line ==10):
            string = 'fidr = fopen(\'DS_'+current_study_name+'_FibreVector_InModel.ipxi\',\'r\');\n'
        elif(num_line == 11):
            string = 'fidw = fopen(\'DS_'+current_study_name+'_FibreVector_InModel_No_Apex.ipxi\',\'w\');\n'
        elif(num_line == 31):
            string = 'fidr = fopen(\''+current_study_name+'/DS_'+current_study_name+'_FibreVector.ipdata\', \'r\');\n'
        elif(num_line == 32):
            string = 'fidw = fopen(\''+current_study_name+'/DS_'+current_study_name+'_FibreVector_InModel_No_Apex.ipdata\', \'w\');\n'
        else: 
            string = data
        filew.write(str(string))
        data = file.readline()
        num_line = num_line + 1

    file.close()
    filew.close() 

