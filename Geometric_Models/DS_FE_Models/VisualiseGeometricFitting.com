gfx create material muscle_trans normal_mode ambient 0.4 0.14 0.11 diffuse 0.5 0.12 0.1 emission 0 0 0 specular 0.3 0.5 0.5 alpha 0.65 shininess 0.2;

set dir /hpc/zwan145/MICCAI_Challenge/Geometric_Models/DS_FE_Models
gfx read node CreateRegularModel/rc region Regular
gfx read elem CreateRegularModel/rc region Regular

# Read translated regular model
gfx read node Geometric_Fitting/LVCanineModel_Translated region Translated
gfx read elem Geometric_Fitting/LVCanineModel_Translated region Translated

# Read scaled regular model
gfx read node Geometric_Fitting/LVCanineModel_Scaled region Scaled
gfx read elem Geometric_Fitting/LVCanineModel_Scaled region Scaled


# Read registered regular model.
gfx read node 0912/DS_fitted region Registered
gfx read elem 0912/DS_fitted region Registered

# Read registered challenge data
gfx read data ../Surface_Data/0912/DS_Endo 
gfx change data_offset 10000
gfx read data ../Surface_Data/0912/DS_Epi 
gfx change data_offset 10000

gfx cre window 1
gfx modify window 1 background colour 1 1 1 texture none
gfx modify g_element "/" general clear;
gfx modify g_element "/" data_points subgroup DS_Endo coordinate coordinates LOCAL glyph sphere_hires general size "1*1*1" centre 0,0,0 font default select_on material green selected_material default_selected;
gfx modify g_element "/" point LOCAL glyph axes_solid_xyz general size "40*40*40" centre 0,0,0 font default select_on material default selected_material default_selected;
gfx modify g_element "/" data_points subgroup DS_Epi coordinate coordinates LOCAL glyph sphere_hires general size "1*1*1" centre 0,0,0 font default select_on material green selected_material default_selected;
gfx modify g_element /Regular/ general clear;
gfx modify g_element /Regular/ lines coordinate coordinates tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element /Regular/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material tissue selected_material default_selected render_shaded;
gfx modify g_element /Scaled/ general clear;
gfx modify g_element /Scaled/ lines coordinate coordinates tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element /Scaled/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material green selected_material default_selected render_shaded;
gfx modify g_element /Translated/ general clear;
gfx modify g_element /Translated/ lines coordinate coordinates tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element /Translated/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material gold selected_material default_selected render_shaded;
gfx modify g_element /Registered/ general clear;
gfx modify g_element /Registered/ lines coordinate coordinates tessellation default LOCAL select_on material default selected_material default_selected;
gfx modify g_element /Registered/ cylinders coordinate coordinates tessellation default LOCAL circle_discretization 6 constant_radius 0.2 select_on material muscle selected_material default_selected render_shaded;
gfx modify g_element /Registered/ surfaces coordinate coordinates exterior face xi3_0 tessellation default LOCAL select_on material muscle_trans selected_material default_selected render_shaded;
gfx modify g_element /Registered/ surfaces coordinate coordinates exterior face xi3_1 tessellation default LOCAL select_on material muscle_trans selected_material default_selected render_shaded;



