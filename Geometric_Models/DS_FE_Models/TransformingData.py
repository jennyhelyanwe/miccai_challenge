"""
This function is written to read in the UPF and canine surface data, perform transformation
to align the UPF data with the model
Author: Vicky Wang
"""
		
import os
import scipy
import scipy.constants
import numpy
import math
import fitting_SurfaceData
import re
import string

from scipy import array,concatenate
from scipy import spatial
from scipy import delete
from fitting_SurfaceData import readSurfaceData, readIpdata, constructTransformationMatrix, inverseTransformRigid3D, transformScale3D
from fitting_SurfaceData import transformRigid3DFinal, fitDataRigidScaleNoCorr, fitDataRigidScaleNoCorr_TwoSurfaces, fitDataRigidAnisotropicScaleNoCorr
from fitting_SurfaceData import fitDataRigidScaleNoCorr_ModelTree, fitDataRigidAnisotropicScaleNoCorr_ModelTree, fitDataRigidAnisotropicScaleNoCorr_ModelTree_TwoSurfaces
from fitting_SurfaceData import fitDataAnisotropicScaleRigidNoCorr_ModelTree_TwoSurfaces
from fitting_SurfaceData import writeIpdata, writeTransformation, writeTransformation_Scaling, writeTransformation_Rotation
from transform3D import transformRigid3D

#===========================================================================================#
def TransformingData (current_study_name,current_file):
	print ''
	print '==================================================='
	print '== Step 1 : Align Surface Data with the LV Model =='
	print '==================================================='
	print ''

	#####################################################################################
	##############============ Step 1.1(a): Read in the UPF data ==============############
	## Select files associated with DS
	for j in current_file:
			if re.search('_epi_DS',str(j)):
				filename_tmp=j
	
	filename='../Data/'+ filename_tmp
	UPFEpi=readSurfaceData(filename)
	noData_UPFEpi=len(UPFEpi)
	## Select files associated with DS
	for j in current_file:
			if re.search('_endo_DS',str(j)):
				filename_tmp=j

	filename='../Data/'+ filename_tmp
	UPFEndo=readSurfaceData(filename)
	noData_UPFEndo=len(UPFEndo)
	UPFTotal = concatenate((UPFEndo,UPFEpi),0)
	noData=len(UPFTotal)
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ds_lvepi.ipdata'
	groupname=current_study_name.replace("-","")
	writeIpdata(UPFEpi, filename, header=groupname+'_ds_lvepi')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ds_lvendo.ipdata'
	writeIpdata(UPFEndo, filename, header=groupname+'_ds_lvendo')
	print '== Step 1.1 : Read in UPF Data  =='
	print 'The total number data in UPF is ', noData
	print ' '

	## Calculate the max x
	x_coor=UPFEpi[:,0]
	y_coor=UPFEpi[:,1]
	z_coor=UPFEpi[:,2]
	x_coor_mean=scipy.mean(x_coor)
	y_coor_mean=scipy.mean(y_coor)
	z_coor_mean=scipy.mean(z_coor)
	#####################################################################################

	##############======== Step 1.1(b): Remove Extra RV Endo points =========############
	print '== Step 1.1(b) : Remove extra RV Endo points at DS =='	
	index=[38,39,40,42,43,44,48,49,50,77,78,98,99,100,101,133,135,138,151,153,154,155,158,167,168,174,180,191,204,205,215,220,221,224,225,238,240,245,248,252,255,259,270,275,285,290,293,298,302,312,319,327,330,331,334,337,353,354,358,363,365,373,379,380,382,385,387,388,390,400,401,406,415,416,418,423,428,442,452,457,462,472,484,486,513,526,527,540,543,544,559,566,574,575,578,583,593,597,610,612,613,644,645,654,664,666,682,683,695,696,698,702,707,709,712,735,742,748,750,751,766,770,771,787,790,807,808,810,818,819,828,846,857,861,864,866,867,871,875,883,885,887,893,894,896,906,918,919,920,923,932,937,941,942,944,949,960,961,964,973,977,983,988,989,994,999,1002,1014,1018,1020,1024,1025,1027,1030,1039,1041,1048,1052,1054,1063,1067,1070,1079,1080,1087,1093,1101,1110,1116,1118]
	no_data_delete=len(index)
	print 'The total number of Endo points to be deleted is '
	print no_data_delete

	UPFEndo_trunc=[]
	j=0
	for i in range(0,noData_UPFEndo):
			if j<no_data_delete :
				if i!=(index[j]-1):
					UPFEndo_trunc.append(UPFEndo[i,:])
				else :
					j=j+1
			else :
				UPFEndo_trunc.append(UPFEndo[i,:])
							
	UPFEndo=array(UPFEndo_trunc)
	noData_UPFEndo_trunc=len(UPFEndo)
	print 'After deleting the extra RV points, the total number of Endo points is '
	print noData_UPFEndo_trunc
	
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ds_lvendo_trunc.ipdata'
	writeIpdata(UPFEndo, filename, header=groupname+'_ds_lvendo_trunc')
	
	#####################################################################################
	##############======== Step 1.2: Read in the Canine Surface data ==========############
	CanineEpi  = readIpdata('../../../LVModel/VickyCanineModel/LVModelEpi.ipdata')
	CanineEndo = readIpdata('../../../LVModel/VickyCanineModel/LVModelEndo.ipdata')
	CanineTotal = concatenate((CanineEndo,CanineEpi),0)
	noData=len(CanineTotal)
	print '== Step 1.2 : Read in Canine Data =='
	print 'The total number data in Canine is ', noData
	print ' '
	
	## Firstly translate the data
	UPFEpi_Trans=transformRigid3D( UPFEpi, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	UPFEndo_Trans=transformRigid3D(UPFEndo, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ds_lvepi_Trans.ipdata'
	writeIpdata(UPFEpi_Trans, filename, header=groupname+'_ds_lvepi_Trans')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ds_lvendo_Trans.ipdata'
	writeIpdata(UPFEndo_Trans, filename, header=groupname+'_ds_lvendo_Trans')
	
	UPFEpi=UPFEpi_Trans
	UPFEndo=UPFEndo_Trans
	
	
	#####################################################################################
	##########============= Step 1.3: Perform initial transformation ==============########
	##########========== to match canine surface data with UPF surface data =====########
	print '== Step 1.3 : Translate,Rotate and Scale the canine data to match UPF data =='
	## Write out the final scaling vector
	
	filename='../'+ current_study_name + '/GeometricModel_DS_' + current_study_name +'/TransformationMatrix_Scaling.TRN'
	
	[TransformationVector,CanineEpi_Transformed,CanineEndo_Transformed]= fitDataAnisotropicScaleRigidNoCorr_ModelTree_TwoSurfaces(CanineEpi,CanineEndo,UPFEpi,UPFEndo, filename,xtol=1e-5, maxfev=0, t0=None)
	print 'Transformation Vector from Canine to UPF with Anisotropic Scaling is '
	print TransformationVector
	print ' '
		
	## Import the transformation vector and compute the full transformation matrix
	TransformationMatrix_Full=constructTransformationMatrix(TransformationVector)
	
	#####################################################################################
	########========= Step 1.4: Inverse rigid transform the UPF data ==============########
	########======== Back transform UPF data to cardiac cooridinate system ======########
	print '== Step 1.4 : Back transform UPF to align with cardiac coordinate system =='
	UPFEpi_BackTransformed = inverseTransformRigid3D( UPFEpi, scipy.hstack(TransformationVector))
	UPFEndo_BackTransformed = inverseTransformRigid3D( UPFEndo, scipy.hstack(TransformationVector))
	CanineEpi_BackTransformed = inverseTransformRigid3D( CanineEpi_Transformed, scipy.hstack(TransformationVector))
	CanineEndo_BackTransformed = inverseTransformRigid3D( CanineEndo_Transformed, scipy.hstack(TransformationVector))

	#####################################################################################
	#########========= Step 1.5(a): Rotate the UPF data along the y and z axes ==========########
	print '== Step 1.5(a) : Rotate UPF data  =='
	x_apex = UPFEpi_BackTransformed[6,0]
	y_apex = UPFEpi_BackTransformed[6,1]	## Most Apical Epi data point
	z_apex = UPFEpi_BackTransformed[6,2]	## Most Apical Epi data point
	print 'The Y and Z coordinates of the most apical data points are ',y_apex,z_apex
	print ' '
	## Further rotate the UPF data
	apex_vector_length=scipy.sqrt(x_apex*x_apex+y_apex*y_apex+z_apex*z_apex)
	x_apex=x_apex/apex_vector_length
	y_apex=y_apex/apex_vector_length
	z_apex=z_apex/apex_vector_length
	theta_z=-scipy.arctan2(y_apex,x_apex)
	theta_y=scipy.arctan2(z_apex,x_apex)
	print theta_z
	print theta_y
	UPFEpi_BackTransformed_Rotated = transformRigid3D( UPFEpi_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )
	UPFEndo_BackTransformed_Rotated = transformRigid3D( UPFEndo_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )

	#####################################################################################
	#########========= Step 1.5(b): Rotate the UPF data along the long-axis ==========########
	####### to ensure y-axis goes through data point 1199 (center points in RV) #########

	## Print closest RV point
	print '== Step 1.5(b) : Rotate UPF data to align y-axis with RV =='
	print 'Cloest RV points'
	print UPFEpi_BackTransformed_Rotated[1270]
	print ' '
	## Define the theta angle for rigid rotation about long-axis to align y with RV
	theta=scipy.arctan2(UPFEpi_BackTransformed_Rotated[1270,2],UPFEpi_BackTransformed_Rotated[1270,1])
	print 'UPF data should be rotated along the x-axis by ', theta
	print ' '

	## Write the rotational angle to a file
	filename='../'+ current_study_name + '/GeometricModel_DS_' + current_study_name + '/TransformationMatrix_Scaling.TRN'
	writeTransformation_Rotation(theta/scipy.constants.pi*180,filename)
	## Perform rigid rotation about long-axis to align y-axis with RV for the UPF data
	UPFEpi_BackTransformed_FinalRotated = transformRigid3D( UPFEpi_BackTransformed_Rotated,scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )
	UPFEndo_BackTransformed_FinalRotated = transformRigid3D( UPFEndo_BackTransformed_Rotated, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )

	## Perform rigid rotation about long-axis to align y-axis with RV for the canine data
	CanineEpi_BackTransformed_Rotated = transformRigid3D( CanineEpi_BackTransformed, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )
	CanineEndo_BackTransformed_Rotated = transformRigid3D( CanineEndo_BackTransformed, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )
	#####################################################################################

	#####################################################################################
	#############============= Step 1.6: WriteOut Transformed Data ===================#####
	## Create a folder to store output data
	print '== Step 1.6(a) : Write out transformed canine model data  =='
	filename='../'+current_study_name +'/TransformedData/TransformedCanine_Epi.ipdata'
	writeIpdata(CanineEpi_Transformed, filename, header='TransformedCanine_Epi')
	filename='../'+current_study_name +'/TransformedData/TransformedCanine_Endo.ipdata'
	writeIpdata(CanineEndo_Transformed, filename, header='TransformedCanine_Endo')
	
	print '== Step 1.6(b) : Write out backtransformed UPF data  =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPF_Epi.ipdata'
	writeIpdata(UPFEpi_BackTransformed, filename, header='BackTransformedUPF_Epi')
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPF_Endo.ipdata'
	writeIpdata(UPFEndo_BackTransformed, filename, header='BackTransformedUPF_Endo')
	
	print '== Step 1.6(c) : Write out backtransformed canine model  data  =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedCanine_Epi.ipdata'
	writeIpdata(CanineEpi_BackTransformed, filename, header='BackTransformedCanine_Epi')
	filename='../'+current_study_name +'/TransformedData/BackTransformedCanine_Endo.ipdata'
	writeIpdata(CanineEndo_BackTransformed, filename, header='BackTransformedCanine_Endo')
	
	print '== Step 1.6(d) : Write out backtransformed canine model data after rotating along long-axis =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedCanineRotated_Epi.ipdata'
	writeIpdata(CanineEpi_BackTransformed_Rotated, filename, header='BackTransformedCanineRotated_Epi')
	filename='../'+current_study_name +'/TransformedData/BackTransformedCanineRotated_Endo.ipdata'
	writeIpdata(CanineEndo_BackTransformed_Rotated, filename, header='BackTransformedCanineRotated_Endo')

	print '== Step 1.6(e) : Write out backtransformed UPF data after rotating along y and z axes =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFRotated_Epi.ipdata'
	writeIpdata(UPFEpi_BackTransformed_Rotated, filename, header='BackTransformedUPFRotated_Epi')
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFRotated_Endo.ipdata'
	writeIpdata(UPFEndo_BackTransformed_Rotated, filename, header='BackTransformedUPFRotated_Endo')

	print '== Step 1.6(f) : Write out backtransformed UPF data after rotating along long-axis =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Epi.ipdata'
	writeIpdata(UPFEpi_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Epi')
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Endo.ipdata'
	writeIpdata(UPFEndo_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Endo')
	
	#####################################################################################
	########### Step 1.7: Apply transformation to the ED surface data #####################

	## Select files associated with ED
	for j in current_file:
			if re.search('_epi_ED',str(j)):
				filename_tmp=j
	
	filename='../Data/'+ filename_tmp
	UPFEpi_ED=readSurfaceData(filename)
	noData_UPFEpi_ED=len(UPFEpi_ED)

	for j in current_file:
			if re.search('_endo_ED',str(j)):
				filename_tmp=j
	
	filename='../Data/'+ filename_tmp
	UPFEndo_ED=readSurfaceData(filename)
	noData_UPFEndo_ED=len(UPFEndo_ED)
	UPFTotal_ED = concatenate((UPFEndo_ED,UPFEpi_ED),0)
	noData=len(UPFTotal_ED)
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ed_lvepi.ipdata'
	writeIpdata(UPFEpi_ED, filename, header=groupname+'_ed_lvepi')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ed_lvendo.ipdata'
	writeIpdata(UPFEndo_ED, filename, header=groupname+'_ed_lvendo')
	print '================ Step 1.7(a) : Read in UPF Data  at ED ===================='
	print 'The total number data in UPF is ', noData
	print ' '
	
	##############======== Step 1.7(b): Remove Extra RV Endo points =========############
	print '== Step 1.7(b) : Remove extra RV Endo points at ED =='
	index=[38,39,40,42,43,44,48,49,50,77,78,98,99,100,101,133,135,138,151,153,154,155,158,167,168,174,180,191,204,205,215,220,221,224,225,238,240,245,248,252,255,259,270,275,285,290,293,298,302,312,319,327,330,331,334,337,353,354,358,363,365,373,379,380,382,385,387,388,390,400,401,406,415,416,418,423,428,442,452,457,462,472,484,486,513,526,527,540,543,544,559,566,574,575,578,583,593,597,610,612,613,644,645,654,664,666,682,683,695,696,698,702,707,709,712,735,742,748,750,751,766,770,771,787,790,807,808,810,818,819,828,846,857,861,864,866,867,871,875,883,885,887,893,894,896,906,918,919,920,923,932,937,941,942,944,949,960,961,964,973,977,983,988,989,994,999,1002,1014,1018,1020,1024,1025,1027,1030,1039,1041,1048,1052,1054,1063,1067,1070,1079,1080,1087,1093,1101,1110,1116,1118]
	no_data_delete=len(index)
	print 'The total number of Endo points to be deleted is '
	print no_data_delete

	UPFEndo_ED_trunc=[]
	j=0
	for i in range(0,noData_UPFEndo_ED):
			if j<no_data_delete :
				if i!=(index[j]-1):
					UPFEndo_ED_trunc.append(UPFEndo_ED[i,:])
				else :
					j=j+1
			else :
				UPFEndo_ED_trunc.append(UPFEndo_ED[i,:])
			
			
	
	UPFEndo_ED=array(UPFEndo_ED_trunc)
	noData_UPFEndo_trunc=len(UPFEndo_ED)
	print 'After deleting the extra RV points, the total number of Endo points is '
	print noData_UPFEndo_trunc

	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ed_lvendo_trunc.ipdata'
	writeIpdata(UPFEndo_ED, filename, header=groupname+'_ed_lvendo_trunc')


	## Firstly translate the data
	UPFEpi_ED_Trans=transformRigid3D( UPFEpi_ED, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	UPFEndo_ED_Trans=transformRigid3D(UPFEndo_ED, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ed_lvepi_Trans.ipdata'
	writeIpdata(UPFEpi_ED_Trans, filename, header=groupname+'_ed_lvepi_Trans')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_ed_lvendo_Trans.ipdata'
	writeIpdata(UPFEndo_ED_Trans, filename, header=groupname+'_ed_lvendo_Trans')
	
	UPFEpi_ED=UPFEpi_ED_Trans
	UPFEndo_ED=UPFEndo_ED_Trans
	
	print '== Step 1.7(c) : Back-transform the UPF data at ED =='
	UPFEpi_ED_BackTransformed = inverseTransformRigid3D( UPFEpi_ED, scipy.hstack(TransformationVector))
	UPFEndo_ED_BackTransformed = inverseTransformRigid3D( UPFEndo_ED, scipy.hstack(TransformationVector))

	print '== Step 1.7(d) : Rotate UPF data at ED along y and z axes =='
	UPFEpi_ED_BackTransformed_Rotated = transformRigid3D( UPFEpi_ED_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )
	UPFEndo_ED_BackTransformed_Rotated = transformRigid3D( UPFEndo_ED_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )

	print '== Step 1.7(e) : Rotate UPF data at ED to align y-axis with RV =='
	UPFEpi_ED_BackTransformed_FinalRotated = transformRigid3D( UPFEpi_ED_BackTransformed_Rotated, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )
	UPFEndo_ED_BackTransformed_FinalRotated = transformRigid3D( UPFEndo_ED_BackTransformed_Rotated, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )

	print '== Step 1.7(f) : Write out the transformed data at ED =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Epi_ED.ipdata'
	writeIpdata(UPFEpi_ED_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Epi_ED')
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Endo_ED.ipdata'
	writeIpdata(UPFEndo_ED_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Endo_ED')
	
	#####################################################################################
	########### Step 1.8: Apply transformation to the ES surface data #####################
	## Select files associated with ED
	for j in current_file:
			if re.search('_epi_ES',str(j)):
				filename_tmp=j
	
	filename='../Data/'+ filename_tmp	
	UPFEpi_ES=readSurfaceData(filename)
	noData_UPFEpi_ES=len(UPFEpi_ES)

	for j in current_file:
			if re.search('_endo_ES',str(j)):
				filename_tmp=j
	
	filename='../Data/'+ filename_tmp
	UPFEndo_ES=readSurfaceData(filename)
	noData_UPFEndo_ES=len(UPFEndo_ES)
	UPFTotal_ES = concatenate((UPFEndo_ES,UPFEpi_ES),0)
	noData=len(UPFTotal_ES)
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_es_lvepi.ipdata'
	writeIpdata(UPFEpi_ES, filename, header=groupname+'_es_lvepi')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_es_lvendo.ipdata'
	writeIpdata(UPFEndo_ES, filename, header=groupname+'_es_lvendo')
	print '== Step 1.8(a) : Read in UPF Data  at ES =='
	print 'The total number data in UPF is ', noData
	print ' '
	
	##############======== Step 1.8(b): Remove Extra RV Endo points =========############
	print '== Step 1.8(b) : Remove extra RV Endo points at ES =='
	index=[38,39,40,42,43,44,48,49,50,77,78,98,99,100,101,133,135,138,151,153,154,155,158,167,168,174,180,191,204,205,215,220,221,224,225,238,240,245,248,252,255,259,270,275,285,290,293,298,302,312,319,327,330,331,334,337,353,354,358,363,365,373,379,380,382,385,387,388,390,400,401,406,415,416,418,423,428,442,452,457,462,472,484,486,513,526,527,540,543,544,559,566,574,575,578,583,593,597,610,612,613,644,645,654,664,666,682,683,695,696,698,702,707,709,712,735,742,748,750,751,766,770,771,787,790,807,808,810,818,819,828,846,857,861,864,866,867,871,875,883,885,887,893,894,896,906,918,919,920,923,932,937,941,942,944,949,960,961,964,973,977,983,988,989,994,999,1002,1014,1018,1020,1024,1025,1027,1030,1039,1041,1048,1052,1054,1063,1067,1070,1079,1080,1087,1093,1101,1110,1116,1118]
	no_data_delete=len(index)
	print 'The total number of Endo points to be deleted is '
	print no_data_delete

	UPFEndo_ES_trunc=[]
	j=0
	for i in range(0,noData_UPFEndo_ES):
			if j<no_data_delete :
				if i!=(index[j]-1):
					UPFEndo_ES_trunc.append(UPFEndo_ES[i,:])
				else :
					j=j+1
			else :
				UPFEndo_ES_trunc.append(UPFEndo_ES[i,:])
			
			
	
	UPFEndo_ES=array(UPFEndo_ES_trunc)
	noData_UPFEndo_trunc=len(UPFEndo_ES)
	print 'After deleting the extra RV points, the total number of Endo points is '
	print noData_UPFEndo_trunc

	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_es_lvendo_trunc.ipdata'
	writeIpdata(UPFEndo_ES, filename, header=groupname+'_ES_lvendo_trunc')
	
	## Firstly translate the data
	UPFEpi_ES_Trans=transformRigid3D( UPFEpi_ES, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	UPFEndo_ES_Trans=transformRigid3D(UPFEndo_ES, scipy.hstack(([-x_coor_mean,-y_coor_mean,-z_coor_mean,0.0,0,0,0.0])) )
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_es_lvepi_Trans.ipdata'
	writeIpdata(UPFEpi_ES_Trans, filename, header=groupname+'_es_lvepi_Trans')
	filename='../'+current_study_name +'/TransformedData/' + current_study_name + '_es_lvendo_Trans.ipdata'
	writeIpdata(UPFEndo_ES_Trans, filename, header=groupname+'_es_lvendo_Trans')
	
	UPFEpi_ES=UPFEpi_ES_Trans
	UPFEndo_ES=UPFEndo_ES_Trans

	print '== Step 1.8(c) : Back-transform the UPF data at ES =='
	UPFEpi_ES_BackTransformed = inverseTransformRigid3D( UPFEpi_ES, scipy.hstack(TransformationVector))
	UPFEndo_ES_BackTransformed = inverseTransformRigid3D( UPFEndo_ES, scipy.hstack(TransformationVector))

	print '== Step 1.8(d) : Rotate UPF data at ES along y and z axes =='
	UPFEpi_ES_BackTransformed_Rotated = transformRigid3D( UPFEpi_ES_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )
	UPFEndo_ES_BackTransformed_Rotated = transformRigid3D( UPFEndo_ES_BackTransformed, scipy.hstack(([0.0,0.0,0.0,0.0,theta_y,theta_z])) )

	print '== Step 1.8(e) : Rotate UPF data at ES to align y-axis with RV =='
	UPFEpi_ES_BackTransformed_FinalRotated = transformRigid3D( UPFEpi_ES_BackTransformed_Rotated, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )
	UPFEndo_ES_BackTransformed_FinalRotated = transformRigid3D( UPFEndo_ES_BackTransformed_Rotated, scipy.hstack(([0.0,0.0,0.0,-theta,0.0,0.0])) )

	print '== Step 1.8(f) : Write out the transformed data at ES =='
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Epi_ES.ipdata'
	writeIpdata(UPFEpi_ES_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Epi_ES')
	filename='../'+current_study_name +'/TransformedData/BackTransformedUPFFinalRotated_Endo_ES.ipdata'
	writeIpdata(UPFEndo_ES_BackTransformed_FinalRotated, filename, header='BackTransformedUPFFinalRotated_Endo_ES')
	
	return
	
#===========================================================================================#
