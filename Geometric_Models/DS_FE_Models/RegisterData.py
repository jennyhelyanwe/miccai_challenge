"""
This function is used to perform transformation to register target data with a regular model. The output is a rigidly rotated target model which matches the coordinate system orientation of the regular model. The function takes in the path pointers of the registered data and the target data and transforms DS, ED and ES target data. 

Modified from Vicky Wang's code
24th April 2014
"""

import os
import scipy
import scipy.constants
import numpy
import math
import fitting_SurfaceData
import re
import string

from scipy import array,concatenate
from scipy import spatial
from scipy import delete
from fitting_SurfaceData import readSurfaceData, readIpdata, constructTransformationMatrix, inverseTransformRigid3D, transformScale3D
from fitting_SurfaceData import transformRigid3DFinal, fitDataRigidScaleNoCorr, fitDataRigidScaleNoCorr_TwoSurfaces, fitDataRigidAnisotropicScaleNoCorr
from fitting_SurfaceData import fitDataRigidScaleNoCorr_ModelTree, fitDataRigidAnisotropicScaleNoCorr_ModelTree, fitDataRigidAnisotropicScaleNoCorr_ModelTree_TwoSurfaces
from fitting_SurfaceData import fitDataAnisotropicScaleRigidNoCorr_ModelTree_TwoSurfaces
from fitting_SurfaceData import writeIpdata, writeTransformation, writeTransformation_Scaling, writeTransformation_Rotation
from transform3D import transformRigid3D
from ExportData import writeComFile


def RegisterData(RegularData, TargetData, FibreData, current_study_name):

    ## current directory should be: Geometric_Models/DS_FE_Models
    ## NB: the input file names should not include the extension name. 
    print ''
    print '==================================================='
    print '== Step 1 : Align Surface Data with the LV Model =='
    print '==================================================='
    print ''
    
    ##### Step 1: Read in surface data files #####
    ### Step 1.1: Read in regular surface data
    filename = RegularData+'LVRegular_Epi.ipdata'
    print filename
    regularEpi = readIpdata(filename)
    numData_regularEpi = len(regularEpi)
    regularEndo = readIpdata(RegularData+'LVRegular_Endo.ipdata')
    numData_regularEndo = len(regularEndo)
    """
    writeComFile(RegularData+'LVRegular_Endo','LVRegular_Endo')
    os.system('cm ExportData.com')
    writeComFile(RegularData+'LVRegular_Epi','LVRegular_Epi')
    os.system('cm ExportData.com')
    """
    regularTotal = concatenate((regularEndo, regularEpi),0)	
    numData = len(regularTotal)
    print '== Step 1.1 : Read in regular model Data  =='
    print 'The total number data in the regular model is ', numData
    ################################ DS #############################################
    ### Step 1.2: Read in target surface data
    filenameEpi = TargetData + 'DS_Epi.ipdata'
    print filename
    targetEpi = readIpdata(filenameEpi)
    filenameEndo = TargetData + 'DS_Endo.ipdata'
    targetEndo = readIpdata(filenameEndo)
    targetTotal = concatenate((targetEndo, targetEpi),0)
    numData = len(targetTotal)
    print '== Step 1.2 : Read in target model Data  =='
    print 'The total number data in the target model is ', numData

    ##### Step 2: Transformation #####
    ### Step 2.1: Perform initial transformation of target data to match regular
    print '== Step 2.1: Translate, rotate and scale the regular data to match regular data =='

    [transVector, regularEpi_transformed, regularEndo_transformed] = fitDataAnisotropicScaleRigidNoCorr_ModelTree_TwoSurfaces(regularEpi, regularEndo, targetEpi, targetEndo, 'transMatrixScaling.TRN', xtol=1e-5, maxfev=0, t0=None)

"""
    print 'Transformation Vector from target to regular with Anisotropic Scaling is '
    print transVector
    print ' '
    #transMatrix_Full = constructTransformationMatrix(transVector)

    ### Step 2.2: Inverse transform to rotate target data. 
    targetEpi_invTrans = inverseTransformRigid3D(targetEpi, scipy.hstack(transVector))
    targetEndo_invTrans = inverseTransformRigid3D(targetEndo, scipy.hstack(transVector))
    
    ### Step 3: Write out transformed data #####
    print '== Step 3: Write out final transformed target data =='
    writeIpdata(targetEpi_invTrans, TargetData+'DS_Epi_registered.ipdata', header='RegisteredEpi')
    writeComFile(TargetData+'DS_Epi_registered','RegisteredEpi')
    os.system('cm ExportData.com')

    writeIpdata(targetEndo_invTrans, TargetData+'DS_Endo_registered.ipdata', header='RegisteredEndo')
    writeComFile(TargetData+'DS_Endo_registered','RegisteredEndo')
    os.system('cm ExportData.com')

    ################################ ED and ES #####################################   
    frames = ['ED', 'ES']
    for i in range (0, 2):
        ### Step 1.2: Read in target surface data
        filename = TargetData + frames[i]+'_Epi.ipdata'
        print filename
        targetEpi = readIpdata(filename)
        targetEndo = readIpdata(TargetData+frames[i]+'_Endo.ipdata')
        targetTotal = concatenate((targetEndo, targetEpi),0)
        numData = len(targetTotal)
        print '== Step 1.2 : Read in target model Data  =='
        print 'The total number data in the target model is ', numData

        # Calculate mean coordinate in all three directions of regular model data
        print targetEpi
        x_coor = targetEpi[:,0]
        y_coor = targetEpi[:,1]
        z_coor = targetEpi[:,2]
        x_coor_mean=scipy.mean(x_coor)
        y_coor_mean=scipy.mean(y_coor)
        z_coor_mean=scipy.mean(z_coor)

        # Translate the target model so that its mean coordinates also lie on the origin.
        targetEpi_trans = transformRigid3D(targetEpi, scipy.hstack(([-x_coor_mean, -y_coor_mean, -z_coor_mean, 0.0, 0.0, 0.0]))) 
        targetEndo_trans = transformRigid3D(targetEndo, scipy.hstack(([-x_coor_mean, -y_coor_mean, -z_coor_mean, 0.0, 0.0, 0.0])))
        writeIpdata(targetEpi_trans, TargetData+frames[i]+'_Epi_translated.ipdata', header='LVtarget_Epi_translated')
        writeIpdata(targetEndo_trans, TargetData+frames[i]+'_Endo_translated.ipdata', header='LVtarget_Epi_translated')
        targetEpi = targetEpi_trans
        targetEndo = targetEndo_trans 

        ### Step 2.2: Inverse transform to rotate target data. 
        targetEpi_invTrans = inverseTransformRigid3D(targetEpi, scipy.hstack(transVector))
        targetEndo_invTrans = inverseTransformRigid3D(targetEndo, scipy.hstack(transVector))
    

        ### Step 3: Write out transformed data #####
        print '== Step 3: Write out final transformed target data =='
        writeIpdata(targetEpi_invTrans, TargetData+frames[i]+'_Epi_registered.ipdata', header='RegisteredEpi')
        writeComFile(TargetData+frames[i] +'_Epi_registered','RegisteredEpi')
        os.system('cm ExportData.com')

        writeIpdata(targetEndo_invTrans, TargetData+frames[i]+'_Endo_registered.ipdata', header='RegisteredEndo')
        writeComFile(TargetData+frames[i] +'_Endo_registered','RegisteredEndo')
        os.system('cm ExportData.com')
    
    ############################# Fibre data #######################################
        ############################# Fibre data #######################################

    filename = FibreData+'DS_'+current_study_name+'_FibreVector.txt'
    print filename
    [coordsData, fibreData] = readFibreIpdata(filename)
    numData = len(fibreData)

    print '== Step 4: Transform fibre data =='
    coordsData_invTrans = inverseTransformRigid3D(coordsData, scipy.hstack(transVector))
    fibreData_invTrans = inverseTransformRigid3D(fibreData, scipy.hstack(transVector))

    writeFibreIpdata(fibreData_invTrans, filename, FibreData+'DS_'+current_study_name+'FibreVector_registered.ipdata')
"""


