""" 
This script does the geometric fitting of the regular LV model to each of the Challenge studies to give the DS model for each study.

Author: Jenny Zhinuo Wang
Date: 28th April 2014

"""
import dircache
import os,sys
import shutil
from shutil import copy, move


from RegisterData import RegisterData
from RewriteFibreFittingFiles import Rewrite_Fibre_Fitting_Comfiles
import re

def Main(current_study_name):
    """
    ### Step 1: Register challenge data with regular model ######
    RegularData = '/hpc/zwan145/MICCAI_Challenge/Geometric_Models/DS_FE_Models/CreateRegularModel/'
    StudyData= '/hpc/zwan145/MICCAI_Challenge/Geometric_Models/Surface_Data/'+current_study_name+'/'
    FibreData='/hpc/zwan145/MICCAI_Challenge/Geometric_Models/Fibre/'+current_study_name+'/'
    print '==== Registering data to regular model ====\n'
    RegisterData(RegularData, StudyData, FibreData, current_study_name)

    ### Step 2: Fit the regular model to the registered data #####
    # Copy over template files into fitting folder. 
    all_files = os.listdir('GeometricFitting_Template')
    os.chdir('GeometricFitting_Template')
    for file in all_files:
        copy(file, '../Geometric_Fitting/')

    os.chdir('../')
    print 'Geometric Fitting Template files copied.\n'

    # Copy regular model into generic name for fitting
    copy('CreateRegularModel/rc_arithmetic.ipnode', 'Geometric_Fitting/LVCanineModel.ipnode')
    copy('CreateRegularModel/rc_arithmetic.ipelem', 'Geometric_Fitting/LVCanineModel.ipelem')
    copy(StudyData + 'DS_Endo.ipdata', 'Geometric_Fitting/BackTransformedUPFFinalRotated_Endo.ipdata')
    copy(StudyData + 'DS_Epi.ipdata', 'Geometric_Fitting/BackTransformedUPFFinalRotated_Epi.ipdata') 
    copy('transMatrixScaling.TRN', 'Geometric_Fitting/TransformationMatrix_Scaling.TRN')
    os.chdir('Geometric_Fitting')  
    
    #print "==== Start Geometric Fitting ===='
    os.system('cm LVHumanDSModel.com')

    # Copy over fitted files into DS_FE_Models folder with the study name attached.
    # Note that the fitted filename has postscript of fitted. Once fibre information
    # has been included, the filename will become just e.g. DS_Endo  
    os.chdir('../')
    if not os.path.exists(current_study_name):
        os.mkdir(current_study_name)
    copy('Geometric_Fitting/fitted_epi_humanLV.ipnode', current_study_name+'/DS_fitted.ipnode')
    copy('Geometric_Fitting/fitted_epi_humanLV.ipelem', current_study_name+'/DS_fitted.ipelem')
    copy('Geometric_Fitting/fitted_epi_humanLV.exnode', current_study_name+'/DS_fitted.exnode')
    copy('Geometric_Fitting/fitted_epi_humanLV.exelem', current_study_name+'/DS_fitted.exelem')
    copy('Geometric_Fitting/Endofittingerror.exdata', current_study_name+'/EndoFittingError.exdata')
    copy('Geometric_Fitting/Epifittingerror.exdata', current_study_name+'/EpiFittingError.exdata')
    """
    ### Step 3(a): Embed fibre orientation data #####
    print '==== Start fibre fitting ====\n'
    # Customise fibre fitting comfiles.
    os.chdir('../')
    Rewrite_Fibre_Fitting_Comfiles(current_study_name)
    # Run fibre fitting comfiles. 
    os.chdir('Fibre/')
    os.system('/people/ywan215/cm_expwallvec/cm/bin/x86_64-linux/cm MainFitting.com')

    ### Step 3(b): Embed sheet orientation data #####
        
########################################################################################

### Set up output log file. 
so = se = open("FibreFitting.log", 'w', 0)
# re-open stdout without buffering
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

# redirect stdout and stderr to the log file opened above
os.dup2(so.fileno(), sys.stdout.fileno())
os.dup2(se.fileno(), sys.stderr.fileno())

os.system('rm *.*~')
### Step 0: Address each study ######
study_ID = ['0912', '0917', '1017', '1024']
no_studies = len(study_ID)

print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
print '      The total number of studies is',no_studies 
print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
for i in range (0,1):
#for i in range (0, no_studies):
    current_study_name = study_ID[i]
    Main(current_study_name)
