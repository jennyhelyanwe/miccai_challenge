	

import os
import shutil
from shutil import copy
import fnmatch

#=========================================================================================================#
def CopyData(no_studies,study_ID):

	####### Copy surface data int correct study folder in Data #############
	## Copy over data files for Diastole and Systole into correct folders in Data


	for i in range(no_studies):

		current_study_name=study_ID[i]
		os.chdir('../SurfacePoints')
		## If the data folder for the current study doesn't exist, then create a new
		## directory
		if not os.path.exists(current_study_name): 
			print 'Creating directory for study '
			os.mkdir(current_study_name)

		## Copy all surface data points for the current study
		os.chdir(current_study_name)
		dst=os.getcwd()
		print dst
		os.chdir('../../../GenerateSurfacePoints_AllFrames/' + current_study_name)

		for file in os.listdir('.'):
			if fnmatch.fnmatch(file, '*.ipdata'):
				copy(file,dst)
	
		os.chdir(dst)
		os.chdir('../')

	########################################################################

	################ Copy the pressure data into Data ######################
	## Copy the pressure file for the current study
	os.chdir('../PressureTrace')
	dst=os.getcwd()
	os.chdir('../../../HaemoDataFromSTF/RegisteredPressure/')

	for file in os.listdir('.'):
		copy(file,dst)

	os.chdir(dst)
	os.chdir('../PYTHON_Codes')
	########################################################################
