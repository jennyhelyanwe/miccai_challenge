"""
This function is written to write the ipinit file based on the nodal displacement
Author: Vicky Wang
"""
		
##############################################################################################################
def Rewrite_Ipinit_MF(fileName,Node31_def,Node32_def,Node33_def,Node34_def,fileNamew, toggle):
	
		try:
				file = open( fileName, 'r' )
				filew=open(fileNamew,'w')
		except IOError:
				print 'ERROR: Rewrite_Ipinit: unable to open', fileName
				return

		
		## Read a single line at a time
		no_line=0
		data = file.readline()
		no_line=no_line+1
		if (toggle == 'DS'):
			while (no_line<=374):
					if (no_line==16):
						string = '    1\n'
					elif (no_line==80): 		## Node 31-x
						string=' The increment is [0.0]:    ' + str(Node31_def[0]) + '\n'
					elif (no_line==93):	## Node 32-x
						string=' The increment is [0.0]:    ' + str(Node32_def[0]) + '\n'
					elif (no_line==106):	## Node 33-x
						string=' The increment is [0.0]:    ' + str(Node33_def[0]) + '\n'
					elif (no_line==119):	## Node 34-x
						string=' The increment is [0.0]:    ' + str(Node34_def[0]) + '\n'
					elif (no_line==192):	## Node 31-y
						string=' The increment is [0.0]:    ' + str(Node31_def[1]) + '\n'
					elif (no_line==205):	## Node 32-y
						string=' The increment is [0.0]:    ' + str(Node32_def[1]) + '\n'
					elif (no_line==218):	## Node 33-y
						string=' The increment is [0.0]:    ' + str(Node33_def[1]) + '\n'
					elif (no_line==231):	## Node 34-y
						string=' The increment is [0.0]:    ' + str(Node34_def[1]) + '\n'
					elif (no_line==304):	## Node 31-z
						string=' The increment is [0.0]:    ' + str(Node31_def[2]) + '\n'
					elif (no_line==317):	## Node 32-z
						string=' The increment is [0.0]:    ' + str(Node32_def[2]) + '\n'
					elif (no_line==330):	## Node 33-z
						string=' The increment is [0.0]:    ' + str(Node33_def[2]) + '\n'
					elif (no_line==343):	## Node 34-z
						string=' The increment is [0.0]:    ' + str(Node34_def[2]) + '\n'
					else :
						string=data
				
					filew.write(str(string))
					data = file.readline()
					no_line=no_line+1
		else:
			while (no_line<=2919):
					if (no_line==16):
						string = '    2\n'
					elif (no_line==137): 		## Node 31-x
						string=' The increment is [0.0]:    ' + str(Node31_def[0]) + '\n'
					elif (no_line==150):	## Node 32-x
						string=' The increment is [0.0]:    ' + str(Node32_def[0]) + '\n'
					elif (no_line==163):	## Node 33-x
						string=' The increment is [0.0]:    ' + str(Node33_def[0]) + '\n'
					elif (no_line==176):	## Node 34-x
						string=' The increment is [0.0]:    ' + str(Node34_def[0]) + '\n'
					elif (no_line==(306)):	## Node 31-y
						string=' The increment is [0.0]:    ' + str(Node31_def[1]) + '\n'
					elif (no_line==(319)):	## Node 32-y
						string=' The increment is [0.0]:    ' + str(Node32_def[1]) + '\n'
					elif (no_line==(332)):	## Node 33-y
						string=' The increment is [0.0]:    ' + str(Node33_def[1]) + '\n'
					elif (no_line==(345)):	## Node 34-y
						string=' The increment is [0.0]:    ' + str(Node34_def[1]) + '\n'
					elif (no_line==(475)):	## Node 31-z
						string=' The increment is [0.0]:    ' + str(Node31_def[2]) + '\n'
					elif (no_line==(488)):	## Node 32-z
						string=' The increment is [0.0]:    ' + str(Node32_def[2]) + '\n'
					elif (no_line==(501)):	## Node 33-z
						string=' The increment is [0.0]:    ' + str(Node33_def[2]) + '\n'
					elif (no_line==(514)):	## Node 34-z
						string=' The increment is [0.0]:    ' + str(Node34_def[2]) + '\n'
					else :
						string=data
				
					filew.write(str(string))
					data = file.readline()
					no_line=no_line+1
	
		file.close()	
		filew.close()
	
		return	
##############################################################################################################
