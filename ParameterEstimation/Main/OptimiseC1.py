"""
This function is designed to optimise C1 to best match ED surface points
--Author: Vicky Wang
--Date: 10/11/2011
"""

import os
import scipy
import numpy
import math
import re
import fnmatch

from scipy import array,concatenate
from scipy import spatial
from scipy.optimize import leastsq, fmin,fmin_bfgs,fmin_cg,fminbound,fmin_slsqp


#==========================================================================#
def optimisePassiveStiffness(const_model_type,current_study_name):

	""" 
	Set up objective function and optimise C1
	"""
	
	#==============================================================================================================================#
	def obj(params_to_opt,const_model_type):

		
		if fnmatch.fnmatch(const_model_type, 'H'):
			ipcell_temp='holzapfel_struct_modify_TEMPLATE.ipcell'
			ipcell_final='holzapfel_struct_modify.ipcell'
			##           M,  N,xendo,xperi
			parameters=[params_to_opt[0],1.0,1.0,1.0]
			createIPCELL(ipcell_temp,parameters,ipcell_final)
			
			print '=================================================================================='
			print ''
			print 'Evaluating the objective function with Holzapfel parameters: M = ',params_to_opt[0]
			print '=================================================================================='
		
			os.system('cm OptimiseCAPPassive_Holzapfel.com')
			
		elif fnmatch.fnmatch(const_model_type, 'G'):
			createIPMATE(params_to_opt[0])
			print '=================================================================================='
			print ''
			print 'Evaluating the objective function with Guccione parameters: C1 = ',params_to_opt[0]
			print '=================================================================================='
			
			os.system('cm OptimiseCAPPassive_Guccione.com')
		
		
		print '================================================================================'
		print 'Finished solving mechanics with current passive parameters =',params_to_opt[0]
		print '================================================================================'

		
		########## The following commands extract RMSE error from opdata files ######
		## Read in the opdata files and combine to calculate the overall RMS error
		filename='EpiProjectionToED.opdata'
		readdata = open(filename,'rb').read()
		array = re.split(r'[\n\\]', readdata)
		temp = array[3].split()
		no_points_Epi=float(temp[len(temp)-1])
		temp = array[7].split()
		Epi_RMSE=float(temp[len(temp)-1])

		##
		filename='EndoProjectionToED.opdata'
		readdata = open(filename,'rb').read()
		array = re.split(r'[\n\\]', readdata)
		temp = array[3].split()
		no_points_Endo=float(temp[len(temp)-1])
		temp = array[7].split()
		Endo_RMSE=float(temp[len(temp)-1])

		## Calculate overall mean squared error
		MSE=(Epi_RMSE*Epi_RMSE*no_points_Epi+Endo_RMSE*Endo_RMSE*no_points_Endo)/(no_points_Epi+no_points_Endo)
		print 'Current Mean-Squared-Error =',MSE
		print ''
		#output_tmp = scipy.array([opt_Iteration,C1,MSE])
		#opt_Output.append(output_tmp)
		#opt_Output=scipy.array(opt_Output)
		#opt_Iteration +=1
		#print 'Current result'
		#print opt_Output
		return MSE
		
		"""
		## The following commands extract the error vectors ###
		## Epi data 
		filename='CAPEDEpi_Error_ToED.exdata'
		error_mag_epi=readExdata_Error(filename)
		## Calculate the norm of error_mag
		RMSE_epi=numpy.linalg.norm(error_mag_epi)/math.sqrt(len(error_mag_epi))
		print 'Passive: RMS error for epi is ', RMSE_epi

		## Endo data
		filename='CAPEDEndo_Error_ToED.exdata'
		error_mag_endo=readExdata_Error(filename)	
		## Calculate the norm of error_mag
		RMSE_endo=numpy.linalg.norm(error_mag_endo)/math.sqrt(len(error_mag_endo))
		print 'Passive: RMS error for endo is ', RMSE_endo
		
		error= error_mag_epi + error_mag_endo
		
		return error
		"""
	#===============================================End of obj(C1) ====================================================#
		
	print ' '
	print '***************** Start Passive Mechanics *******************'
	print ' '
	
	## If no previous solution is available, then do an initial solve
	## Otherwise comment out the following three lines
	## Vicky' passive material parameters
	os.chdir('../'+ current_study_name + '/LVMechanics_' + current_study_name + '/PassiveMechanics/') 
	
	if fnmatch.fnmatch(const_model_type, 'Holzapfel'):
		print 'Setting Scaling Parameters M & N to be 1'
		ipcell_temp='holzapfel_struct_modify_TEMPLATE.ipcell'
		ipcell_final='holzapfel_struct_modify.ipcell'
		##           M,  N,xendo,xperi
		parameters=[1.0,1.0,1.0,1.0]
		print parameters
		createIPCELL(ipcell_temp,parameters,ipcell_final)
		os.system('cm SolveInitialInflation_Holzapfel.com')
		## Set initial guess for parameters
		M=1.138
		params_to_opt=M

	elif fnmatch.fnmatch(const_model_type, 'Guccione'):
		print 'Setting prarameters to be the same as Vicky Canine Parameters'
		createIPCELL(2.8)	
		os.system('cp LV_CubicGuc_temp.ipmate LV_CubicGuc.ipmate')
		os.system('cm SolveInitialInflation_Guccione.com')
		C1_Init=5.0000000
		params_to_opt=C1_Init

	####################Initialising the problem ##########################
	os.system('cp output/LV_Inflation_10.ipinit output/LV_Inflation_OptED.ipinit')
	## Inflated model solved using Vicky's material parameters
	os.system('cp -f InitialInflated.ipinit CurrentInflated.ipinit')
	## Record initial RMS error
	os.system('cp EpiProjectionToED.opdata EpiProjectionToED_Initial.opdata')
	os.system('cp EndoProjectionToED.opdata EndoProjectionToED_Initial.opdata')
	os.system('cp CAPEDEpi_Error_ToED.exdata CAPEDEpi_Error_ToED_Initial.exdata')
	os.system('cp CAPEDEndo_Error_ToED.exdata CAPEDEndo_Error_ToED_Initial.exdata')

	"""
	## Initialise the counter for optimisation iterations
	print '    '
	print '**************** Start Optimisation ****************'
	print '    '
	
	#Opt_params=fmin_slsqp(obj,params_to_opt,epsilon=1e-1)
			
	Opt_params=fmin_bfgs(obj,params_to_opt,args=const_model_type[0],gtol = 1e-2,epsilon=1e-1,full_output=1)

	#Opt_params = fmin(obj(params_to_opt,opt_Iteration,opt_Output),C1_Init,xtol=1e-6,ftol=1e-3)
	#Opt_params = leastsq(obj,params_to_opt,epsfcn=1e-2,factor=0.5)

	# Write the optimal parameter to file
	if fnmatch.fnmatch(const_model_type, 'Holzapfel'):
		ipcell_temp='holzapfel_struct_modify_TEMPLATE.ipcell'
		ipcell_final='holzapfel_struct_modify.ipcell'
		##           M,  N,xendo,xperi
		parameters=[Opt_params[0][0],1.0,1.0,1.0]
		createIPCELL(ipcell_temp,parameters,ipcell_final)
	elif fnmatch.fnmatch(const_model_type, 'Guccione'):
		createIPMATE(Opt_params[0][0])
	
	"""
	return 
#==========================================================================#

#==========================================================================#	
def createIPMATE(C1):

	""" write the ipmate file with the current value of C1
	"""
	
	comand="awk -v param="+"%12.12f" % ( C1 ) +" -f createIPMATE.awk LV_CubicGuc_TEMPLATE.ipmate > LV_CubicGuc_temp.ipmate"

	os.system(comand)
#==========================================================================#


#======================================================================#
def readExdata_Error( fileName ):
	""" reads exdata file and returns the error vectors for each data point
	"""
	
	try:
		file = open( fileName, 'r' )
	except IOError:
		print 'ERROR: readExdata_Error: unable to open', fileName
		return
	

        ## Read in the header (10 lines)
	no_header_line=10
	for i in range(0, no_header_line) :
		header_line=file.readline()

	
	error_post=[]
	error_vector=[]
	## Start to read the error vectors
	data_info=file.readline()
	
	## Continue to read until it reaches the end of file
	while len(data_info) !=0 :
			data_info=file.readline()
			error_post.append([float(data) for data in data_info.split()[0:3]])
			data_info=file.readline()			
			error_vector.append([float(data) for data in data_info.split()[0:3]])
			data_info=file.readline()
	
	error_post = array (error_post)
	error_vector = array (error_vector)


	## Calculate the magnitude for the error vector
	error_mag=[]
	for i in range(0,len(error_vector)):
		error_mag.append(numpy.linalg.norm(error_vector[i]))


	## Calculate the norm of error_mag
	RMSE=numpy.linalg.norm(error_mag)/math.sqrt(len(error_mag))


	return error_mag
#======================================================================#

#==============================================================================================================================#
def createIPCELL(filename,passive_parameters,filenamew):

	""" 
	write the ipcell file with the current values of passive_parameters
	"""

	try: 
			file = open(filename, 'r')
			filew = open(filenamew, 'w')
	except IOError:
			print 'ERROR: Update_Ipinit: unable to open ', filename
			return


	# Read in a single line at a time
	no_line = 0
	data = file.readline()
	no_line = no_line + 1

	while (no_line <= 140):
		if (no_line == 61) :
			for parameter_no in range (10):
				for no_line_sub in range(6):

					if ((no_line_sub==4) and parameter_no<=3) :
						parameter=float(data.split()[-1])*passive_parameters[0] ## M*a
						string = '   Initial value [0.0000E+00]: ' + str(parameter) + '\n'
					elif ((no_line_sub==4) and parameter_no>3 and parameter_no<=7) :
						parameter=float(data.split()[-1])*passive_parameters[1] ## N*b
						string = '   Initial value [0.0000E+00]: ' + str(parameter) + '\n'
					elif ((no_line_sub==4) and parameter_no==8) :	
						parameter=passive_parameters[2] ## xendo
						string = '   Initial value [0.0000E+00]: ' + str(parameter) + '\n'
					elif ((no_line_sub==4) and parameter_no==9) :	
						parameter=passive_parameters[3] ## xperi
						string = '   Initial value [0.0000E+00]: ' + str(parameter) + '\n'
					else :
						string = data

					filew.write(str(string))
					data = file.readline()
					no_line=no_line + 1
					
					## End of lines for each parameters
			
				string = data ## This is the blank line between each parameter
				filew.write(str(string))
				data = file.readline()
				no_line=no_line + 1
				## End of all parameters

		else :
			string = data		
			filew.write(str(string))
			data = file.readline()
			no_line=no_line + 1
			## Rest of lines
	
	file.close()
	filew.close()
	
#=============================================End of createIPCELL(M) =========================================================#

