"""
This main function is designed to automatically implement the following steps
(1) convert CIM guide-point models to exnode format, and then ipnode format
(2) generate surface data points from the ED CIM models
(3) perform passive mechanics simulation and estimate C1

-- Author: Vicky Wang (29/11/2013)
"""

####### Generic python scripts #######
import dircache
import os,sys
import shutil
from shutil import copy
import re
import scipy
import fnmatch

####### Case-specific python scripts #######
from CopyData import CopyData
from InitialSetup import InitialSetup
from SetupGeometricModel import SetupGeometricModel
from CreateCavityModel_NonZeroYZ import CreateCavityModel
from MainPassive import Main_Program_Passive
#from MainPassive_MF import Main_Program_Passive_MF
from MainActive import Main_Program_Active
#from Rewrite_Ipinit_MF import Rewrite_Ipinit_MF
from OptimiseC1 import optimisePassiveStiffness,createIPMATE
#from OptimiseC1MF import optimisePassiveStiffness_MF,createIPMATE
from writeCMGUIfile import writeCMGUIfile
from extractNodalDisp import extractNodalIndices,extractNodalDisp,extractNodalDisp_MF


#==================================================================================================================#
def Main_Program(current_study_name):

	
	print ''
	print '******************************************************************'
	print '  (0) Initial set-up for Study ',current_study_name
	print '******************************************************************'
	print ''

	frame_pressure=InitialSetup(current_study_name)

	print ''
	print '******************************************************************'
	print '  Finished initial set-up for Study ',current_study_name
	print '******************************************************************'
	print ''
		
	
	print ''
	print '******************************************************************'
	print '  (1) Set Up Geometric Model for Study ',current_study_name
	print '******************************************************************'
	print ''
		
	#Node_data=SetupGeometricModel(current_study_name)

	print ''
	print '******************************************************************'
	print '  Finished Setting Up Geometric Model for Study ',current_study_name
	print '******************************************************************'
	print ''
		

	print ''
	print '******************************************************************'
	print '(2) Start Solving Passive Mechanics for Study ',current_study_name
	print '******************************************************************'
	print ''


	#Main_Program_Passive(const_model,current_study_name,frame_pressure,Node_data)
	## This is the multi-frame script
	#Main_Program_Passive_MF(const_model,current_study_name,current_study_frame, frame_pressure,Node_data)


	print ''
	print '******************************************************************'
	print '      Finished Optimising Passive Properties for Study ',current_study_name
	print '******************************************************************'
	print ''
		
	print ''
	print '******************************************************************'
	print '(3) Start Solving Active Mechanics for Study ',current_study_name
	print '******************************************************************'
	print ''
		
	#Main_Program_Active(const_model,current_study_name,current_study_frame, frame_pressure,Node_data)
		
	print ''
	print '******************************************************************'
	print '    Finished Solving Active Mechanics for Study ',current_study_name
	print '******************************************************************'
	print ''
		
		
	return
#================================================================================================================================#

os.system('rm *.*~')
#####################################################################
###################### List the Data Files ##########################
#####################################################################

## List all files in the ../Data folder
#all_files_list=dircache.listdir('../Data')

## Count the number of files in the folder
## Each study has 6 files associated with it
# no_studies=len(all_files_list)/8
	
################### Obtain ID for all studies ######################	
## Read a txt file which lists the frame number for all studies
file = open( '../STACOMChallenge_studyID.txt', 'r' )

study_ID=[]
#study_frame_tmp=scipy.zeros((0,3), int)
#study_frame=[]

study_infor=file.readline()


while len(study_infor) != 0 :	# Reaching the end of the file
	
	a=study_infor.split()[0]
	study_ID.append(a)
	#study_frame_tmp=study_infor.split()[1:5]
	#study_frame.append(study_frame_tmp)
	study_infor=file.readline()

no_studies = len(study_ID)
print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
print '      The total number of studies is',no_studies 
print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++'


########################################################################


#CopyData(no_studies,study_ID)


##################### Start Material Parameter estimation ##############

const_model='Holzapfel'

for i in range(1):
#for i in range(no_studies):
#for i in range(1):

	current_study_name=study_ID[i]
	"""
	current_file=[]
			
	
	## Select files asocaited with the current study
	for file in all_files_list :
			if re.search(str(current_study_name),str(file)) :
				current_file.append(file)
	"""		
	print ''
	print '*****************************************************************'
	print '          Current Study Name is ',current_study_name
	print '*****************************************************************'
	print ''
	#print current_study_frame
	Main_Program(current_study_name)

