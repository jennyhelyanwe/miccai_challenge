

import os
import shutil
from shutil import copy
from CreateCavityModel_NonZeroYZ import CreateCavityModel
from extractNodalDisp import extractNodalIndices,extractNodalDisp_MF,extractNodalDisp

#=========================================================================================================#
def SetupGeometricModel(current_study_name, current_study_frame):
		################# Define important frame numbers ############
		DS_frame,ED_frame,ES_frame, total_frame=tuple(current_study_frame)
		DS_frame = int(DS_frame)
		ED_frame = int(ED_frame)
		ES_frame = int(ES_frame)
		total_frame = int(total_frame)
		print DS_frame,ED_frame,ES_frame,total_frame		

		## Copy the important frame data from CIM_Models to the Geometric_Model
		## folder. 
		os.chdir('../'+current_study_name + '/GeometricModel_' + current_study_name)
		dstGeometry = os.getcwd()
		os.chdir('../../../Surface_Points/'+current_study_name+'/Passive')

		copy(current_study_name + '_'+ str(DS_frame)+'.ipelem',dstGeometry + '/CAP_DS_humanLV.ipelem')
		copy(current_study_name + '_'+ str(DS_frame)+'.ipnode',dstGeometry + '/CAP_DS_humanLV.ipnode')
		copy(current_study_name + '_Surface_Points_Endo_'+ str(DS_frame)+'.ipdata',dstGeometry + '/Surface_Points_Endo_DS.ipdata')	
		copy(current_study_name + '_Surface_Points_Epi_'+ str(DS_frame)+'.ipdata',dstGeometry + '/Surface_Points_Epi_DS.ipdata')
		copy(current_study_name + '_Surface_Points_Endo_'+ str(ED_frame)+'.ipdata',dstGeometry + '/Surface_Points_Endo_ED.ipdata')
		copy(current_study_name + '_Surface_Points_Epi_'+ str(ED_frame)+'.ipdata',dstGeometry + '/Surface_Points_Epi_ED.ipdata')

		os.chdir('../Active')		
		copy(current_study_name + '_Surface_Points_Endo_'+ str(ES_frame)+'.ipdata',dstGeometry + '/Surface_Points_Endo_ES.ipdata')
		copy(current_study_name + '_Surface_Points_Epi_'+ str(ES_frame)+'.ipdata',dstGeometry + '/Surface_Points_Epi_ES.ipdata')

		## Copy the geometric template files over
		all_files = os.listdir('../../../Multiframe_Method/GeometricModel_TemplateFiles')
		os.chdir('../../../Multiframe_Method/GeometricModel_TemplateFiles')
		for file in all_files:
			copy(file, dstGeometry)
		os.chdir(dstGeometry)

		## Create the cavity model
		fileName_wall='CAP_DS_humanLV.ipnode'
		fileName_cavity='LVCavity.ipnode'
		CreateCavityModel(fileName_wall,fileName_cavity)
		## Export model
		os.system('cm LVHumanDSModel.com')


		## Extract data points to construct ipinit file for first solve
		fileNameDS = 'CAP_DS_humanLV.ipnode'
		fileNameSurfaceDS = 'Surface_Points_Epi_DS.ipdata'
		# Find indices of surface data which are closest to the four epi nodes at the base of model. 
		Node_data = extractNodalIndices(fileNameDS, fileNameSurfaceDS)	
	
		## Copy the wall and cavity meshes to the mechanics folder
		dst='../LVMechanics_' + current_study_name + '/PassiveMechanics/'
		copy('CAP_DS_humanLV.ipnode',dst)
		copy('CAP_DS_humanLV.ipelem',dst)
		copy('LVCavity.ipnode',dst)
		copy('LVCavity.ipelem',dst)
		copy('Surface_Points_Epi_DS.ipdata',dst)
		copy('Surface_Points_Endo_DS.ipdata',dst)
		copy('Surface_Points_Epi_ED.ipdata',dst)
		copy('Surface_Points_Endo_ED.ipdata',dst)

		dst = '../LVMechanics_' + current_study_name + '/ActiveMechanics'
		copy('CAP_DS_humanLV.ipnode',dst)
		copy('CAP_DS_humanLV.ipelem',dst)
		copy('LVCavity.ipnode',dst)
		copy('LVCavity.ipelem',dst)
		copy('Surface_Points_Epi_DS.ipdata',dst)
		copy('Surface_Points_Endo_DS.ipdata',dst)
		copy('Surface_Points_Epi_ES.ipdata',dst)
		copy('Surface_Points_Endo_ES.ipdata',dst)

		os.chdir('../../Main')

		return Node_data

