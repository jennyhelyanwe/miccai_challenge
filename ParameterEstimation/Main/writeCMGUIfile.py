"""
This python code is designed to write the cmgui com file to visualise transformation
Author: Vicky Wang
"""

##################################################################################
def writeCMGUIfile(current_study_name):

		try:
				fileName='../../GeometricModel_TemplateFiles/ViewAllTransformation.com'
				filer = open( fileName, 'r' )
		except IOError:
				print 'ERROR: writeCMGUIfile', fileName
				return
		
		## Define file name 
		fileNamew='ViewAllTransformation.com'
		filew=open(fileNamew,'w')
		
		groupname=current_study_name.replace("-","")

		## Read a single line at a time
		no_line=0
		data = filer.readline()
		no_line=no_line+1
		
		while (no_line<=150):
				if (no_line==18):
					string='gfx read data '+ current_study_name + '_ds_lvepi_Trans\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvepi_Trans general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvepi_Trans lines select_on material default selected_material default_selected;\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvepi_Trans data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string=data
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx change data_offset 10000\n'
					filew.write(str(string))
					string='gfx read data '+ current_study_name + '_ds_lvendo_Trans\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvendo_Trans general clear circle_discretization 6 default_coordinate coordinates element_discretization "4*4*4" native_discretization none;\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvendo_Trans lines select_on material default selected_material default_selected;\n'
					filew.write(str(string))
					data = filer.readline()
					no_line=no_line+1
					string='gfx modify g_element '+groupname +'_ds_lvendo_Trans data_points glyph sphere general size "2*2*2" centre 0,0,0 font default select_on material green selected_material default_selected;\n'
					data = filer.readline()
					no_line=no_line+1
				else :
					string=data

				filew.write(str(string))
				data = filer.readline()
				no_line=no_line+1
				

	
		filer.close()	
		filew.close()
	
		return	
#############################################################################################################3

