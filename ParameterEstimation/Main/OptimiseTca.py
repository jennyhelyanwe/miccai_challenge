"""
This function is designed to optimise TCa to best match ED surface points
--Author: Vicky Wang
--Date: 10/11/2011
"""

import os
import scipy
import numpy
import math
import re
import fnmatch

from scipy import array,concatenate
from scipy import spatial

from scipy.spatial import cKDTree
from scipy.optimize import leastsq, fmin,fmin_bfgs,fmin_cg,fminbound,fmin_slsqp

#==========================================================================#
def optimiseTCa(const_model_type,current_study_name):

	""" Set up objective function and optimise TCa
	"""
	def obj(TCa,const_model_type):

		print '==================================================='
		print ''
		print 'Evaluating the objective function with TCa = ',TCa[0]
		print '==================================================='
		
		createIPACTI(TCa[0])
		
		if fnmatch.fnmatch(const_model_type, 'H'):
			os.system('/hpc/ywan215/cm_fixactivetension_NonInvert_Tca/cm/bin/x86_64-linux/cm OptimiseCAPActive_Holzapfel.com')
		elif fnmatch.fnmatch(const_model_type, 'G'):
			os.system('/hpc/ywan215/cm_fixactivetension_NonInvert_Tca/cm/bin/x86_64-linux/cm OptimiseCAPActive_Guccione.com')
			
		print '=================================================='
		print 'Finished solving mechanics with current TCa =',TCa[0]
		print '=================================================='

		
		########## The following commands extract RMSE error from opdata files ######
		## Read in the opdata files and combine to calculate the overall RMS error
		filename='EpiProjectionToES.opdata'
		readdata = open(filename).read()
		array = re.split(r'[\n\\]', readdata)
		temp = array[3].split()
		no_points_Epi=float(temp[len(temp)-1])
		temp = array[7].split()
		Epi_RMSE=float(temp[len(temp)-1])

		##
		filename='EndoProjectionToES.opdata'
		readdata = open(filename).read()
		array = re.split(r'[\n\\]', readdata)
		temp = array[3].split()
		no_points_Endo=float(temp[len(temp)-1])
		temp = array[7].split()
		Endo_RMSE=float(temp[len(temp)-1])

		## Calculate overall mean squared error
		MSE=(Epi_RMSE*Epi_RMSE*no_points_Epi+Endo_RMSE*Endo_RMSE*no_points_Endo)/(no_points_Epi+no_points_Endo)
		print 'Current Mean-Sqaured-Error =',MSE
		print ''
		#output_tmp = scipy.array([opt_Iteration,TCa,MSE])
		#opt_Output.append(output_tmp)
		#opt_Output=scipy.array(opt_Output)
		#opt_Iteration +=1
		#print 'Current result'
		#print opt_Output
		return MSE
	
		"""

		## The following commands extract the error vectors ###
		## Epi data 
		filename='CAPESEpi_Error_ToES.exdata'
		error_mag_epi=readExdata_Error(filename)
		## Calculate the norm of error_mag
		RMSE_epi=numpy.linalg.norm(error_mag_epi)/math.sqrt(len(error_mag_epi))
		print 'Active: RMS error for epi is ', RMSE_epi

		## Endo data
		filename='CAPESEndo_Error_ToES.exdata'
		error_mag_endo=readExdata_Error(filename)	
		## Calculate the norm of error_mag
		RMSE_endo=numpy.linalg.norm(error_mag_endo)/math.sqrt(len(error_mag_endo))
		print 'Active: RMS error for endo is ', RMSE_endo
		
		error= error_mag_epi + error_mag_endo

		return error
		"""

	## If there is no initial end-systole solution, then do a forward solve
	os.chdir('../'+ current_study_name + '/LVMechanics_' + current_study_name + '/ActiveMechanics/') 
	"""
	if fnmatch.fnmatch(const_model_type, 'Holzapfel'):
		os.system('/hpc/ywan215/cm_fixactivetension_NonInvert_Tca/cm/bin/x86_64-linux/cm SolveInitialSystole_Holzapfel.com')
	elif fnmatch.fnmatch(const_model_type, 'Guccione'):
		os.system('/hpc/ywan215/cm_fixactivetension_NonInvert_Tca/cm/bin/x86_64-linux/cm SolveInitialSystole_Guccione.com')
	"""	
	####################Initialising the problem ##########################
	print 'Setting TCa to be the initial guess'
	print ''
	os.system('cp -f calcium0xx_OptTCa_Initial.ipacti calcium0xx_OptTCa.ipacti')
	os.system('cp -f InitialEndSystole.ipinit CurrentEndSystole.ipinit')
	## Record initial RMS error
	os.system('cp EpiProjectionToES.opdata EpiProjectionToES_Initial.opdata')
	os.system('cp EndoProjectionToES.opdata EndoProjectionToES_Initial.opdata')
	os.system('cp CAPESEpi_Error_ToES.exdata CAPESEpi_Error_ToES_Initial.exdata')
	os.system('cp CAPESEndo_Error_ToES.exdata CAPESEndo_Error_ToES_Initial.exdata')
	
	## Set initial value
	TCa_Init=100
	print 'Start to optimise .....'
	
	TCaOpt=fmin_bfgs(obj,TCa_Init,args=const_model_type[0],gtol = 1e-4,epsilon=5e+0,full_output=1)
	#TCaOpt = fmin(obj,TCa_Init,args=const_model_type[0],xtol=1e-3,ftol=1e-3)
	#TCaOpt = fmin(obj(TCa_Init,opt_Iteration,opt_Output),TCa_Init,xtol=1e-6,ftol=1e-3)
	
	#TCaOpt = leastsq(obj, TCa_Init, xtol=1e-6,ftol=1e-12)[0]
	
	createIPACTI(TCaOpt[0][0])

	return
#==========================================================================#


#==========================================================================#	
def createIPACTI(TCa):

	""" write the ipacti file with the current value of TCa
	"""
	
	comand="awk -v param="+"%12.12f" % ( TCa ) +" -f createIPACTI.awk calcium0xx_OptTCa.ipacti > calcium0xx_OptTCa_temp.ipacti"
	os.system(comand)

#==========================================================================#

#======================================================================#
def readExdata_Error( fileName ):
	""" reads exdata file and returns the error vectors for each data point
	"""
	
	try:
		file = open( fileName, 'r' )
	except IOError:
		print 'ERROR: readExdata_Error: unable to open', fileName
		return
	

        ## Read in the header (10 lines)
	no_header_line=10
	for i in range(0, no_header_line) :
		header_line=file.readline()

	
	error_post=[]
	error_vector=[]
	## Start to read the error vectors
	data_info=file.readline()
	
	## Continue to read until it reaches the end of file
	while len(data_info) !=0 :
			data_info=file.readline()
			error_post.append([float(data) for data in data_info.split()[0:3]])
			data_info=file.readline()			
			error_vector.append([float(data) for data in data_info.split()[0:3]])
			data_info=file.readline()
	
	error_post = array (error_post)
	error_vector = array (error_vector)


	## Calculate the magnitude for the error vector
	error_mag=[]
	for i in range(0,len(error_vector)):
		error_mag.append(numpy.linalg.norm(error_vector[i]))


	## Calculate the norm of error_mag
	RMSE=numpy.linalg.norm(error_mag)/math.sqrt(len(error_mag))


	return error_mag
#======================================================================#




