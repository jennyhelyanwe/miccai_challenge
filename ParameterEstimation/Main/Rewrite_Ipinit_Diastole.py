"""
This function is written to write the ipinit file based on the nodal displacement
Author: Vicky Wang
"""
		
##############################################################################################################
def Rewrite_Ipinit_Diastole(fileName,LVP_ED,Node31_ED,Node32_ED,Node33_ED,Node34_ED,fileNamew):
	
		try:
				file = open( fileName, 'r' )
				filew=open(fileNamew,'w')
		except IOError:
				print 'ERROR: Rewrite_Ipinit: unable to open', fileName
				return

		
		## Read a single line at a time
		no_line=0
		data = file.readline()
		no_line=no_line+1

		while (no_line<=374):
				if (no_line==80): 		## Node 31-x
					string=' The increment is [0.0]:    ' + str(Node31_ED[0]) + '\n'
				elif (no_line==93):	## Node 32-x
					string=' The increment is [0.0]:    ' + str(Node32_ED[0]) + '\n'
				elif (no_line==106):	## Node 33-x
					string=' The increment is [0.0]:    ' + str(Node33_ED[0]) + '\n'
				elif (no_line==119):	## Node 34-x
					string=' The increment is [0.0]:    ' + str(Node34_ED[0]) + '\n'
				elif (no_line==192):	## Node 31-y
					string=' The increment is [0.0]:    ' + str(Node31_ED[1]) + '\n'
				elif (no_line==205):	## Node 32-y
					string=' The increment is [0.0]:    ' + str(Node32_ED[1]) + '\n'
				elif (no_line==218):	## Node 33-y
					string=' The increment is [0.0]:    ' + str(Node33_ED[1]) + '\n'
				elif (no_line==231):	## Node 34-y
					string=' The increment is [0.0]:    ' + str(Node34_ED[1]) + '\n'
				elif (no_line==304):	## Node 31-z
					string=' The increment is [0.0]:    ' + str(Node31_ED[2]) + '\n'
				elif (no_line==317):	## Node 32-z
					string=' The increment is [0.0]:    ' + str(Node32_ED[2]) + '\n'
				elif (no_line==330):	## Node 33-z
					string=' The increment is [0.0]:    ' + str(Node33_ED[2]) + '\n'
				elif (no_line==343):	## Node 34-z
					string=' The increment is [0.0]:    ' + str(Node34_ED[2]) + '\n'
				elif (no_line==360):	## Pressue load
					string=' The increment is [0.0]:    ' + str(LVP_ED) + '\n'
				else :
					string=data
				
				filew.write(str(string))
				data = file.readline()
				no_line=no_line+1
	
		file.close()	
		filew.close()
	
		return	
##############################################################################################################
