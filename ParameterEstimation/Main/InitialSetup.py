		
import os,sys


#=========================================================================================================#
def InitialSetup(current_study_name):

	####### Make a directory for the current study ############
	if not os.path.exists('../'+ current_study_name): 
		print 'Creating directory for study '
		print current_study_name
		os.mkdir('../'+ current_study_name)
		## Make directory for the geometric fitting for diastole
		os.mkdir('../'+ current_study_name + '/GeometricModel_' + current_study_name )
		## Make directory for solving mechanics
		os.mkdir('../'+ current_study_name + '/LVMechanics_'+ current_study_name)
		## Make directory for solving passive mechanics
		os.mkdir('../'+ current_study_name + '/LVMechanics_'+ current_study_name + '/PassiveMechanics')
		## Make directory for solving active mechanics
		os.mkdir('../'+ current_study_name + '/LVMechanics_'+ current_study_name + '/ActiveMechanics')
	#############################################################
	"""
	##################### Set up Output file ####################
	os.chdir('../'+ current_study_name)	
	#os.system('rm *.log')
	so = se = open("AllOutput.log", 'w', 0)
	
	# re-open stdout without buffering
	sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
	# redirect stdout and stderr to the log file opened above
	os.dup2(so.fileno(), sys.stdout.fileno())
	os.dup2(se.fileno(), sys.stderr.fileno())
	#############################################################
	"""
	##################### Extract pressure values ###############
	file = open('../../Boundary_Conditions/Pressure/'+current_study_name+'_PV_NoOffset.txt')

	frame_pressure=[0,0,0] # The pressure is stored in the order DS, ED, ES
	pressure_infor=file.readline() # First line contains labels. 
	pressure_infor=file.readline()

	while len(pressure_infor) != 0:
		frame_name=str(pressure_infor.split()[3])
		pressure_temp=float(pressure_infor.split()[1])
		if frame_name=='DS':
			frame_pressure[0]=pressure_temp
		elif frame_name=='ED':
			frame_pressure[1]=pressure_temp
		elif frame_name=='ES':
			frame_pressure[2]=pressure_temp
		pressure_infor=file.readline()

	print 'Pressure values for Study ',current_study_name
	print frame_pressure
	return frame_pressure
	#############################################################
