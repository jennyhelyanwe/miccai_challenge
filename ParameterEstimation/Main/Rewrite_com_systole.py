"""
This function is written to write the com file based on the subject-specific afterload
Author: Vicky Wang
"""
		
##############################################################################################################
def Rewrite_com_systole(fileName,LVP_ES,fileNamew):
	
		try:
				file = open( fileName, 'r' )
				filew=open(fileNamew,'w')
		except IOError:
				print 'ERROR: Rewrite_com_systole: unable to open', fileName
				return

		
		## Read a single line at a time
		no_line=0
		data = file.readline()
		no_line=no_line+1

		while (no_line<=140):
				if (no_line==4): 		## Define LVP at ES
					string='$Pee=' + str(LVP_ES) + ';\n'
				else :
					string=data
				
				filew.write(str(string))
				data = file.readline()
				no_line=no_line+1
	
		file.close()	
		filew.close()
	
		return	
##############################################################################################################
