"""
This main function is designed to estimate Tca at all frames during systole
-- Author: Vicky Wang 
-- Modified for multi-frame: Jenny Zhinuo Wang
-- Modified : Vicky Wang (26/11/2013)
"""

import dircache
import os,sys
import shutil

from shutil import copy
import re
import scipy
import fnmatch

from Rewrite_com_systole import Rewrite_com_systole
from Rewrite_Ipinit_MF import Rewrite_Ipinit_MF
from Rewrite_Ipinit_Systole import Rewrite_Ipinit_Systole
from OptimiseTca import optimiseTCa,createIPACTI
from extractNodalDisp import extractNodalIndices,extractNodalDisp,extractNodalDisp_Systole


#=====================================================#

def Main_Program_Active(const_model_type,current_study_name,current_study_frame, frame_pressure,Node_data):

		
		## Change directory to mechanics template files
		## Copy files to passive mechanics files
		all_files=os.listdir('../Mechanics_TemplateFiles/ActiveMechanics')
		os.chdir('../Mechanics_TemplateFiles/ActiveMechanics')
		dstActiveMechanics='../../' + current_study_name + '/LVMechanics_' + current_study_name + '/ActiveMechanics/'
		
		for file in all_files:
			copy(file,dstActiveMechanics)

		## Write com file
		if fnmatch.fnmatch(const_model_type, 'Holzapfel'):
			## Write the systole simulation file
			fileName='SolveIVCEjection_tmp_Holzapfel.com'
			fileNamew='../../'+ current_study_name + '/LVMechanics_'+ current_study_name + '/ActiveMechanics/SolveIVCEjection_Holzapfel.com'
		elif fnmatch.fnmatch(const_model_type, 'Guccione'):
			## Write the systole simulation file
			fileName='SolveIVCEjection_tmp.com'
			fileNamew='../../'+ current_study_name + '/LVMechanics_'+ current_study_name + '/ActiveMechanics/SolveIVCEjection_Guccione.com'
		
		LVP_ES=frame_pressure[int(current_study_frame[2])-1]
		Rewrite_com_systole(fileName,LVP_ES,fileNamew)
			
		print '==Finished writing com file for active mechanics with subject-specific ES pressure =='

		## Write ipinit
		os.chdir(dstActiveMechanics)
		
		fileName='CAP_DS_humanLV.ipnode'		
		[Node31_ES,Node32_ES,Node33_ES,Node34_ES]=extractNodalDisp_Systole(fileName)
		## Write ipinit file
		fileName='LV_CubicPreEpiBase_tmp.ipinit'
		fileNamew='LV_CubicPreEpiBase_tmp_2.ipinit'
		Rewrite_Ipinit_Systole(fileName,Node31_ES,Node32_ES,Node33_ES,Node34_ES,fileNamew)
		os.system('mv LV_CubicPreEpiBase_tmp_2.ipinit LV_CubicPreEpiBase_tmp.ipinit')
		print '==Finished writing ipinit file for active mechanics =='
		os.chdir('../../../PYTHON_Codes')
		
		
		## Start TCa_max estimation
		optimiseTCa(const_model_type,current_study_name)

		os.chdir('../../../PYTHON_Codes/')
		
		
##########################################################################################
#======================================================#


