##################################################
"""
This function is designed to extract nodal displacement from ipdata file
"""
import re
import numpy
import scipy
from scipy import array,append

def extractNodalIndices(fileName_DS, fileName_surfaceDS_Epi) :
	
	######################## Step 1: Read in the ipnode file ########################
	## Read in the fitted ipnde file
	file = open( fileName_DS, 'r' )

	## Read a single line at a time
	no_line=0
	data = file.readline()
	no_line=no_line+1
	Node31=[]
	Node32=[]
	Node33=[]
	Node34=[]
	Basal_Node=[]

	
	while (no_line<=1159) :
			if (no_line==1053 or no_line==1062 or no_line==1071): 	# Node 31
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node31.append(float(data_infor[3]))
				if (no_line==1071):
					#Node31=array(Node31)
					Basal_Node.append(Node31)
			elif (no_line==1082 or no_line==1091 or no_line==1100): 	# Node 32
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node32.append(float(data_infor[3]))
				if (no_line==1100):
					#Node32=array(Node32)
					Basal_Node.append(Node32)
			elif (no_line==1111 or no_line==1120 or no_line==1129):	# Node 33
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node33.append(float(data_infor[3]))
				if (no_line==1129):
					#Node33=array(Node33)
					Basal_Node.append(Node33)
			elif (no_line==1140 or no_line==1149 or no_line==1158):	# Node 34
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node34.append(float(data_infor[3]))
				if (no_line==1158):
					#Node34=array(Node34)
					Basal_Node.append(Node34)
							

			data = file.readline()
			no_line=no_line+1
	

	######################## Step 2: read in the data at DS ########################
	CAPEpi=readIpdata(fileName_surfaceDS_Epi)
	no_data=len(CAPEpi)

	Node_Data=[]
	## Loop through all data
	for i in range(4):
		Basal_Node_tmp=array(Basal_Node[i])
		Eucli_Disp=[]
		for j in range(no_data) :
			data=CAPEpi[j]
			## Calculate the euclidean distance
			Eucli_Disp.append(numpy.linalg.norm(data-Basal_Node_tmp))
			
		## Calculate the minimum
		Node_ED_min=min(Eucli_Disp)
		Node_Data.append(Eucli_Disp.index(Node_ED_min))
			
	## Print the four data index
	print 'The data indices for Node 31,32,32,34 are ',Node_Data

	return Node_Data

#===========================================================================================#

#===========================================================================================#
def extractNodalDisp(fileName) :
	
	######################## Step 1: Read in the ipnode file ########################
	## Read in the fitted ipnde file
	file = open( fileName, 'r' )

	## Read a single line at a time
	no_line=0
	data = file.readline()
	no_line=no_line+1
	Node31=[]
	Node32=[]
	Node33=[]
	Node34=[]
	Basal_Node=[]

	
	while (no_line<=1159) :
			if (no_line==1053 or no_line==1062 or no_line==1071): 	# Node 31
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node31.append(float(data_infor[3]))
				if (no_line==1071):
					#Node31=array(Node31)
					Basal_Node.append(Node31)
			elif (no_line==1082 or no_line==1091 or no_line==1100): 	# Node 32
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node32.append(float(data_infor[3]))
				if (no_line==1100):
					#Node32=array(Node32)
					Basal_Node.append(Node32)
			elif (no_line==1111 or no_line==1120 or no_line==1129):	# Node 33
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node33.append(float(data_infor[3]))
				if (no_line==1129):
					#Node33=array(Node33)
					Basal_Node.append(Node33)
			elif (no_line==1140 or no_line==1149 or no_line==1158):	# Node 34
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node34.append(float(data_infor[3]))
				if (no_line==1158):
					#Node34=array(Node34)
					Basal_Node.append(Node34)
							

			data = file.readline()
			no_line=no_line+1
	

	######################## Step 2: read in the data at DS ########################
	CAPEpi=readIpdata('Surface_Points_Epi_DS.ipdata')
	no_data=len(CAPEpi)

	Node_Data=[]
	## Loop through all data
	for i in range(4):
		Basal_Node_tmp=array(Basal_Node[i])
		Eucli_Disp=[]
		for j in range(no_data) :
			data=CAPEpi[j]
			## Calculate the euclidean distance
			Eucli_Disp.append(numpy.linalg.norm(data-Basal_Node_tmp))
			
		## Calculate the minimum
		Node_ED_min=min(Eucli_Disp)
		Node_Data.append(Eucli_Disp.index(Node_ED_min))
			
	## Print the four data index
	print 'The data indices for Node 31,32,32,34 are ',Node_Data

	## Read in the data at ED and ES
	CAPEpi=CAPEpi
	CAPEpi_ED=readIpdata('Surface_Points_Epi_ED.ipdata')
	

	print '================ Write out ipinit file based on displacement  ===================='
	## Calculate the nodal displacement during inflation
	Node31_x_ED=CAPEpi_ED[Node_Data[0],0]-CAPEpi[Node_Data[0],0] 	
	Node32_x_ED=CAPEpi_ED[Node_Data[1],0]-CAPEpi[Node_Data[1],0]	
	Node33_x_ED=CAPEpi_ED[Node_Data[2],0]-CAPEpi[Node_Data[2],0]	
	Node34_x_ED=CAPEpi_ED[Node_Data[3],0]-CAPEpi[Node_Data[3],0]	

	Node31_y_ED=CAPEpi_ED[Node_Data[0],1]-CAPEpi[Node_Data[0],1] 	
	Node32_y_ED=CAPEpi_ED[Node_Data[1],1]-CAPEpi[Node_Data[1],1]	
	Node33_y_ED=CAPEpi_ED[Node_Data[2],1]-CAPEpi[Node_Data[2],1]	
	Node34_y_ED=CAPEpi_ED[Node_Data[3],1]-CAPEpi[Node_Data[3],1]	

	Node31_z_ED=CAPEpi_ED[Node_Data[0],2]-CAPEpi[Node_Data[0],2] 	
	Node32_z_ED=CAPEpi_ED[Node_Data[1],2]-CAPEpi[Node_Data[1],2]	
	Node33_z_ED=CAPEpi_ED[Node_Data[2],2]-CAPEpi[Node_Data[2],2]	
	Node34_z_ED=CAPEpi_ED[Node_Data[3],2]-CAPEpi[Node_Data[3],2]	

	print 'Nodal displacement during Inflation '
	print 'Node 31 x, y, z diplacements = ',Node31_x_ED,Node31_y_ED,Node31_z_ED
	print 'Node 32 x, y, z diplacements = ',Node32_x_ED,Node32_y_ED,Node32_z_ED
	print 'Node 33 x, y, z diplacements = ',Node33_x_ED,Node33_y_ED,Node33_z_ED
	print 'Node 34 x, y, z diplacements = ',Node34_x_ED,Node34_y_ED,Node34_z_ED

	Node31_ED=[Node31_x_ED,Node31_y_ED,Node31_z_ED]
	Node32_ED=[Node32_x_ED,Node32_y_ED,Node32_z_ED]
	Node33_ED=[Node33_x_ED,Node33_y_ED,Node33_z_ED]
	Node34_ED=[Node34_x_ED,Node34_y_ED,Node34_z_ED]
	
	return Node31_ED,Node32_ED,Node33_ED,Node34_ED

#===============================================================================#

#===========================================================================================#
def extractNodalDisp_Systole(fileName) :
	
	######################## Step 1: Read in the ipnode file ########################
	## Read in the fitted ipnde file
	file = open( fileName, 'r' )

	## Read a single line at a time
	no_line=0
	data = file.readline()
	no_line=no_line+1
	Node31=[]
	Node32=[]
	Node33=[]
	Node34=[]
	Basal_Node=[]

	
	while (no_line<=1159) :
			if (no_line==1053 or no_line==1062 or no_line==1071): 	# Node 31
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node31.append(float(data_infor[3]))
				if (no_line==1071):
					#Node31=array(Node31)
					Basal_Node.append(Node31)
			elif (no_line==1082 or no_line==1091 or no_line==1100): 	# Node 32
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node32.append(float(data_infor[3]))
				if (no_line==1100):
					#Node32=array(Node32)
					Basal_Node.append(Node32)
			elif (no_line==1111 or no_line==1120 or no_line==1129):	# Node 33
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node33.append(float(data_infor[3]))
				if (no_line==1129):
					#Node33=array(Node33)
					Basal_Node.append(Node33)
			elif (no_line==1140 or no_line==1149 or no_line==1158):	# Node 34
				data_infor=re.findall("(-\d+\.\d+|\d+\.\d+|\d+)", data)
				Node34.append(float(data_infor[3]))
				if (no_line==1158):
					#Node34=array(Node34)
					Basal_Node.append(Node34)
							

			data = file.readline()
			no_line=no_line+1
	

	######################## Step 2: read in the data at DS ########################
	CAPEpi=readIpdata('Surface_Points_Epi_DS.ipdata')
	no_data=len(CAPEpi)

	Node_Data=[]
	## Loop through all data
	for i in range(4):
		Basal_Node_tmp=array(Basal_Node[i])
		Eucli_Disp=[]
		for j in range(no_data) :
			data=CAPEpi[j]
			## Calculate the euclidean distance
			Eucli_Disp.append(numpy.linalg.norm(data-Basal_Node_tmp))
			
		## Calculate the minimum
		Node_ED_min=min(Eucli_Disp)
		Node_Data.append(Eucli_Disp.index(Node_ED_min))
			
	## Print the four data index
	print 'The data indices for Node 31,32,32,34 are ',Node_Data

	## Read in the data at ED and ES
	CAPEpi=CAPEpi
	CAPEpi_ES=readIpdata('Surface_Points_Epi_ES.ipdata')
	

	print '================ Write out ipinit file based on displacement  ===================='
	## Calculate the nodal displacement during inflation
	Node31_x_ES=CAPEpi_ES[Node_Data[0],0]-CAPEpi[Node_Data[0],0] 	
	Node32_x_ES=CAPEpi_ES[Node_Data[1],0]-CAPEpi[Node_Data[1],0]	
	Node33_x_ES=CAPEpi_ES[Node_Data[2],0]-CAPEpi[Node_Data[2],0]	
	Node34_x_ES=CAPEpi_ES[Node_Data[3],0]-CAPEpi[Node_Data[3],0]	

	Node31_y_ES=CAPEpi_ES[Node_Data[0],1]-CAPEpi[Node_Data[0],1] 	
	Node32_y_ES=CAPEpi_ES[Node_Data[1],1]-CAPEpi[Node_Data[1],1]	
	Node33_y_ES=CAPEpi_ES[Node_Data[2],1]-CAPEpi[Node_Data[2],1]	
	Node34_y_ES=CAPEpi_ES[Node_Data[3],1]-CAPEpi[Node_Data[3],1]	

	Node31_z_ES=CAPEpi_ES[Node_Data[0],2]-CAPEpi[Node_Data[0],2] 	
	Node32_z_ES=CAPEpi_ES[Node_Data[1],2]-CAPEpi[Node_Data[1],2]	
	Node33_z_ES=CAPEpi_ES[Node_Data[2],2]-CAPEpi[Node_Data[2],2]	
	Node34_z_ES=CAPEpi_ES[Node_Data[3],2]-CAPEpi[Node_Data[3],2]	

	print 'Nodal displacement during Systole '
	print 'Node 31 x, y, z diplacements = ',Node31_x_ES,Node31_y_ES,Node31_z_ES
	print 'Node 32 x, y, z diplacements = ',Node32_x_ES,Node32_y_ES,Node32_z_ES
	print 'Node 33 x, y, z diplacements = ',Node33_x_ES,Node33_y_ES,Node33_z_ES
	print 'Node 34 x, y, z diplacements = ',Node34_x_ES,Node34_y_ES,Node34_z_ES

	Node31_ES=[Node31_x_ES,Node31_y_ES,Node31_z_ES]
	Node32_ES=[Node32_x_ES,Node32_y_ES,Node32_z_ES]
	Node33_ES=[Node33_x_ES,Node33_y_ES,Node33_z_ES]
	Node34_ES=[Node34_x_ES,Node34_y_ES,Node34_z_ES]
	
	return Node31_ES,Node32_ES,Node33_ES,Node34_ES

#===============================================================================#

def extractNodalDisp_MF(Node_Data, fileNameCurrent, fileNameNext):

	## Read in the data at ED and ES
	CAPEpi= readIpdata(fileNameCurrent)
	CAPEpi_def=readIpdata(fileNameNext)	

	print '================ Write out ipinit file based on displacement  ===================='
	## Calculate the nodal displacement during inflation
	Node31_x_def=CAPEpi_def[Node_Data[0],0]-CAPEpi[Node_Data[0],0] 	
	Node32_x_def=CAPEpi_def[Node_Data[1],0]-CAPEpi[Node_Data[1],0]	
	Node33_x_def=CAPEpi_def[Node_Data[2],0]-CAPEpi[Node_Data[2],0]	
	Node34_x_def=CAPEpi_def[Node_Data[3],0]-CAPEpi[Node_Data[3],0]

	Node31_y_def=CAPEpi_def[Node_Data[0],1]-CAPEpi[Node_Data[0],1] 	
	Node32_y_def=CAPEpi_def[Node_Data[1],1]-CAPEpi[Node_Data[1],1]	
	Node33_y_def=CAPEpi_def[Node_Data[2],1]-CAPEpi[Node_Data[2],1]	
	Node34_y_def=CAPEpi_def[Node_Data[3],1]-CAPEpi[Node_Data[3],1]	

	Node31_z_def=CAPEpi_def[Node_Data[0],2]-CAPEpi[Node_Data[0],2] 	
	Node32_z_def=CAPEpi_def[Node_Data[1],2]-CAPEpi[Node_Data[1],2]	
	Node33_z_def=CAPEpi_def[Node_Data[2],2]-CAPEpi[Node_Data[2],2]	
	Node34_z_def=CAPEpi_def[Node_Data[3],2]-CAPEpi[Node_Data[3],2]	

	print 'Nodal displacement: '
	print 'Node 31 x, y, z diplacements = ',Node31_x_def,Node31_y_def,Node31_z_def
	print 'Node 32 x, y, z diplacements = ',Node32_x_def,Node32_y_def,Node32_z_def
	print 'Node 33 x, y, z diplacements = ',Node33_x_def,Node33_y_def,Node33_z_def
	print 'Node 34 x, y, z diplacements = ',Node34_x_def,Node34_y_def,Node34_z_def

	Node31_def=[Node31_x_def,Node31_y_def,Node31_z_def]
	Node32_def=[Node32_x_def,Node32_y_def,Node32_z_def]
	Node33_def=[Node33_x_def,Node33_y_def,Node33_z_def]
	Node34_def=[Node34_x_def,Node34_y_def,Node34_z_def]

	return Node31_def,Node32_def,Node33_def,Node34_def


#======================================================================#
def readIpdata( fileName ):
	""" reads ipdata file and returns the x y z coords on data points
	and the header if there is one
	"""
	
	try:
		file = open( fileName, 'r' )
	except IOError:
		print 'ERROR: readIpdata: unable to open', fileName
		return
	
	lines = file.readline()
	lines = file.readlines()

	coords = []
	for l in lines:
		coords.append([float(string) for string in l.split()[1:4]])

	coords = array( coords )
	
	
	return coords
#======================================================================#

