"""
This main function is designed to automatically implement the following steps
(1) convert CIM guide-point models to exnode format, and then ipnode format
(2) generate surface data points from the ED and ES CIM models
(3) perform passive mechanics simulation and estimate C1
(4) Use optimal C1 to perform active mechanics simulation and estimate Tca
Author: Vicky Wang 
Modified for multi-frame: Jenny Zhinuo Wang
"""

import dircache
import os,sys
import shutil
from shutil import copy
import re
import scipy
import fnmatch

from Rewrite_Ipinit_Diastole import Rewrite_Ipinit_Diastole
from OptimiseC1 import optimisePassiveStiffness,createIPMATE
from extractNodalDisp import extractNodalIndices,extractNodalDisp


#=====================================================#

def Main_Program_Passive(const_model_type,current_study_name,current_study_frame, frame_pressure,Node_data):

		
		## Change directory to mechanics template files
		## Copy files to passive mechanics files
		all_files=os.listdir('../Mechanics_TemplateFiles/PassiveMechanics')
		os.chdir('../Mechanics_TemplateFiles/PassiveMechanics')
		dstPassiveMechanics='../../' + current_study_name + '/LVMechanics_' + current_study_name + '/PassiveMechanics/'
		
		for file in all_files:
			copy(file,dstPassiveMechanics)

		os.chdir(dstPassiveMechanics)


		## Extract relevant data points to construct ipinit file (LV_CubicPreEpiBase_temp.ipinit)
		## This is for the 2-stage analysis only 
		fileName='CAP_DS_humanLV.ipnode'		
		[Node31_ED,Node32_ED,Node33_ED,Node34_ED]=extractNodalDisp(fileName)

		## Copy over ipinit file template
		fileName='LV_CubicPreEpiBase_temp.ipinit'
		fileNamew='LV_CubicPreEpiBase.ipinit'
		copy(fileName, fileNamew)
		## Add the pressure information to construct the final ipinit file (LV_CubicPreEpiBase.ipinit)
		LVP_ED=frame_pressure[int(current_study_frame[1])-1]
		print LVP_ED
		Rewrite_Ipinit_Diastole(fileName,LVP_ED,Node31_ED,Node32_ED,Node33_ED,Node34_ED,fileNamew)
		print '==Finished writing ipinit file for passive mechanics =='

		os.chdir('../../../PYTHON_Codes')
	
		## Start C1 estimation
		optimisePassiveStiffness(const_model_type,current_study_name)

		# Copy the optimal passive solution to active mechanics folder
		os.system('cp CAP_DS_humanLV.ipnode ../ActiveMechanics/')
		os.system('cp CAP_DS_humanLV.ipelem ../ActiveMechanics/')
		os.system('cp LVCavity.ipnode ../ActiveMechanics/')
		os.system('cp LVCavity.ipelem ../ActiveMechanics/')
		os.system('cp -r output ../ActiveMechanics/')
		
		if fnmatch.fnmatch(const_model_type, 'Holzapfel'):
			## Copy the optimal ipive mechanics folder
			os.system('cp holzapfel_struct_modify.ipcell ../ActiveMechanics/')
			os.system('cp DTMRI_CIMModel_sheet.ipfibr ../ActiveMechanics/')
			os.system('cp DTMRI_CIMModel_sheet.ipelfb ../ActiveMechanics/')
		elif fnmatch.fnmatch(const_model_type, 'Guccione'):
			## Copy the optimal ipmate file to the active mechanics folder
			os.system('cp LV_CubicGuc.ipmate ../ActiveMechanics/')
			os.system('cp DTMRI_CIMModel.ipfibr ../ActiveMechanics/')
			os.system('cp DTMRI_CIMModel.ipelfb ../ActiveMechanics/')

		os.chdir('../../../PYTHON_Codes')
			
##########################################################################################
#======================================================#


